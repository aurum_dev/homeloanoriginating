import React, { useContext, useState } from "react";
import Header from "../../MultiStepForm/header/Header";
import girt from "../../../assets/images/girt-surprice.svg";
import successicon from "../../../assets/images/successicon.svg";
import info from "../../../assets/images/info.svg";

import "../../../assets/css/Document/Document.css";
import { Formik, Field } from "formik";
import { Button } from "antd";
import { Input } from "formik-antd";
import MultiStepFormContext from "../MultiStepFormContext";

const Documentverify = () => {
  const [preview, setPreview] = useState("");
  const [previewpancard, setPreviewpancard] = useState("");
  const { documentverify, setDocumentverify, next, prev } =
    useContext(MultiStepFormContext);

  async function uploadFile(filepath) {
    const reader = new FileReader();

    reader.readAsBinaryString(filepath);

    reader.onload = () => {
      // this is the base64 data

      const fileRes = btoa(reader.result);

      // console.log(`data:image/jpg;base64,${fileRes}`);

      setPreview(`data:image/jpg;base64,${fileRes}`);
      
    };
  }
  async function uploadFilePan(filepath) {
    const reader = new FileReader();

    reader.readAsBinaryString(filepath);

    reader.onload = () => {
      // this is the base64 data

      const fileRes = btoa(reader.result);

      // console.log(`data:image/jpg;base64,${fileRes}`);

      setPreview(`data:image/jpg;base64,${fileRes}`);
      
    };
  }
  async function onChange(e) {
    const [fil] = e.target.files || e.dataTransfer.files;
    uploadFile(fil);
  }
  async function onChangePan(e) {
    const [fil] = e.target.files || e.dataTransfer.files;
    uploadFilePan(fil);
  }
  return (
    <Formik
      initialValues={documentverify}
      onSubmit={(values) => {
        setDocumentverify(values);
        next();
      }}
      validate={(values) => {
        const errors = {};
        // if (!values.explorename) errors.explorename = "Full name is required";
        if (!values.file) errors.file = "Bank statment is required";
        if (!values.pancard) errors.pancard = "Company Pan card is required";
        if (!values.tancard) errors.tancard = "Tan card is required";
        if (!values.form) errors.form = "Form 16 is required";
        if (!values.itr) errors.itr = "ITR is required";
        if (!values.subpan) errors.subpan = "PAN Card is required";
        if (!values.relation)
          errors.relation = "Proof of realation is required";
        if (!values.salaryslip) errors.salaryslip = "Salary slip is Required";

        return errors;
      }}
    >
      {({ handleSubmit, errors }) => {
        <Header />;
        return (
          <div className={"details__wrapper"}>
            {/* <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4 ">
              <div
                className={` form__item ${
                  errors.explorename && "input__error"
                }`}
              >
                <label
                  htmlhtmlFor="name"
                  className="col-span-6 lg:col-span-2 relative block"
                >
                  <div className="app-form-wrapper">
                    <span data-testid="name_label" className="app-form-label">
                      Full Name
                    </span>
                  </div>
                  <div className="relative rounded-md" role="presentation">
                    <Input
                      name={"explorename"}
                      className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled"
                      aria-invalid="false"
                      aria-describedby="name-error"
                      type="text"
                      placeholder="Enter name"
                      data-testid="name"
                    />
                  </div>
                  <p className={"error__feedback"}>{errors.explorename}</p>
                </label>
              </div>
            </div> */}
            <div className="container relative px-2 sm:px-0 mx-auto mt-24 sm:mt-28 sm:w-7/12">
              {/* <ol className="inline-flex items-center justify-between w-full rounded-md">
          <li className="flex w-auto sm:min-w-[106px] justify-center sm:min-h-[72px]">
            <button
              type="button"
              className="flex items-center group focus:outline-none group-hover:text-secondary"
            >
              <div className="flex flex-col sm:h-full sm:justify-between items-center text-xs font-medium">
                <div className="text-white rounded-full">
                  <div className="text-secondary">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="transparent"
                      width="24"
                      height="24"
                      viewBox="0 0 20 20"
                    >
                      <g fill="currentColor" fill-rule="evenodd">
                        <g fill="currentColor" fill-rule="nonzero">
                          <path
                            d="M24.996 16c4.968.011 8.993 4.034 9.004 9 0 2.695-1.209 5.248-3.292 6.957-2.084 1.71-4.824 2.396-7.468 1.87-4.538-.906-7.65-5.104-7.196-9.708.453-4.604 4.324-8.115 8.952-8.119zm4.722 6.386c-.304-.304-.796-.304-1.1 0L24.503 26.5l-2.042-2.042c-.304-.303-.796-.303-1.1 0-.303.304-.303.796 0 1.1l2.592 2.59c.304.304.796.304 1.1 0l4.665-4.663c.304-.304.304-.796 0-1.1z"
                            transform="translate(-16 -16)"
                          ></path>
                        </g>
                      </g>
                    </svg>
                  </div>
                </div>
                <div className="font-body text-base sm:text-sm font-bold text-primary absolute sm:relative w-full sm:w-auto text-center left-0 top-8 sm:top-0 hidden sm:block">
                  Basic Details
                </div>
              </div>
            </button>
          </li>
          <li className="flex-1 sm:flex-auto sm:min-h-[72px]">
            <div className="border-b-2 sm:h-[13px] sm:w-[102px] sm:m-auto border-secondary"></div>
          </li>
          <li className="flex w-auto sm:min-w-[106px] justify-center sm:min-h-[72px]">
            <button
              type="button"
              className="flex items-center group focus:outline-none group-hover:text-secondary"
            >
              <div className="flex flex-col sm:h-full sm:justify-between items-center text-xs font-medium">
                <div className="text-white rounded-full">
                  <div className="text-secondary">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="transparent"
                      width="24"
                      height="24"
                      viewBox="0 0 20 20"
                    >
                      <g fill="currentColor" fill-rule="evenodd">
                        <g fill="currentColor" fill-rule="nonzero">
                          <path
                            d="M24.996 16c4.968.011 8.993 4.034 9.004 9 0 2.695-1.209 5.248-3.292 6.957-2.084 1.71-4.824 2.396-7.468 1.87-4.538-.906-7.65-5.104-7.196-9.708.453-4.604 4.324-8.115 8.952-8.119zm4.722 6.386c-.304-.304-.796-.304-1.1 0L24.503 26.5l-2.042-2.042c-.304-.303-.796-.303-1.1 0-.303.304-.303.796 0 1.1l2.592 2.59c.304.304.796.304 1.1 0l4.665-4.663c.304-.304.304-.796 0-1.1z"
                            transform="translate(-16 -16)"
                          ></path>
                        </g>
                      </g>
                    </svg>
                  </div>
                </div>
                <div className="font-body text-base sm:text-sm font-bold text-primary absolute sm:relative w-full sm:w-auto text-center left-0 top-8 sm:top-0 hidden sm:block">
                  Personal Information
                </div>
              </div>
            </button>
          </li>
          <li className="flex-1 sm:flex-auto sm:min-h-[72px]">
            <div className="border-b-2 sm:h-[13px] sm:w-[102px] sm:m-auto border-secondary"></div>
          </li>
          <li className="flex w-auto sm:min-w-[106px] justify-center sm:min-h-[72px]">
            <button
              type="button"
              className="flex items-center group focus:outline-none group-hover:text-secondary"
            >
              <div className="flex flex-col sm:h-full sm:justify-between items-center text-xs font-medium">
                <div className="text-white rounded-full">
                  <div className="text-secondary">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="transparent"
                      width="24"
                      height="24"
                      viewBox="0 0 20 20"
                    >
                      <g fill="currentColor" fill-rule="evenodd">
                        <g fill="currentColor" fill-rule="nonzero">
                          <path
                            d="M24.996 16c4.968.011 8.993 4.034 9.004 9 0 2.695-1.209 5.248-3.292 6.957-2.084 1.71-4.824 2.396-7.468 1.87-4.538-.906-7.65-5.104-7.196-9.708.453-4.604 4.324-8.115 8.952-8.119zm4.722 6.386c-.304-.304-.796-.304-1.1 0L24.503 26.5l-2.042-2.042c-.304-.303-.796-.303-1.1 0-.303.304-.303.796 0 1.1l2.592 2.59c.304.304.796.304 1.1 0l4.665-4.663c.304-.304.304-.796 0-1.1z"
                            transform="translate(-16 -16)"
                          ></path>
                        </g>
                      </g>
                    </svg>
                  </div>
                </div>
                <div className="font-body text-base sm:text-sm font-bold text-primary absolute sm:relative w-full sm:w-auto text-center left-0 top-8 sm:top-0 hidden sm:block">
                  Explore Loan Offers
                </div>
              </div>
            </button>
          </li>
          <li className="flex-1 sm:flex-auto sm:min-h-[72px]">
            <div className="border-b-2 sm:h-[13px] sm:w-[102px] sm:m-auto border-secondary"></div>
          </li>
          <li className="flex w-auto sm:min-w-[106px] justify-center sm:min-h-[72px]">
            <button
              type="button"
              className="flex items-center group focus:outline-none group-hover:text-secondary"
            >
              <div className="flex flex-col sm:h-full sm:justify-between items-center text-xs font-medium">
                <div className="text-white rounded-full">
                  <div className="text-secondary">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none">
                      <circle
                        cx="12.666"
                        cy="12"
                        r="9.5"
                        fill="white"
                        stroke="currentColor"
                      ></circle>
                      <circle
                        cx="12.666"
                        cy="12"
                        r="6.5"
                        fill="currentColor"
                        stroke="currentColor"
                      ></circle>
                    </svg>
                  </div>
                </div>
                <div className="font-body text-base sm:text-sm font-bold text-primary absolute sm:relative w-full sm:w-auto text-center left-0 top-8 sm:top-0 block">
                  Document Verification
                </div>
              </div>
            </button>
          </li>
          <li className="flex-1 sm:flex-auto sm:min-h-[72px]">
            <div className="border-b-2 sm:h-[13px] sm:w-[102px] sm:m-auto border-secondary"></div>
          </li>
          <li className="flex w-auto sm:min-w-[106px] justify-center sm:min-h-[72px]">
            <button
              type="button"
              className="flex items-center group focus:outline-none group-hover:text-secondary"
            >
              <div className="flex flex-col sm:h-full sm:justify-between items-center text-xs font-medium">
                <div className="text-white rounded-full">
                  <div className="text-secondary">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none">
                      <circle
                        cx="10"
                        cy="10"
                        r="9.5"
                        fill="white"
                        stroke="#003B70"
                      ></circle>
                    </svg>
                  </div>
                </div>
                <div className="font-body text-base sm:text-sm font-bold text-primary absolute sm:relative w-full sm:w-auto text-center left-0 top-8 sm:top-0 hidden sm:block">
                  Provisional Offer Letter
                </div>
              </div>
            </button>
          </li>
        </ol> */}
              <div className="flex items-start sm:items-center px-2 border border-gray-300 rounded-md sm:px-6 mt-10 sm:mt-10 mb-5">
                <img
                  className="w-[72px] h-[72px] flex-shrink-0"
                  src={girt}
                  alt="girt"
                />
                <div className="flex justify-between items-center w-full ml-4">
                  <div className="py-2 sm:py-0">
                    <p className="font-body text-[13px] sm:text-[22px] sm:font-bold text-primary leading-4 sm:leading-[26px]">
                      Your application was successfully submitted
                    </p>
                    <p className="font-body text-[11px] sm:text-[13px] text-gray-400 leading-[16px] mt-1">
                      We will be sharing your application soon, help us with few
                      more documents to make your application stronger.
                    </p>
                  </div>
                  <img
                    className="h-10 ml-6 flex-shrink-0"
                    src="https://easiloan-public.s3.ap-south-1.amazonaws.com/hdfc.png"
                    alt="girt"
                  />
                </div>
              </div>
              <div className="flex items-center justify-between">
                <div className="text-left">
                  <h3 className="app-page-title">Document Verification</h3>
                  <div className="mt-1">
                    <p className="app-page-sub-title">
                      Easiloan will be requiring following documents to process
                      your application further.
                    </p>
                  </div>
                </div>
              </div>
              <div>
                <div className="mx-auto max-w-7xl">
                  <div className="border-b border-gray-light">
                    <dt className="text-lg">
                      <button
                        type="button"
                        className="flex items-center justify-between w-full text-left text-gray-400"
                        aria-controls="5f6ea2ab-a345-4993-be31-8280622ed085"
                        aria-expanded="true"
                      >
                        <div className="flex items-center py-5 sm:py-6">
                          <span className="mr-3 text-lg font-medium sm:text-xl sm:mr-6 text-primary">
                            Primary Applicant
                          </span>
                        </div>
                        <div className="flex items-center">
                          <span className="text-sm text-primary-100"> </span>
                          <span className="flex items-center ml-3 sm:ml-5 h-7">
                            <svg
                              className="rotate-180 text-black h-5 w-5 transform"
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              viewBox="0 0 24 24"
                              stroke="currentColor"
                              aria-hidden="true"
                            >
                              <path
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M19 9l-7 7-7-7"
                              ></path>
                            </svg>
                          </span>
                        </div>
                      </button>
                    </dt>
                    <dd className="block">
                      <div className="border border-gray-300 rounded-md mb-4">
                        <div
                          className="accordion-button flex items-center justify-between w-full text-left text-gray-400"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target="#collapseOne"
                          // aria-expanded="true"
                          aria-controls="collapseOne"
                          role="presentation"
                        >
                          <span className="text-xl font-normal app-page-title">
                            Aadhaar Card
                          </span>
                          <div className="flex items-center text-sm text-green">
                            Completed
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="w-4 h-4 ml-1"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path
                                fill-rule="evenodd"
                                d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                                clip-rule="evenodd"
                              ></path>
                            </svg>
                          </div>
                        </div>

                        <div
                          id="collapseOne"
                          className="accordion-collapse collapse "
                          aria-labelledby="headingOne"
                          data-bs-parent="#accordionExample"
                        >
                          <div className="relative">
                            <div className="flex items-center justify-between mb-2"></div>
                            <div className="p-1 border-t border-gray-300">
                              <div className="px-[10px] py-2 rounded-full border border-green-400 bg-green-50 mb-2 mt-2">
                                <div className="flex items-center">
                                  <div className="flex-shrink-0">
                                    <img
                                      className="w-[18px] h-[18px]"
                                      src={successicon}
                                      alt="img"
                                    />
                                  </div>
                                  <div className="ml-2">
                                    <p className="app-text-dm-small text-gray text-opacity-80">
                                      Your offline e-kyc is completed
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="border border-gray-300 rounded-md mb-4">
                        <div
                          className="accordion-button flex items-center justify-between w-full text-left text-gray-400"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target="#collapseTwo"
                          // aria-expanded="true"
                          aria-controls="collapseTwo"
                          role="presentation"
                        >
                          <span className="text-xl font-normal app-page-title">
                            Bank Statement
                          </span>
                          <div className="flex items-center text-sm">
                            In-Progress
                          </div>
                        </div>
                        <div
                          id="collapseTwo"
                          className="accordion-collapse collapse "
                          aria-labelledby="headingTwo"
                          data-bs-parent="#accordionExample"
                        >
                          <div className="p-4 border-t border-gray-300">
                            <div className="px-[10px] py-2 border border-secondary rounded-full bg-secondary-400 mb-5">
                              <div className="flex items-center">
                                <div className="flex-shrink-0">
                                  <img
                                    src={info}
                                    className="w-[18px] h-[18px]"
                                    alt="info"
                                  />
                                </div>
                                <div className="ml-2">
                                  <p className="app-text-dm-small text-gray text-opacity-80">
                                    Please upload your last one year bank
                                    statement.
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div className="block mb-6 uppercase app-text-link text-secondary md:hidden">
                              Choose your verification method
                            </div>
                            <div className="flex flex-col pb-6 mt-6 mb-6 border-b border-gray-300 md:flex-row md:items-center md:mb-8 gap-x-6 md:gap-x-20 gap-y-7">
                              <div
                                style={{ display: "flex", paddingTop: "15px" }}
                                className={`form__item ${
                                  errors.upload && "input__error"
                                }`}
                              >
                                <input
                                  id="toggle-on"
                                  name="upload"
                                  type="radio"
                                />
                                <label
                                  htmlFor="toggle-on"
                                  style={{
                                    padding: "10px 75px",
                                    // backgroundColor: "orange",
                                    border: "thin solid darkorange",
                                    borderRadius: "10px",
                                    margin: "5px",
                                    display: "inline-block",
                                  }}
                                >
                                  Upload
                                </label>
                                <input
                                  id="toggle-off"
                                  name="upload"
                                  type="radio"
                                />
                                <label
                                  htmlFor="toggle-off"
                                  style={{
                                    padding: "10px 75px",
                                    // backgroundColor: "orange",
                                    border: "thin solid darkorange",
                                    borderRadius: "10px",
                                    margin: "5px",
                                    display: "inline-block",
                                  }}
                                >
                                  IT Portal
                                </label>
                              </div>
                            </div>
                            <form data-testid="">
                              <div className="">
                                <div className="grid gap-y-5">
                                  <div className="space-y-8">
                                    <label
                                      htmlFor="upload_aadhar_front"
                                      className="relative block col-span-6"
                                    >
                                      <div className="app-form-wrapper">
                                        <span
                                          data-testid="upload_aadhar_front_label"
                                          className="app-form-label"
                                        >
                                          Upload Statement
                                        </span>
                                      </div>
                                      <div className="relative space-y-6">
                                        <div className="relative text-center border border-dashed border-primary-100 rounded-xl">
                                          <label
                                            htmlFor="document-g9acx"
                                            className="font-medium cursor-pointer"
                                          >
                                            <div className="px-8 py-6 ">
                                              <svg
                                                width="24"
                                                height="24"
                                                viewBox="0 0 24 24"
                                                fill="none"
                                                className="mx-auto"
                                              >
                                                <path
                                                  d="M12.0009 12.586L16.2439 16.828L14.8289 18.243L13.0009 16.415V22H11.0009V16.413L9.17287 18.243L7.75787 16.828L12.0009 12.586ZM12.0009 2C13.7179 2.00008 15.3749 2.63111 16.6571 3.77312C17.9392 4.91512 18.757 6.48846 18.9549 8.194C20.1991 8.53332 21.2846 9.2991 22.0215 10.3575C22.7585 11.416 23.1 12.6997 22.9865 13.9844C22.873 15.2691 22.3116 16.473 21.4004 17.3858C20.4893 18.2986 19.2864 18.8622 18.0019 18.978V16.964C18.462 16.8983 18.9045 16.7416 19.3035 16.503C19.7024 16.2644 20.0498 15.9487 20.3254 15.5744C20.6011 15.2001 20.7993 14.7746 20.9087 14.3228C21.0181 13.8711 21.0364 13.402 20.9626 12.9431C20.8887 12.4841 20.7242 12.0445 20.4786 11.6498C20.233 11.2552 19.9112 10.9134 19.5321 10.6445C19.1529 10.3755 18.724 10.1848 18.2704 10.0834C17.8167 9.98203 17.3474 9.97203 16.8899 10.054C17.0465 9.32489 17.038 8.56997 16.8651 7.84455C16.6922 7.11913 16.3592 6.44158 15.8905 5.86153C15.4218 5.28147 14.8293 4.81361 14.1563 4.49219C13.4834 4.17078 12.7471 4.00397 12.0014 4.00397C11.2556 4.00397 10.5193 4.17078 9.8464 4.49219C9.17347 4.81361 8.58097 5.28147 8.11227 5.86153C7.64358 6.44158 7.31058 7.11913 7.13765 7.84455C6.96473 8.56997 6.95626 9.32489 7.11287 10.054C6.20053 9.88267 5.25749 10.0808 4.49121 10.6048C3.72494 11.1287 3.1982 11.9357 3.02687 12.848C2.85554 13.7603 3.05366 14.7034 3.57763 15.4697C4.10161 16.2359 4.90853 16.7627 5.82087 16.934L6.00087 16.964V18.978C4.71632 18.8623 3.51328 18.2989 2.602 17.3862C1.69073 16.4735 1.1292 15.2696 1.01554 13.9848C0.901892 12.7001 1.24335 11.4163 1.98024 10.3578C2.71713 9.29926 3.80258 8.53339 5.04687 8.194C5.24458 6.48838 6.06228 4.91491 7.34445 3.77287C8.62662 2.63082 10.2838 1.99986 12.0009 2Z"
                                                  fill="#003B70"
                                                ></path>
                                              </svg>
                                              <h4 className="mt-2 mb-2 app-text-sub-main-link text-primary whitespace-nowrap">
                                                Drag and drop your files here
                                              </h4>
                                              <p className="mb-2 text-gray-200 app-text-input-secondary">
                                                Max. file size 5MB
                                              </p>

                                              <label
                                                htmlFor="document-g9acx"
                                                className="w-full app-btn app-btn-primary-outline cursor-pointer"
                                              >
                                                {/* <Input
                                                  id="file"
                                                  name="file"
                                                  type="file"
                                                  //  accept="application/pdf,application/vnd.ms-excel"
                                                  className="sr-only"
                                                  multiple=""
                                                  // onChange={onchange}
                                                /> */}
                                                <Input
                                                  type="file"
                                                  id="file"
                                                  name="file"
                                                  className="upload-file"
                                                  accept="image/*"
                                                  onChange={onChange}
                                                />
                                              </label>

                                              <p className={"error__feedback"}>
                                                {errors.file}
                                              </p>
                                              <div>
                                                <img src={preview} />
                                              </div>
                                            </div>
                                          </label>
                                        </div>
                                        <div className="space-y-6"></div>
                                      </div>
                                    </label>
                                    <label
                                      htmlFor="password"
                                      className="relative block col-span-6"
                                    >
                                      <div className="app-form-wrapper">
                                        <span
                                          data-testid="password_label"
                                          className="app-form-label"
                                        >
                                          Password, if the above document is
                                          password protected
                                        </span>
                                      </div>
                                      <div
                                        className="relative rounded-md"
                                        role="presentation"
                                      >
                                        <input
                                          className="block w-full pl-5 pr-9 py-[15px]  shadow-none"
                                          aria-invalid="false"
                                          aria-describedby="password-error"
                                          name="password"
                                          placeholder="Password"
                                          type="password"
                                          autocomplete="off"
                                          data-testid="password"
                                        />
                                        <div className="absolute inset-y-0 right-0 flex items-center pr-3">
                                          <button
                                            type="button"
                                            data-testid="-password"
                                          >
                                            <svg
                                              xmlns="http://www.w3.org/2000/svg"
                                              className="w-5 h-5 text-secondary"
                                              viewBox="0 0 20 20"
                                              fill="currentColor"
                                            >
                                              <path d="M10 12a2 2 0 100-4 2 2 0 000 4z"></path>
                                              <path
                                                fill-rule="evenodd"
                                                d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z"
                                                clip-rule="evenodd"
                                              ></path>
                                            </svg>
                                          </button>
                                        </div>
                                      </div>
                                    </label>
                                  </div>
                                </div>
                                <div className="mt-10 md:mt-8">
                                  <button
                                    disabled=""
                                    className="app-btn app-btn-primary min-w-[230px]"
                                    type="submit"
                                    data-testid="submit"
                                    data-category="bank_pdf"
                                    data-action="Upload Pdf"
                                    data-label="-"
                                  >
                                    <span>Continue</span>
                                  </button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>

                      <div className="border border-gray-300 rounded-md mb-4">
                        <div
                          className="accordion-button flex items-center justify-between w-full text-left text-gray-400"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target="#collapseThree"
                          // aria-expanded="true"
                          aria-controls="collapseThree"
                          role="presentation"
                        >
                          <span className="text-xl font-normal app-page-title">
                            Company PAN Card
                          </span>
                          <div className="flex items-center text-sm">
                            In-Progress
                          </div>
                        </div>
                        <div
                          id="collapseThree"
                          className="accordion-collapse collapse "
                          aria-labelledby="headingThree"
                          data-bs-parent="#accordionExample"
                        >
                          <div className="p-4 border-t border-gray-300">
                            <div className="">
                              <div className="">
                                <label htmlFor="" className="relative block col-span-6">
                                  <div className="app-form-wrapper">
                                    <span
                                      data-testid="_label"
                                      className="app-form-label"
                                    ></span>
                                  </div>
                                  <div className="relative space-y-6">
                                    <div className="relative text-center border border-dashed border-primary-100 rounded-xl">
                                      <label
                                        htmlFor="document-b1aaz"
                                        className="font-medium cursor-pointer"
                                      >
                                        <div className="px-8 py-6">
                                          <svg
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            className="mx-auto"
                                          >
                                            <path
                                              d="M12.0009 12.586L16.2439 16.828L14.8289 18.243L13.0009 16.415V22H11.0009V16.413L9.17287 18.243L7.75787 16.828L12.0009 12.586ZM12.0009 2C13.7179 2.00008 15.3749 2.63111 16.6571 3.77312C17.9392 4.91512 18.757 6.48846 18.9549 8.194C20.1991 8.53332 21.2846 9.2991 22.0215 10.3575C22.7585 11.416 23.1 12.6997 22.9865 13.9844C22.873 15.2691 22.3116 16.473 21.4004 17.3858C20.4893 18.2986 19.2864 18.8622 18.0019 18.978V16.964C18.462 16.8983 18.9045 16.7416 19.3035 16.503C19.7024 16.2644 20.0498 15.9487 20.3254 15.5744C20.6011 15.2001 20.7993 14.7746 20.9087 14.3228C21.0181 13.8711 21.0364 13.402 20.9626 12.9431C20.8887 12.4841 20.7242 12.0445 20.4786 11.6498C20.233 11.2552 19.9112 10.9134 19.5321 10.6445C19.1529 10.3755 18.724 10.1848 18.2704 10.0834C17.8167 9.98203 17.3474 9.97203 16.8899 10.054C17.0465 9.32489 17.038 8.56997 16.8651 7.84455C16.6922 7.11913 16.3592 6.44158 15.8905 5.86153C15.4218 5.28147 14.8293 4.81361 14.1563 4.49219C13.4834 4.17078 12.7471 4.00397 12.0014 4.00397C11.2556 4.00397 10.5193 4.17078 9.8464 4.49219C9.17347 4.81361 8.58097 5.28147 8.11227 5.86153C7.64358 6.44158 7.31058 7.11913 7.13765 7.84455C6.96473 8.56997 6.95626 9.32489 7.11287 10.054C6.20053 9.88267 5.25749 10.0808 4.49121 10.6048C3.72494 11.1287 3.1982 11.9357 3.02687 12.848C2.85554 13.7603 3.05366 14.7034 3.57763 15.4697C4.10161 16.2359 4.90853 16.7627 5.82087 16.934L6.00087 16.964V18.978C4.71632 18.8623 3.51328 18.2989 2.602 17.3862C1.69073 16.4735 1.1292 15.2696 1.01554 13.9848C0.901892 12.7001 1.24335 11.4163 1.98024 10.3578C2.71713 9.29926 3.80258 8.53339 5.04687 8.194C5.24458 6.48838 6.06228 4.91491 7.34445 3.77287C8.62662 2.63082 10.2838 1.99986 12.0009 2Z"
                                              fill="#003B70"
                                            ></path>
                                          </svg>
                                          <h4 className="mt-2 mb-2 app-text-sub-main-link text-primary whitespace-nowrap">
                                            Drag and drop your files here
                                          </h4>
                                          <p className="mb-2 text-gray-200 app-text-input-secondary">
                                            Max. file size 5MB
                                          </p>
                                          <label
                                            htmlFor="document-b1aaz"
                                            className="w-full app-btn app-btn-primary-outline cursor-pointer"
                                          >
                                            <Input
                                              id="pancard"
                                              name="pancard"
                                              type="file"
                                              // accept="[object Object]"
                                              className="sr-only"
                                              multiple=""
                                              onChange={onChangePan}
                                            />
                                          </label>
                                        </div>
                                        <p className={"error__feedback"}>
                                          {errors.pancard}
                                        </p>
                                        <div>
                                                <img src={previewpancard} />
                                              </div>
                                      </label>
                                    </div>
                                    <div className="space-y-6"></div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <div className="mt-6">
                              <button
                                type="type"
                                disabled=""
                                className="app-btn app-btn-primary min-w-[230px]"
                                data-testid="submit"
                              >
                                <span>Continue</span>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="border border-gray-300 rounded-md mb-4">
                        <div
                          className="accordion-button flex items-center justify-between w-full text-left text-gray-400"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target="#collapseFour"
                          // aria-expanded="true"
                          aria-controls="collapseFour"
                          role="presentation"
                        >
                          <span className="text-xl font-normal app-page-title">
                            Company TAN Card
                          </span>
                          <div className="flex items-center text-sm">
                            In-Progress
                          </div>
                        </div>
                        <div
                          id="collapseFour"
                          className="accordion-collapse collapse "
                          aria-labelledby="headingFour"
                          data-bs-parent="#accordionExample"
                        >
                          <div className="p-4 border-t border-gray-300">
                            <div className="">
                              <div className="">
                                <label htmlFor="" className="relative block col-span-6">
                                  <div className="app-form-wrapper">
                                    <span
                                      data-testid="_label"
                                      className="app-form-label"
                                    ></span>
                                  </div>
                                  <div className="relative space-y-6">
                                    <div className="relative text-center border border-dashed border-primary-100 rounded-xl">
                                      <label
                                        htmlFor="document-eknpj"
                                        className="font-medium cursor-pointer"
                                      >
                                        <div className="px-8 py-6">
                                          <svg
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            className="mx-auto"
                                          >
                                            <path
                                              d="M12.0009 12.586L16.2439 16.828L14.8289 18.243L13.0009 16.415V22H11.0009V16.413L9.17287 18.243L7.75787 16.828L12.0009 12.586ZM12.0009 2C13.7179 2.00008 15.3749 2.63111 16.6571 3.77312C17.9392 4.91512 18.757 6.48846 18.9549 8.194C20.1991 8.53332 21.2846 9.2991 22.0215 10.3575C22.7585 11.416 23.1 12.6997 22.9865 13.9844C22.873 15.2691 22.3116 16.473 21.4004 17.3858C20.4893 18.2986 19.2864 18.8622 18.0019 18.978V16.964C18.462 16.8983 18.9045 16.7416 19.3035 16.503C19.7024 16.2644 20.0498 15.9487 20.3254 15.5744C20.6011 15.2001 20.7993 14.7746 20.9087 14.3228C21.0181 13.8711 21.0364 13.402 20.9626 12.9431C20.8887 12.4841 20.7242 12.0445 20.4786 11.6498C20.233 11.2552 19.9112 10.9134 19.5321 10.6445C19.1529 10.3755 18.724 10.1848 18.2704 10.0834C17.8167 9.98203 17.3474 9.97203 16.8899 10.054C17.0465 9.32489 17.038 8.56997 16.8651 7.84455C16.6922 7.11913 16.3592 6.44158 15.8905 5.86153C15.4218 5.28147 14.8293 4.81361 14.1563 4.49219C13.4834 4.17078 12.7471 4.00397 12.0014 4.00397C11.2556 4.00397 10.5193 4.17078 9.8464 4.49219C9.17347 4.81361 8.58097 5.28147 8.11227 5.86153C7.64358 6.44158 7.31058 7.11913 7.13765 7.84455C6.96473 8.56997 6.95626 9.32489 7.11287 10.054C6.20053 9.88267 5.25749 10.0808 4.49121 10.6048C3.72494 11.1287 3.1982 11.9357 3.02687 12.848C2.85554 13.7603 3.05366 14.7034 3.57763 15.4697C4.10161 16.2359 4.90853 16.7627 5.82087 16.934L6.00087 16.964V18.978C4.71632 18.8623 3.51328 18.2989 2.602 17.3862C1.69073 16.4735 1.1292 15.2696 1.01554 13.9848C0.901892 12.7001 1.24335 11.4163 1.98024 10.3578C2.71713 9.29926 3.80258 8.53339 5.04687 8.194C5.24458 6.48838 6.06228 4.91491 7.34445 3.77287C8.62662 2.63082 10.2838 1.99986 12.0009 2Z"
                                              fill="#003B70"
                                            ></path>
                                          </svg>
                                          <h4 className="mt-2 mb-2 app-text-sub-main-link text-primary whitespace-nowrap">
                                            Drag and drop your files here
                                          </h4>
                                          <p className="mb-2 text-gray-200 app-text-input-secondary">
                                            Max. file size 5MB
                                          </p>
                                          <label
                                            htmlFor="document-eknpj"
                                            className="w-full app-btn app-btn-primary-outline cursor-pointer"
                                          >
                                            <Input
                                              id="tancard"
                                              name="tancard"
                                              type="file"
                                              // accept="[object Object]"
                                              className="sr-only"
                                              multiple=""
                                            />
                                          </label>
                                        </div>
                                        <p className={"error__feedback"}>
                                          {errors.tancard}
                                        </p>
                                      </label>
                                    </div>
                                    <div className="space-y-6"></div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <div className="mt-6">
                              <button
                                type="type"
                                disabled=""
                                className="app-btn app-btn-primary min-w-[230px]"
                                data-testid="submit"
                              >
                                <span>Continue</span>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="border border-gray-300 rounded-md mb-4">
                        <div
                          className="accordion-button flex items-center justify-between w-full text-left text-gray-400"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target="#collapseFive"
                          // aria-expanded="true"
                          aria-controls="collapseFive"
                          role="presentation"
                        >
                          <span className="text-xl font-normal app-page-title">
                            Form 16
                          </span>
                          <div className="flex items-center text-sm">
                            In-Progress
                          </div>
                        </div>
                        <div
                          id="collapseFive"
                          className="accordion-collapse collapse "
                          aria-labelledby="headingFive"
                          data-bs-parent="#accordionExample"
                        >
                          <div className="p-4 border-t border-gray-300">
                            <div className="">
                              <div className="">
                                <label htmlFor="" className="relative block col-span-6">
                                  <div className="app-form-wrapper">
                                    <span
                                      data-testid="_label"
                                      className="app-form-label"
                                    ></span>
                                  </div>
                                  <div className="relative space-y-6">
                                    <div className="relative text-center border border-dashed border-primary-100 rounded-xl">
                                      <label
                                        htmlFor="document-eknpj"
                                        className="font-medium cursor-pointer"
                                      >
                                        <div className="px-8 py-6">
                                          <svg
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            className="mx-auto"
                                          >
                                            <path
                                              d="M12.0009 12.586L16.2439 16.828L14.8289 18.243L13.0009 16.415V22H11.0009V16.413L9.17287 18.243L7.75787 16.828L12.0009 12.586ZM12.0009 2C13.7179 2.00008 15.3749 2.63111 16.6571 3.77312C17.9392 4.91512 18.757 6.48846 18.9549 8.194C20.1991 8.53332 21.2846 9.2991 22.0215 10.3575C22.7585 11.416 23.1 12.6997 22.9865 13.9844C22.873 15.2691 22.3116 16.473 21.4004 17.3858C20.4893 18.2986 19.2864 18.8622 18.0019 18.978V16.964C18.462 16.8983 18.9045 16.7416 19.3035 16.503C19.7024 16.2644 20.0498 15.9487 20.3254 15.5744C20.6011 15.2001 20.7993 14.7746 20.9087 14.3228C21.0181 13.8711 21.0364 13.402 20.9626 12.9431C20.8887 12.4841 20.7242 12.0445 20.4786 11.6498C20.233 11.2552 19.9112 10.9134 19.5321 10.6445C19.1529 10.3755 18.724 10.1848 18.2704 10.0834C17.8167 9.98203 17.3474 9.97203 16.8899 10.054C17.0465 9.32489 17.038 8.56997 16.8651 7.84455C16.6922 7.11913 16.3592 6.44158 15.8905 5.86153C15.4218 5.28147 14.8293 4.81361 14.1563 4.49219C13.4834 4.17078 12.7471 4.00397 12.0014 4.00397C11.2556 4.00397 10.5193 4.17078 9.8464 4.49219C9.17347 4.81361 8.58097 5.28147 8.11227 5.86153C7.64358 6.44158 7.31058 7.11913 7.13765 7.84455C6.96473 8.56997 6.95626 9.32489 7.11287 10.054C6.20053 9.88267 5.25749 10.0808 4.49121 10.6048C3.72494 11.1287 3.1982 11.9357 3.02687 12.848C2.85554 13.7603 3.05366 14.7034 3.57763 15.4697C4.10161 16.2359 4.90853 16.7627 5.82087 16.934L6.00087 16.964V18.978C4.71632 18.8623 3.51328 18.2989 2.602 17.3862C1.69073 16.4735 1.1292 15.2696 1.01554 13.9848C0.901892 12.7001 1.24335 11.4163 1.98024 10.3578C2.71713 9.29926 3.80258 8.53339 5.04687 8.194C5.24458 6.48838 6.06228 4.91491 7.34445 3.77287C8.62662 2.63082 10.2838 1.99986 12.0009 2Z"
                                              fill="#003B70"
                                            ></path>
                                          </svg>
                                          <h4 className="mt-2 mb-2 app-text-sub-main-link text-primary whitespace-nowrap">
                                            Drag and drop your files here
                                          </h4>
                                          <p className="mb-2 text-gray-200 app-text-input-secondary">
                                            Max. file size 5MB
                                          </p>
                                          <label
                                            htmlFor="document-eknpj"
                                            className="w-full app-btn app-btn-primary-outline cursor-pointer"
                                          >
                                            <Input
                                              id="form"
                                              name="form"
                                              type="file"
                                              accept="[object Object]"
                                              className="sr-only"
                                              multiple=""
                                            />
                                          </label>
                                        </div>
                                        <p className={"error__feedback"}>
                                          {errors.form}
                                        </p>
                                      </label>
                                    </div>

                                    <div className="space-y-6"></div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <div className="mt-6">
                              <button
                                type="type"
                                disabled=""
                                className="app-btn app-btn-primary min-w-[230px]"
                                data-testid="submit"
                              >
                                <span>Continue</span>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="border border-gray-300 rounded-md mb-4">
                        <div
                          className="accordion-button flex items-center justify-between w-full text-left text-gray-400"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target="#collapseSix"
                          // aria-expanded="true"
                          aria-controls="collapseSix"
                          role="presentation"
                        >
                          <span className="text-xl font-normal app-page-title">
                            ITR
                          </span>
                          <div className="flex items-center text-sm">
                            In-Progress
                          </div>
                        </div>
                        <div
                          id="collapseSix"
                          className="accordion-collapse collapse "
                          aria-labelledby="headingSix"
                          data-bs-parent="#accordionExample"
                        >
                          <div className="p-4 border-t border-gray-300">
                            <div className="flex flex-col mb-3 md:flex-row md:items-center md:mb-6 gap-x-4 md:gap-x-20 gap-y-3 md:w-full">
                              <div className="flex items-center space-x-2">
                                <input
                                  id="bank-statement-type0"
                                  name="checkbox"
                                  type="radio"
                                  value="upload"
                                  checked=""
                                />
                                <label
                                  htmlFor="bank-statement-type0"
                                  className="app-text-dm-small text-gray text-opacity-80"
                                >
                                  Upload
                                </label>
                              </div>
                              <div className="flex items-center space-x-2">
                                <input
                                  id="bank-statement-type1"
                                  name="checkbox"
                                  type="radio"
                                  value="it-portal"
                                />
                                <label
                                  htmlFor="bank-statement-type1"
                                  className="app-text-dm-small text-gray text-opacity-80"
                                >
                                  IT Portal
                                </label>
                              </div>
                            </div>
                            <div className="p-4 border-t border-gray-300">
                              <div className="px-[10px] py-2 border border-secondary rounded-full bg-secondary-400 mb-5">
                                <div className="flex items-center">
                                  <div className="flex-shrink-0">
                                    <img
                                      src={info}
                                      className="w-[18px] h-[18px]"
                                      alt="info"
                                    />
                                  </div>
                                  <div className="ml-2">
                                    <p className="app-text-dm-small text-gray text-opacity-80">
                                      Please provide the complete ITR (incl.
                                      Saral Page, Profit Loss, Balance Sheet and
                                      all other applicable documents).For
                                      salaried professional, simply upload your
                                      last three year ITR
                                    </p>
                                  </div>
                                </div>
                              </div>
                              <div className="">
                                <div className="">
                                  <label
                                    htmlFor=""
                                    className="relative block col-span-6"
                                  >
                                    <div className="app-form-wrapper">
                                      <span
                                        data-testid="_label"
                                        className="app-form-label"
                                      ></span>
                                    </div>
                                    <div className="relative space-y-6">
                                      <div className="relative text-center border border-dashed border-primary-100 rounded-xl">
                                        <label
                                          htmlFor="document-vass7"
                                          className="font-medium cursor-pointer"
                                        >
                                          <div className="px-8 py-6">
                                            <svg
                                              width="24"
                                              height="24"
                                              viewBox="0 0 24 24"
                                              fill="none"
                                              className="mx-auto"
                                            >
                                              <path
                                                d="M12.0009 12.586L16.2439 16.828L14.8289 18.243L13.0009 16.415V22H11.0009V16.413L9.17287 18.243L7.75787 16.828L12.0009 12.586ZM12.0009 2C13.7179 2.00008 15.3749 2.63111 16.6571 3.77312C17.9392 4.91512 18.757 6.48846 18.9549 8.194C20.1991 8.53332 21.2846 9.2991 22.0215 10.3575C22.7585 11.416 23.1 12.6997 22.9865 13.9844C22.873 15.2691 22.3116 16.473 21.4004 17.3858C20.4893 18.2986 19.2864 18.8622 18.0019 18.978V16.964C18.462 16.8983 18.9045 16.7416 19.3035 16.503C19.7024 16.2644 20.0498 15.9487 20.3254 15.5744C20.6011 15.2001 20.7993 14.7746 20.9087 14.3228C21.0181 13.8711 21.0364 13.402 20.9626 12.9431C20.8887 12.4841 20.7242 12.0445 20.4786 11.6498C20.233 11.2552 19.9112 10.9134 19.5321 10.6445C19.1529 10.3755 18.724 10.1848 18.2704 10.0834C17.8167 9.98203 17.3474 9.97203 16.8899 10.054C17.0465 9.32489 17.038 8.56997 16.8651 7.84455C16.6922 7.11913 16.3592 6.44158 15.8905 5.86153C15.4218 5.28147 14.8293 4.81361 14.1563 4.49219C13.4834 4.17078 12.7471 4.00397 12.0014 4.00397C11.2556 4.00397 10.5193 4.17078 9.8464 4.49219C9.17347 4.81361 8.58097 5.28147 8.11227 5.86153C7.64358 6.44158 7.31058 7.11913 7.13765 7.84455C6.96473 8.56997 6.95626 9.32489 7.11287 10.054C6.20053 9.88267 5.25749 10.0808 4.49121 10.6048C3.72494 11.1287 3.1982 11.9357 3.02687 12.848C2.85554 13.7603 3.05366 14.7034 3.57763 15.4697C4.10161 16.2359 4.90853 16.7627 5.82087 16.934L6.00087 16.964V18.978C4.71632 18.8623 3.51328 18.2989 2.602 17.3862C1.69073 16.4735 1.1292 15.2696 1.01554 13.9848C0.901892 12.7001 1.24335 11.4163 1.98024 10.3578C2.71713 9.29926 3.80258 8.53339 5.04687 8.194C5.24458 6.48838 6.06228 4.91491 7.34445 3.77287C8.62662 2.63082 10.2838 1.99986 12.0009 2Z"
                                                fill="#003B70"
                                              ></path>
                                            </svg>
                                            <h4 className="mt-2 mb-2 app-text-sub-main-link text-primary whitespace-nowrap">
                                              Drag and drop your files here
                                            </h4>
                                            <p className="mb-2 text-gray-200 app-text-input-secondary">
                                              Max. file size 5MB
                                            </p>
                                            <label
                                              htmlFor="document-vass7"
                                              className="w-full app-btn app-btn-primary-outline cursor-pointer"
                                            >
                                              <Input
                                                id="itr"
                                                name="itr"
                                                type="file"
                                                // accept="[object Object]"
                                                className="sr-only"
                                                multiple=""
                                              />
                                            </label>
                                          </div>
                                          <p className={"error__feedback"}>
                                            {errors.itr}
                                          </p>
                                        </label>
                                      </div>
                                      <div className="space-y-6"></div>
                                    </div>
                                  </label>
                                </div>
                              </div>
                              <div className="mt-6">
                                <button
                                  className="app-btn app-btn-primary min-w-[230px]"
                                  type="button"
                                  disabled=""
                                  data-testid="submit"
                                >
                                  <span>Continue</span>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="border border-gray-300 rounded-md mb-4">
                        <div
                          className="accordion-button flex items-center justify-between w-full text-left text-gray-400"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target="#collapseEight"
                          // aria-expanded="true"
                          aria-controls="collapseEight"
                          role="presentation"
                        >
                          <span className="text-xl font-normal app-page-title">
                            PAN Card
                          </span>
                          <div className="flex items-center text-sm">
                            In-Progress
                          </div>
                        </div>
                        <div
                          id="collapseEight"
                          className="accordion-collapse collapse "
                          aria-labelledby="heading"
                          data-bs-parent="#accordionExample"
                        >
                          <div className="p-4 border-t border-gray-300">
                            <div className="">
                              <div className="">
                                <label htmlFor="" className="relative block col-span-6">
                                  <div className="app-form-wrapper">
                                    <span
                                      data-testid="_label"
                                      className="app-form-label"
                                    ></span>
                                  </div>
                                  <div className="relative space-y-6">
                                    <div className="relative text-center border border-dashed border-primary-100 rounded-xl">
                                      <label
                                        htmlFor="document-1bbhf"
                                        className="font-medium cursor-pointer"
                                      >
                                        <div className="px-8 py-6">
                                          <svg
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            className="mx-auto"
                                          >
                                            <path
                                              d="M12.0009 12.586L16.2439 16.828L14.8289 18.243L13.0009 16.415V22H11.0009V16.413L9.17287 18.243L7.75787 16.828L12.0009 12.586ZM12.0009 2C13.7179 2.00008 15.3749 2.63111 16.6571 3.77312C17.9392 4.91512 18.757 6.48846 18.9549 8.194C20.1991 8.53332 21.2846 9.2991 22.0215 10.3575C22.7585 11.416 23.1 12.6997 22.9865 13.9844C22.873 15.2691 22.3116 16.473 21.4004 17.3858C20.4893 18.2986 19.2864 18.8622 18.0019 18.978V16.964C18.462 16.8983 18.9045 16.7416 19.3035 16.503C19.7024 16.2644 20.0498 15.9487 20.3254 15.5744C20.6011 15.2001 20.7993 14.7746 20.9087 14.3228C21.0181 13.8711 21.0364 13.402 20.9626 12.9431C20.8887 12.4841 20.7242 12.0445 20.4786 11.6498C20.233 11.2552 19.9112 10.9134 19.5321 10.6445C19.1529 10.3755 18.724 10.1848 18.2704 10.0834C17.8167 9.98203 17.3474 9.97203 16.8899 10.054C17.0465 9.32489 17.038 8.56997 16.8651 7.84455C16.6922 7.11913 16.3592 6.44158 15.8905 5.86153C15.4218 5.28147 14.8293 4.81361 14.1563 4.49219C13.4834 4.17078 12.7471 4.00397 12.0014 4.00397C11.2556 4.00397 10.5193 4.17078 9.8464 4.49219C9.17347 4.81361 8.58097 5.28147 8.11227 5.86153C7.64358 6.44158 7.31058 7.11913 7.13765 7.84455C6.96473 8.56997 6.95626 9.32489 7.11287 10.054C6.20053 9.88267 5.25749 10.0808 4.49121 10.6048C3.72494 11.1287 3.1982 11.9357 3.02687 12.848C2.85554 13.7603 3.05366 14.7034 3.57763 15.4697C4.10161 16.2359 4.90853 16.7627 5.82087 16.934L6.00087 16.964V18.978C4.71632 18.8623 3.51328 18.2989 2.602 17.3862C1.69073 16.4735 1.1292 15.2696 1.01554 13.9848C0.901892 12.7001 1.24335 11.4163 1.98024 10.3578C2.71713 9.29926 3.80258 8.53339 5.04687 8.194C5.24458 6.48838 6.06228 4.91491 7.34445 3.77287C8.62662 2.63082 10.2838 1.99986 12.0009 2Z"
                                              fill="#003B70"
                                            ></path>
                                          </svg>
                                          <h4 className="mt-2 mb-2 app-text-sub-main-link text-primary whitespace-nowrap">
                                            Drag and drop your files here
                                          </h4>
                                          <p className="mb-2 text-gray-200 app-text-input-secondary">
                                            Max. file size 5MB
                                          </p>
                                          <label
                                            htmlFor="document-1bbhf"
                                            className="w-full app-btn app-btn-primary-outline cursor-pointer"
                                          >
                                            <Input
                                              id="subpan"
                                              name="subpan"
                                              type="file"
                                              // accept="[object Object]"
                                              className="sr-only"
                                              multiple=""
                                            />
                                          </label>
                                        </div>
                                        <p className={"error__feedback"}>
                                          {errors.subpan}
                                        </p>
                                      </label>
                                    </div>
                                    <div className="space-y-6"></div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <div className="mt-6">
                              <button
                                type="type"
                                disabled=""
                                className="app-btn app-btn-primary min-w-[230px]"
                                data-testid="submit"
                              >
                                <span>Continue</span>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      {/* <div className="border border-gray-300 rounded-md mb-4">
                  <div
                    className="flex justify-between p-4 cursor-pointer"
                    role="presentation"
                  >
                    <span className="text-xl font-normal app-page-title">
                      Professional Information
                    </span>
                    <div className="flex items-center text-sm">In-Progress</div>
                  </div>
                  <div className="hidden"></div>
                </div> */}
                      <div className="border border-gray-300 rounded-md mb-4">
                        <div
                          className="accordion-button flex items-center justify-between w-full text-left text-gray-400"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target="#collapseNine"
                          // aria-expanded="true"
                          aria-controls="collapseNine"
                          role="presentation"
                        >
                          <span className="text-xl font-normal app-page-title">
                            Proof of Relationship
                          </span>
                          <div className="flex items-center text-sm">
                            In-Progress
                          </div>
                        </div>
                        <div
                          id="collapseNine"
                          className="accordion-collapse collapse "
                          aria-labelledby="heading"
                          data-bs-parent="#accordionExample"
                        >
                          <div className="p-4 border-t border-gray-300">
                            <div className="">
                              <div className="">
                                <label htmlFor="" className="relative block col-span-6">
                                  <div className="app-form-wrapper">
                                    <span
                                      data-testid="_label"
                                      className="app-form-label"
                                    ></span>
                                  </div>
                                  <div className="relative space-y-6">
                                    <div className="relative text-center border border-dashed border-primary-100 rounded-xl">
                                      <label
                                        htmlFor="document-8fcy0"
                                        className="font-medium cursor-pointer"
                                      >
                                        <div className="px-8 py-6">
                                          <svg
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            className="mx-auto"
                                          >
                                            <path
                                              d="M12.0009 12.586L16.2439 16.828L14.8289 18.243L13.0009 16.415V22H11.0009V16.413L9.17287 18.243L7.75787 16.828L12.0009 12.586ZM12.0009 2C13.7179 2.00008 15.3749 2.63111 16.6571 3.77312C17.9392 4.91512 18.757 6.48846 18.9549 8.194C20.1991 8.53332 21.2846 9.2991 22.0215 10.3575C22.7585 11.416 23.1 12.6997 22.9865 13.9844C22.873 15.2691 22.3116 16.473 21.4004 17.3858C20.4893 18.2986 19.2864 18.8622 18.0019 18.978V16.964C18.462 16.8983 18.9045 16.7416 19.3035 16.503C19.7024 16.2644 20.0498 15.9487 20.3254 15.5744C20.6011 15.2001 20.7993 14.7746 20.9087 14.3228C21.0181 13.8711 21.0364 13.402 20.9626 12.9431C20.8887 12.4841 20.7242 12.0445 20.4786 11.6498C20.233 11.2552 19.9112 10.9134 19.5321 10.6445C19.1529 10.3755 18.724 10.1848 18.2704 10.0834C17.8167 9.98203 17.3474 9.97203 16.8899 10.054C17.0465 9.32489 17.038 8.56997 16.8651 7.84455C16.6922 7.11913 16.3592 6.44158 15.8905 5.86153C15.4218 5.28147 14.8293 4.81361 14.1563 4.49219C13.4834 4.17078 12.7471 4.00397 12.0014 4.00397C11.2556 4.00397 10.5193 4.17078 9.8464 4.49219C9.17347 4.81361 8.58097 5.28147 8.11227 5.86153C7.64358 6.44158 7.31058 7.11913 7.13765 7.84455C6.96473 8.56997 6.95626 9.32489 7.11287 10.054C6.20053 9.88267 5.25749 10.0808 4.49121 10.6048C3.72494 11.1287 3.1982 11.9357 3.02687 12.848C2.85554 13.7603 3.05366 14.7034 3.57763 15.4697C4.10161 16.2359 4.90853 16.7627 5.82087 16.934L6.00087 16.964V18.978C4.71632 18.8623 3.51328 18.2989 2.602 17.3862C1.69073 16.4735 1.1292 15.2696 1.01554 13.9848C0.901892 12.7001 1.24335 11.4163 1.98024 10.3578C2.71713 9.29926 3.80258 8.53339 5.04687 8.194C5.24458 6.48838 6.06228 4.91491 7.34445 3.77287C8.62662 2.63082 10.2838 1.99986 12.0009 2Z"
                                              fill="#003B70"
                                            ></path>
                                          </svg>
                                          <h4 className="mt-2 mb-2 app-text-sub-main-link text-primary whitespace-nowrap">
                                            Drag and drop your files here
                                          </h4>
                                          <p className="mb-2 text-gray-200 app-text-input-secondary">
                                            Max. file size 5MB
                                          </p>
                                          <label
                                            htmlFor="document-8fcy0"
                                            className="w-full app-btn app-btn-primary-outline cursor-pointer"
                                          >
                                            <Input
                                              id="relation"
                                              name="relation"
                                              type="file"
                                              // accept="[object Object]"
                                              className="sr-only"
                                              multiple=""
                                            />
                                          </label>
                                        </div>
                                        <p className={"error__feedback"}>
                                          {errors.relation}
                                        </p>
                                      </label>
                                    </div>
                                    <div className="space-y-6"></div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <div className="mt-6">
                              <button
                                type="type"
                                disabled=""
                                className="app-btn app-btn-primary min-w-[230px]"
                                data-testid="submit"
                              >
                                <span>Continue</span>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="border border-gray-300 rounded-md mb-4">
                        <div
                          className="accordion-button flex items-center justify-between w-full text-left text-gray-400"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target="#collapseEleven"
                          // aria-expanded="true"
                          aria-controls="collapseEleven"
                          role="presentation"
                        >
                          <span className="text-xl font-normal app-page-title">
                            Salary Slip
                          </span>
                          <div className="flex items-center text-sm">
                            In-Progress
                          </div>
                        </div>
                        <div
                          id="collapseEleven"
                          className="accordion-collapse collapse "
                          aria-labelledby="heading"
                          data-bs-parent="#accordionExample"
                        >
                          <div className="p-4 border-t border-gray-300">
                            <div className="pt-6">
                              <div className="px-[10px] py-2 border border-secondary rounded-full bg-secondary-400 mb-5">
                                <div className="flex items-center">
                                  <div className="flex-shrink-0">
                                    <img
                                      src={info}
                                      className="w-[18px] h-[18px]"
                                      alt="girt"
                                    />
                                  </div>
                                  <div className="ml-2">
                                    <p className="app-text-dm-small text-gray text-opacity-80">
                                      Please upload your ctc/offer letter or
                                      last three months salary slips
                                    </p>
                                  </div>
                                </div>
                              </div>
                              <div className="">
                                <div className="">
                                  <label
                                    htmlFor=""
                                    className="relative block col-span-6"
                                  >
                                    <div className="app-form-wrapper">
                                      <span
                                        data-testid="_label"
                                        className="app-form-label"
                                      ></span>
                                    </div>
                                    <div className="relative space-y-6">
                                      <div className="relative text-center border border-dashed border-primary-100 rounded-xl">
                                        <label
                                          htmlFor="document-13mw4"
                                          className="font-medium cursor-pointer"
                                        >
                                          <div className="px-8 py-6">
                                            <svg
                                              width="24"
                                              height="24"
                                              viewBox="0 0 24 24"
                                              fill="none"
                                              className="mx-auto"
                                            >
                                              <path
                                                d="M12.0009 12.586L16.2439 16.828L14.8289 18.243L13.0009 16.415V22H11.0009V16.413L9.17287 18.243L7.75787 16.828L12.0009 12.586ZM12.0009 2C13.7179 2.00008 15.3749 2.63111 16.6571 3.77312C17.9392 4.91512 18.757 6.48846 18.9549 8.194C20.1991 8.53332 21.2846 9.2991 22.0215 10.3575C22.7585 11.416 23.1 12.6997 22.9865 13.9844C22.873 15.2691 22.3116 16.473 21.4004 17.3858C20.4893 18.2986 19.2864 18.8622 18.0019 18.978V16.964C18.462 16.8983 18.9045 16.7416 19.3035 16.503C19.7024 16.2644 20.0498 15.9487 20.3254 15.5744C20.6011 15.2001 20.7993 14.7746 20.9087 14.3228C21.0181 13.8711 21.0364 13.402 20.9626 12.9431C20.8887 12.4841 20.7242 12.0445 20.4786 11.6498C20.233 11.2552 19.9112 10.9134 19.5321 10.6445C19.1529 10.3755 18.724 10.1848 18.2704 10.0834C17.8167 9.98203 17.3474 9.97203 16.8899 10.054C17.0465 9.32489 17.038 8.56997 16.8651 7.84455C16.6922 7.11913 16.3592 6.44158 15.8905 5.86153C15.4218 5.28147 14.8293 4.81361 14.1563 4.49219C13.4834 4.17078 12.7471 4.00397 12.0014 4.00397C11.2556 4.00397 10.5193 4.17078 9.8464 4.49219C9.17347 4.81361 8.58097 5.28147 8.11227 5.86153C7.64358 6.44158 7.31058 7.11913 7.13765 7.84455C6.96473 8.56997 6.95626 9.32489 7.11287 10.054C6.20053 9.88267 5.25749 10.0808 4.49121 10.6048C3.72494 11.1287 3.1982 11.9357 3.02687 12.848C2.85554 13.7603 3.05366 14.7034 3.57763 15.4697C4.10161 16.2359 4.90853 16.7627 5.82087 16.934L6.00087 16.964V18.978C4.71632 18.8623 3.51328 18.2989 2.602 17.3862C1.69073 16.4735 1.1292 15.2696 1.01554 13.9848C0.901892 12.7001 1.24335 11.4163 1.98024 10.3578C2.71713 9.29926 3.80258 8.53339 5.04687 8.194C5.24458 6.48838 6.06228 4.91491 7.34445 3.77287C8.62662 2.63082 10.2838 1.99986 12.0009 2Z"
                                                fill="#003B70"
                                              ></path>
                                            </svg>
                                            <h4 className="mt-2 mb-2 app-text-sub-main-link text-primary whitespace-nowrap">
                                              Drag and drop your files here
                                            </h4>
                                            <p className="mb-2 text-gray-200 app-text-input-secondary">
                                              Max. file size 5MB
                                            </p>
                                            <label
                                              htmlFor="document-13mw4"
                                              className="w-full app-btn app-btn-primary-outline cursor-pointer"
                                            >
                                              <Input
                                                id="salaryslip"
                                                name="salaryslip"
                                                type="file"
                                                // accept="[object Object]"
                                                className="sr-only"
                                                multiple=""
                                              />
                                            </label>
                                          </div>
                                          <p className={"error__feedback"}>
                                            {errors.salaryslip}
                                          </p>
                                        </label>
                                      </div>
                                      <div className="space-y-6"></div>
                                    </div>
                                  </label>
                                </div>
                              </div>
                              <div className="mt-6">
                                <button
                                  className="app-btn app-btn-primary min-w-[230px]"
                                  type="button"
                                  data-testid="submit"
                                  disabled=""
                                >
                                  <span>Continue</span>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </dd>
                  </div>
                </div>
              </div>
              <div className="my-5 sm:my-6">
                <button
                  className="app-btn app-btn-primary w-full sm:w-auto sm:min-w-[300px]"
                  type="submit"
                  data-testid="get-scanction-button"
                  disabled=""
                >
                  <span>Get your home loan eligibility advice</span>
                </button>
              </div>
            </div>

            <div
              className={
                "form__item button__items d-flex justify-content-between"
              }
            >
              <Button
                type={"default"}
                onClick={prev}
                className="w-auto space-x-2 app-btn app-btn-primary"
                data-testid="submit"
                data-category="unlock_credit_score"
                data-action="Unlock credit score"
                data-label="/personal-detail"
              >
                <span>Back</span>
              </Button>
              <Button
                type={"primary"}
                onClick={handleSubmit}
                className="w-auto space-x-2 app-btn app-btn-primary"
                data-testid="submit"
                data-category="unlock_credit_score"
                data-action="Unlock credit score"
                data-label="/personal-detail"
              >
                submit
              </Button>
            </div>
          </div>
        );
      }}
    </Formik>
  );
};
export default Documentverify;
