import React, { useContext, useState } from "react";
import { Formik, Field } from "formik";
import { Button } from "antd";
import { Input } from "formik-antd";
import DatePickerField from "react-datepicker";
import MultiStepFormContext from "../MultiStepFormContext";
import Header from "../../MultiStepForm/header/Header";
import "../../../assets/css/personaldetails/personaldetails.css";
import "../../../assets/css/personaldetails/personaldetails2.css";
import cake from "../../../assets/images/cake.webp";
import unlock from "../../../assets/images/unlock.png";
import construction from "../../../assets/images/construction.png";
import house from "../../../assets/images/house.png";

import { Link } from "react-router-dom";

const Personaldetails = () => {
  const { personaldetails, setPersonaldetails, next, prev } =
    useContext(MultiStepFormContext);
  // const [value, onChange] = useState(new Date());

  // const [choose, setChoose] = useState(false);

  // const handleChange = (event) => {
  //   setChoose(event.target.value)
  //   console.log(choose)
  // }
  return (
    <Formik
      initialValues={personaldetails}
      onSubmit={(values) => {
        setPersonaldetails(values);
        next();
      }}
      validate={(values) => {
        const errors = {};
        if (!values.name) errors.name = "Full name is required";
        if (!values.gender) errors.gender = "gender is Required";
        if (!values.date) errors.date = "date of birth is  required";
        if (!values.marriedstatus)
          errors.marriedstatus = "Married Status is required";

        if (!values.email) errors.email = "Email id  is required";
        if (!values.pannum) errors.pannum = "pan number  is required";
        if (!values.mobilenum) errors.mobilenum = "Mobile number is required";
        if (!values.address) errors.address = "Address is required";
        if (!values.pincode) errors.pincode = "Pin code  is required";
        if (!values.state) errors.state = "State is required";
        if (!values.city) errors.city = "city  is required";
       if(!values.propname) errors.propname ="property name is required"
       if(!values.projname) errors.projname ="project name is required"
       if(!values.nameofbuild) errors.nameofbuild="name of building is required"
       if(!values.locality) errors.locality="Locality is Required"
        return errors;
      }}
    >
      {({ handleSubmit, errors }) => {
        <Header />;
        return (
          <div className={"details__wrapper"}>
            <div id="__next">
              <div className="container relative px-4 sm:px-0 mx-auto mt-24 sm:mt-28 sm:w-7/12">
                <div className="bg-white mt-10">
                  <h1 className="bg-white app-page-title">
                    Welcome aboard! Let's get to know you better
                  </h1>
                  <p className="mt-2 app-page-sub-title">
                    Easiloan uses these details to check your loan eligibility
                    and unlock personalised offers for you
                  </p>
                </div>
                <div className="mx-auto max-w-7xl">
                  <div className="">
                    <dt className="text-lg">
                      <button
                        type="button"
                        className="flex items-center justify-between w-full text-left text-gray-400"
                        aria-controls="647"
                        aria-expanded="true"
                      >
                        <div className="flex items-center py-5 sm:py-6">
                          <span className="mr-3 text-lg font-medium sm:text-xl sm:mr-6 text-primary">
                            Primary Applicant
                          </span>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="w-5 h-5 mr-3 text-green"
                            viewBox="0 0 20 20 "
                            fill="currentColor"
                          >
                            <path
                              fill-rule="evenodd"
                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                              clip-rule="evenodd"
                            ></path>
                          </svg>
                        </div>
                        <div className="flex items-center">
                          <span className="text-sm text-primary-100">
                            <Input name={"name"} />
                          </span>
                          <span className="flex items-center ml-3 sm:ml-5 h-7">
                            <svg
                              className="rotate-180 text-black h-5 w-5 transform"
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              viewBox="0 0 24 24"
                              stroke="currentColor"
                              aria-hidden="true"
                            >
                              <path
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M19 9l-7 7-7-7"
                              ></path>
                            </svg>
                          </span>
                        </div>
                      </button>
                    </dt>
                    <dd className="block" id="647">
                      <form data-testid="form-personal-details">
                        <div className="row">
                          <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4 ">
                            <div
                              className={` form__item ${
                                errors.name && "input__error"
                              }`}
                            >
                              <label
                                htmlhtmlFor="name"
                                className="col-span-6 lg:col-span-2 relative block"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="name_label"
                                    className="app-form-label"
                                  >
                                    Full Name
                                  </span>
                                </div>
                                <div
                                  className="relative rounded-md"
                                  role="presentation"
                                >
                                  <Input
                                    name={"name"}
                                    className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled"
                                    aria-invalid="false"
                                    aria-describedby="name-error"
                                    type="text"
                                    placeholder="Enter name"
                                    data-testid="name"
                                  />
                                </div>
                                <p className={"error__feedback"}>
                                  {errors.name}
                                </p>
                              </label>
                            </div>
                          </div>
                          <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                            <div
                              className={`form__item ${
                                errors.gender && "input__error"
                              }`}
                            >
                              <label
                                htmlFor="employment_type"
                                className="col-span-6 lg:col-span-2"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="employment_type_label"
                                    className="app-form-label"
                                  >
                                    Gender
                                  </span>
                                </div>
                                <label
                                  htmlFor="employment_type"
                                  className="col-span-6 col-start-1 lg:col-span-2"
                                >
                                  <div
                                    className="relative rounded-md app-select"
                                    role="presentation"
                                    data-testid="employment_type"
                                    style={{ width: "350px" }}
                                  >
                                    <div className="el-select-container css-2b097c-container">
                                      <div className="el-select__control css-yk16xz-control">
                                        <div className="el-select__value-container css-1hwfws3 boxwidth">
                                          <div className="el-select__placeholder css-1wa3eu0-placeholder">
                                            <div>
                                              <Field
                                                as="select"
                                                name="gender"
                                                id="gender"
                                                style={{
                                                  border: "none",
                                                }}
                                                // onChange={(e) => handleChange(e)}
                                              >
                                                <option value="" selected>
                                                  Please select one option
                                                </option>
                                                <option value="Male">
                                                  Male
                                                </option>
                                                <option value="Female">
                                                  Female
                                                </option>
                                              </Field>
                                            </div>
                                          </div>

                                          <div className="css-1g6gooi">
                                            <div
                                              className="el-select__input"
                                              style={{
                                                display: "inline-block",
                                              }}
                                            >
                                              <input
                                                autocapitalize="none"
                                                autocomplete="off"
                                                autocorrect="off"
                                                id="react-select-2-input"
                                                spellcheck="false"
                                                tabindex="0"
                                                type="text"
                                                aria-autocomplete="list"
                                                value=""
                                                style={{
                                                  boxsizing: "contentbox",
                                                  width: "2px",
                                                  background: "0px center",
                                                  border: "0px",
                                                  fontsize: "inherit",
                                                  opacity: "1",
                                                  outline: "0px",
                                                  padding: "0px",
                                                  color: "inherit",
                                                }}
                                              />

                                              {/* <div
                                        style={{
                                          position: "absolute",
                                          top: "0px",
                                          left: "0px",
                                          visibility: "hidden",
                                          height: "0px",
                                          overflow: "scroll",
                                          whiteSpace: "pre",
                                          fontSize: "16px",
                                          fontFamily: "DM Sans",
                                          fontWeight: "500",
                                          fontStyle: "normal",
                                          letterSpacing: "normal",
                                          textTransform: "none",
                                        }}
                                      ></div> */}
                                            </div>
                                          </div>
                                        </div>
                                        <div className="el-select__indicators css-1wy0on6">
                                          <span className="el-select__indicator-separator css-1okebmr-indicatorSeparator"></span>

                                          <svg
                                            height="20"
                                            width="20"
                                            viewBox="0 0 20 20"
                                            aria-hidden="true"
                                            focusable="false"
                                            className="css-8mmkcg"
                                          >
                                            <path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path>
                                          </svg>
                                        </div>
                                      </div>

                                      <input
                                        name="employment_type"
                                        type="hidden"
                                        value=""
                                      />
                                    </div>
                                  </div>
                                </label>
                                <p className={"error__feedback"}>
                                  {errors.gender}
                                </p>
                              </label>
                            </div>
                          </div>
                          <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                            <div
                              className={` form__item ${
                                errors.date && "input__error"
                              }`}
                            >
                              <label
                                htmlhtmlFor="dob"
                                className="col-span-6 lg:col-span-2 relative block"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="dob_label"
                                    className="app-form-label"
                                  >
                                    Date of Birth
                                  </span>
                                </div>
                                <div
                                  className="relative rounded-md"
                                  role="presentation"
                                >
                                  <div className="react-datepicker-wrapper">
                                    <div className="react-datepicker__input-container">
                                      {/* <DatePickerField name="date" />  */}

                                      <Input
                                        type="date"
                                        id="start"
                                        name={"date"}
                                        // value="2018-07-22"
                                        min="2022-02-10"
                                        // max="2018-12-31"
                                        className="form-select"
                                      />
                                    </div>
                                  </div>
                                  {/* <div className="absolute right-3 top-3">
                                    <div
                                      style={{
                                        display: "inline-block",
                                        maxWidth: "100",
                                        overflow: "hidden",
                                        position: "relative",
                                        boxSizing: "border-box",
                                        margin: "0px",
                                      }}
                                    >
                                      <div
                                        style={{
                                          boxSizing: "border-box",
                                          display: "block",
                                          maxWidth: "100%",
                                        }}
                                      >
                                        <img
                                          alt=""
                                          aria-hidden="true"
                                          role="presentation"
                                          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjMiIGhlaWdodD0iMjUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+"
                                          style={{
                                            maxWidth: "100%",
                                            display: "block",
                                            margin: "0px",
                                            border: "none",
                                            padding: "0px",
                                          }}
                                        />
                                      </div>
                                      <img
                                        alt="cake-icon"
                                        src={cake}
                                        //   src="/_next/image?url=%2Fimages%2Fcake.png&amp;w=48&amp;q=75"
                                        decoding="async"
                                        style={{
                                          position: "absolute",
                                          inset: "0px",
                                          boxSizing: "border-box",
                                          padding: "0px",
                                          border: "none",
                                          margin: "auto",
                                          display: "block",
                                          width: "0px",
                                          height: "0px",
                                          minWidth: "100%",
                                          maxWidth: "100%",
                                          minHeight: "100%",
                                          maxHeight: "100%",
                                        }}
                                        srcset={`${cake} 300w, ${cake} 1000w`}
                                      />
                                    </div>
                                  </div> */}
                                </div>
                                <p className={"error__feedback"}>
                                  {errors.date}
                                </p>
                              </label>
                            </div>
                          </div>
                        </div>

                        <div className="row">
                          <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                            <div
                              className={`form__item ${
                                errors.marriedstatus && "input__error"
                              }`}
                            >
                              <label
                                htmlFor="employment_type"
                                className="col-span-6 lg:col-span-2"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="employment_type_label"
                                    className="app-form-label"
                                  >
                                    Marital Status
                                  </span>
                                </div>
                                <label
                                  htmlFor="employment_type"
                                  className="col-span-6 col-start-1 lg:col-span-2"
                                >
                                  <div
                                    className="relative rounded-md app-select"
                                    role="presentation"
                                    data-testid="employment_type"
                                    style={{ width: "350px" }}
                                  >
                                    <div className="el-select-container css-2b097c-container">
                                      <div className="el-select__control css-yk16xz-control">
                                        <div className="el-select__value-container css-1hwfws3 boxwidth">
                                          <div className="el-select__placeholder css-1wa3eu0-placeholder">
                                            <div>
                                              <Field
                                                as="select"
                                                name="marriedstatus"
                                                id="marriedstatus"
                                                style={{
                                                  border: "none",
                                                }}
                                                // onChange={(e) => handleChange(e)}
                                              >
                                                <option value="" selected>
                                                  Please Select any option
                                                </option>
                                                <option value="Married">
                                                  Married
                                                </option>
                                                <option value=" Single">
                                                  Single
                                                </option>
                                                <option value=" Seperated">
                                                  Seperated
                                                </option>
                                                <option value=" Divorced">
                                                  Divorced
                                                </option>
                                              </Field>
                                            </div>
                                          </div>

                                          <div className="css-1g6gooi">
                                            <div
                                              className="el-select__input"
                                              style={{
                                                display: "inline-block",
                                              }}
                                            >
                                              <input
                                                autocapitalize="none"
                                                autocomplete="off"
                                                autocorrect="off"
                                                id="react-select-2-input"
                                                spellcheck="false"
                                                tabindex="0"
                                                type="text"
                                                aria-autocomplete="list"
                                                value=""
                                                style={{
                                                  boxsizing: "contentbox",
                                                  width: "2px",
                                                  background: "0px center",
                                                  border: "0px",
                                                  fontsize: "inherit",
                                                  opacity: "1",
                                                  outline: "0px",
                                                  padding: "0px",
                                                  color: "inherit",
                                                }}
                                              />

                                              {/* <div
                                        style={{
                                          position: "absolute",
                                          top: "0px",
                                          left: "0px",
                                          visibility: "hidden",
                                          height: "0px",
                                          overflow: "scroll",
                                          whiteSpace: "pre",
                                          fontSize: "16px",
                                          fontFamily: "DM Sans",
                                          fontWeight: "500",
                                          fontStyle: "normal",
                                          letterSpacing: "normal",
                                          textTransform: "none",
                                        }}
                                      ></div> */}
                                            </div>
                                          </div>
                                        </div>
                                        <div className="el-select__indicators css-1wy0on6">
                                          <span className="el-select__indicator-separator css-1okebmr-indicatorSeparator"></span>

                                          <svg
                                            height="20"
                                            width="20"
                                            viewBox="0 0 20 20"
                                            aria-hidden="true"
                                            focusable="false"
                                            className="css-8mmkcg"
                                          >
                                            <path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path>
                                          </svg>
                                        </div>
                                      </div>

                                      <input
                                        name="employment_type"
                                        type="hidden"
                                        value=""
                                      />
                                    </div>
                                  </div>
                                </label>
                                <p className={"error__feedback"}>
                                  {errors.marriedstatus}
                                </p>
                              </label>
                            </div>
                          </div>
                          <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                            <div
                              className={`form__item ${
                                errors.email && "input__error"
                              }`}
                            >
                              <label
                                htmlhtmlFor="email"
                                className="col-span-6 lg:col-span-2 relative block"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="email_label"
                                    className="app-form-label"
                                  >
                                    Your Email ID
                                  </span>
                                </div>
                                <div
                                  className="relative rounded-md"
                                  role="presentation"
                                >
                                  <Input
                                    className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled"
                                    aria-invalid="false"
                                    aria-describedby="email-error"
                                    name={"email"}
                                    type="text"
                                    placeholder="Report will be sent here"
                                    data-testid="email"
                                  />
                                </div>
                                <p className={"error__feedback"}>
                                  {errors.email}
                                </p>
                              </label>
                            </div>
                          </div>
                          <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                            <div
                              className={`form__item ${
                                errors.pannum && "input__error"
                              }`}
                            >
                              <label
                                htmlhtmlFor="pan_number"
                                className="col-span-6 lg:col-span-2 relative block"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="pan_number_label"
                                    className="app-form-label"
                                  >
                                    Pan Number
                                  </span>
                                </div>
                                <div
                                  className="relative rounded-md"
                                  role="presentation"
                                >
                                  <Input
                                    name={"pannum"}
                                    className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled inspectletIgnore"
                                    aria-invalid="false"
                                    aria-describedby="pan_number-error"
                                    type="text"
                                    placeholder="For eg: ESKPL2389K"
                                    data-testid="pan_number"
                                    // value="ASETY5677Y"
                                  />
                                </div>
                                <p className={"error__feedback"}>
                                  {errors.pannum}
                                </p>
                              </label>
                            </div>
                          </div>
                        </div>

                        <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                          <div
                            className={`form__item ${
                              errors.mobilenum && "input__error"
                            }`}
                          >
                            <label
                              htmlhtmlFor="contact_number"
                              className="col-span-6 lg:col-span-2 relative block"
                            >
                              <div className="app-form-wrapper">
                                <span
                                  data-testid="contact_number_label"
                                  className="app-form-label"
                                >
                                  Mobile No
                                </span>
                              </div>
                              <div
                                className="relative rounded-md"
                                role="presentation"
                              >
                                <Input
                                  name={"mobilenum"}
                                  className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled inspectletIgnore"
                                  aria-invalid="false"
                                  aria-describedby="pan_number-error"
                                  type="number"
                                  placeholder="For eg: ESKPL2389K"
                                  data-testid="pan_number"
                                  // value="ASETY5677Y"
                                />
                              </div>
                              <p className={"error__feedback"}>
                                {errors.mobilenum}
                              </p>
                            </label>
                          </div>
                        </div>
                        <div className="grid col-md-12 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                          <div
                            className={`form__item ${
                              errors.address && "input__error"
                            }`}
                          >
                            <label
                              htmlhtmlFor="address.address1"
                              className="col-span-6"
                            >
                              <div className="app-form-wrapper">
                                <span
                                  data-testid="address.address1_label"
                                  className="app-form-label"
                                >
                                  Permanent Address : Address
                                </span>
                              </div>
                              <div
                                className="relative rounded-md"
                                role="presentation"
                              >
                                <Input
                                  name={"address"}
                                  className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled"
                                  aria-invalid="false"
                                  aria-describedby="address.address1-error"
                                  type="text"
                                  placeholder="Mandatory"
                                  data-testid="address.address1"
                                  // value="Shelar park"
                                />
                              </div>
                              <p className={"error__feedback"}>
                                {errors.address}
                              </p>
                            </label>
                          </div>
                        </div>

                        <div className="row">
                          <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                            <div
                              className={`form__item ${
                                errors.pincode && "input__error"
                              }`}
                            >
                              <label
                                htmlhtmlFor="address.pincode"
                                className="col-span-6 lg:col-span-2"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="address.pincode_label"
                                    className="app-form-label"
                                  >
                                    Pincode
                                  </span>
                                </div>
                                <div
                                  className="relative rounded-md"
                                  role="presentation"
                                >
                                  <Input
                                    name={"pincode"}
                                    className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled"
                                    aria-invalid="false"
                                    aria-describedby="address.pincode-error"
                                    type="number"
                                    placeholder="Enter locality pincode"
                                    data-testid="address.pincode"
                                    // value="421301"
                                  />
                                </div>
                                <p className={"error__feedback"}>
                                  {errors.pincode}
                                </p>
                              </label>
                            </div>
                          </div>
                          <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                            <div
                              className={`form__item ${
                                errors.state && "input__error"
                              }`}
                            >
                              <label
                                htmlhtmlFor="address.state"
                                className="col-span-6 lg:col-span-2 relative block"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="address.state_label"
                                    className="app-form-label"
                                  >
                                    State
                                  </span>
                                </div>
                                <div
                                  className="relative rounded-md"
                                  role="presentation"
                                >
                                  <Input
                                    name={"state"}
                                    className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled"
                                    aria-invalid="false"
                                    aria-describedby="address.pincode-error"
                                    type="text"
                                    placeholder="Enter locality pincode"
                                    data-testid="address.pincode"
                                    // value="421301"
                                  />
                                </div>
                                <p className={"error__feedback"}>
                                  {errors.state}
                                </p>
                              </label>
                            </div>
                          </div>
                          <div className="grid col-md-4  gap-x-8 sm:gap-y-5 gap-y-4"  style={{marginTop:"16px"}}>
                            <div
                              className={`form__item ${
                                errors.city && "input__error"
                              }`}
                            >
                              <label
                                htmlFor="employment_type"
                                className="col-span-6 lg:col-span-2"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="employment_type_label"
                                    className="app-form-label"
                                  >  City
                                  </span>
                                </div>
                                <label
                                  htmlFor="employment_type"
                                  className="col-span-6 col-start-1 lg:col-span-2"
                                >
                                  <div
                                    className="relative rounded-md app-select"
                                    role="presentation"
                                    data-testid="employment_type"
                                    style={{ width: "350px" }}
                                  >
                                    <div className="el-select-container css-2b097c-container">
                                      <div className="el-select__control css-yk16xz-control">
                                        <div className="el-select__value-container css-1hwfws3 boxwidth">
                                          <div className="el-select__placeholder css-1wa3eu0-placeholder">
                                            <div>
                                              <Field
                                                as="select"
                                                name={"city"}
                                                id="marriedstatus"
                                                style={{
                                                  border: "none",
                                                }}
                                                // onChange={(e) => handleChange(e)}
                                              >
                                                <option value="" selected>
                                                  Please Select any option
                                                </option>
                                                <option value="Married">
                                                 Kalyan
                                                </option>
                                                <option value=" Single">
                                                Mumbai
                                                </option>
                                                <option value=" Seperated">
                                                 Thane
                                                </option>
                                                <option value=" Divorced">
                                                Sangli
                                                </option>
                                              </Field>
                                            </div>
                                          </div>

                                          <div className="css-1g6gooi">
                                            <div
                                              className="el-select__input"
                                              style={{
                                                display: "inline-block",
                                              }}
                                            >
                                              <input
                                                autocapitalize="none"
                                                autocomplete="off"
                                                autocorrect="off"
                                                id="react-select-2-input"
                                                spellcheck="false"
                                                tabindex="0"
                                                type="text"
                                                aria-autocomplete="list"
                                                value=""
                                                style={{
                                                  boxsizing: "contentbox",
                                                  width: "2px",
                                                  background: "0px center",
                                                  border: "0px",
                                                  fontsize: "inherit",
                                                  opacity: "1",
                                                  outline: "0px",
                                                  padding: "0px",
                                                  color: "inherit",
                                                }}
                                              />

                                              {/* <div
                                        style={{
                                          position: "absolute",
                                          top: "0px",
                                          left: "0px",
                                          visibility: "hidden",
                                          height: "0px",
                                          overflow: "scroll",
                                          whiteSpace: "pre",
                                          fontSize: "16px",
                                          fontFamily: "DM Sans",
                                          fontWeight: "500",
                                          fontStyle: "normal",
                                          letterSpacing: "normal",
                                          textTransform: "none",
                                        }}
                                      ></div> */}
                                            </div>
                                          </div>
                                        </div>
                                        <div className="el-select__indicators css-1wy0on6">
                                          <span className="el-select__indicator-separator css-1okebmr-indicatorSeparator"></span>

                                          <svg
                                            height="20"
                                            width="20"
                                            viewBox="0 0 20 20"
                                            aria-hidden="true"
                                            focusable="false"
                                            className="css-8mmkcg"
                                          >
                                            <path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path>
                                          </svg>
                                        </div>
                                      </div>

                                      <input
                                        name="employment_type"
                                        type="hidden"
                                        value=""
                                      />
                                    </div>
                                  </div>
                                </label>
                                <p className={"error__feedback"}>
                                  {errors.city}
                                </p>
                              </label>
                            </div>
                          </div>
                  
                        </div>
                        <div className="relative flex items-start mt-2 col-span-full">
                          <input
                            id="same_address-647"
                            name="same_address"
                            type="checkbox"
                            value="true"
                          />
                          <div className="ml-3 text-sm">
                            <label
                              htmlFor="same_address-647"
                              className="font-medium cursor-pointer text-primary"
                            >
                              Current address same as permanent address{" "}
                            </label>
                          </div>
                        </div>

                        <div className="grid grid-cols-6 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                          <label
                            htmlhtmlFor="current_address.address1"
                            className="col-span-6"
                          >
                            <div className="app-form-wrapper">
                              <span
                                data-testid="current_address.address1_label"
                                className="app-form-label"
                              >
                                Current Address
                              </span>
                            </div>
                            <div
                              className="relative rounded-md"
                              role="presentation"
                            >
                              {/* <input
                      
                          className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled"
                          aria-invalid="false"
                          aria-describedby="current_address.address1-error"
                          name="current_address.address1"
                          type="text"
                          placeholder="Mandatory"
                          readonly=""
                          data-testid="current_address.address1"
                        /> */}
                              <Input
                                name={"address"}
                                className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled"
                                aria-invalid="false"
                                aria-describedby="address.address1-error"
                                type="text"
                                placeholder="Mandatory"
                                data-testid="address.address1"
                                // value="Shelar park"
                              />
                            </div>
                          </label>
                        </div>

                        <div className="row">
                          <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                            <label
                              htmlhtmlFor="current_address.pincode"
                              className="col-span-6 lg:col-span-2"
                            >
                              <div className="app-form-wrapper">
                                <span
                                  data-testid="current_address.pincode_label"
                                  className="app-form-label"
                                >
                                  Pincode
                                </span>
                              </div>
                              <div
                                className="relative rounded-md"
                                role="presentation"
                              >
                                <Input
                                  name="pincode"
                                  className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled"
                                  aria-invalid="false"
                                  aria-describedby="current_address.pincode-error"
                                  type="number"
                                  placeholder="Enter locality pincode"
                                  readonly=""
                                  data-testid="current_address.pincode"
                                  // value="421301"
                                />
                              </div>
                            </label>
                          </div>
                          <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4">
                            <label
                              htmlhtmlFor="current_address.state"
                              className="col-span-6 lg:col-span-2 relative block"
                            >
                              <div className="app-form-wrapper">
                                <span
                                  data-testid="current_address.state_label"
                                  className="app-form-label"
                                >
                                  State
                                </span>
                              </div>
                              <div
                                className="relative rounded-md"
                                role="presentation"
                              >
                                {/* <input
                          className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled"
                          aria-invalid="false"
                          aria-describedby="current_address.state-error"
                          name="current_address.state"
                          type="text"
                          placeholder="State will be based on your pincode"
                          readonly=""
                          data-testid="current_address.state"
                        /> */}
                                <Input
                                  name={"state"}
                                  className="block w-full px-5 py-[15px] shadow-none disabled:bg-disabled"
                                  aria-invalid="false"
                                  aria-describedby="property_developer_name-error"
                                  // name="property_developer_name"
                                  type="text"
                                  placeholder="Enter State"
                                  data-testid="property_developer_name"
                                />
                              </div>
                            </label>
                          </div>
                          <div className="grid col-md-4  gap-x-8 sm:gap-y-5 gap-y-4" style={{marginTop:"16px"}}>
                            <div
                              className={`form__item ${
                                errors.city && "input__error"
                              }`}
                            >
                              <label
                                htmlFor="employment_type"
                                className="col-span-6 lg:col-span-2"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="employment_type_label"
                                    className="app-form-label"
                                  >  City
                                  </span>
                                </div>
                                <label
                                  htmlFor="employment_type"
                                  className="col-span-6 col-start-1 lg:col-span-2"
                                >
                                  <div
                                    className="relative rounded-md app-select"
                                    role="presentation"
                                    data-testid="employment_type"
                                    style={{ width: "350px" }}
                                  >
                                    <div className="el-select-container css-2b097c-container">
                                      <div className="el-select__control css-yk16xz-control">
                                        <div className="el-select__value-container css-1hwfws3 boxwidth">
                                          <div className="el-select__placeholder css-1wa3eu0-placeholder">
                                            <div>
                                              <Field
                                                as="select"
                                                name={"city"}
                                                id="marriedstatus"
                                                style={{
                                                  border: "none",
                                                }}
                                                // onChange={(e) => handleChange(e)}
                                              >
                                                <option value="" selected>
                                                  Please Select any option
                                                </option>
                                                <option value="Married">
                                                 Kalyan
                                                </option>
                                                <option value=" Single">
                                                Mumbai
                                                </option>
                                                <option value=" Seperated">
                                                 Thane
                                                </option>
                                                <option value=" Divorced">
                                                Sangli
                                                </option>
                                              </Field>
                                            </div>
                                          </div>

                                          <div className="css-1g6gooi">
                                            <div
                                              className="el-select__input"
                                              style={{
                                                display: "inline-block",
                                              }}
                                            >
                                              <input
                                                autocapitalize="none"
                                                autocomplete="off"
                                                autocorrect="off"
                                                id="react-select-2-input"
                                                spellcheck="false"
                                                tabindex="0"
                                                type="text"
                                                aria-autocomplete="list"
                                                value=""
                                                style={{
                                                  boxsizing: "contentbox",
                                                  width: "2px",
                                                  background: "0px center",
                                                  border: "0px",
                                                  fontsize: "inherit",
                                                  opacity: "1",
                                                  outline: "0px",
                                                  padding: "0px",
                                                  color: "inherit",
                                                }}
                                              />

                                              {/* <div
                                        style={{
                                          position: "absolute",
                                          top: "0px",
                                          left: "0px",
                                          visibility: "hidden",
                                          height: "0px",
                                          overflow: "scroll",
                                          whiteSpace: "pre",
                                          fontSize: "16px",
                                          fontFamily: "DM Sans",
                                          fontWeight: "500",
                                          fontStyle: "normal",
                                          letterSpacing: "normal",
                                          textTransform: "none",
                                        }}
                                      ></div> */}
                                            </div>
                                          </div>
                                        </div>
                                        <div className="el-select__indicators css-1wy0on6">
                                          <span className="el-select__indicator-separator css-1okebmr-indicatorSeparator"></span>

                                          <svg
                                            height="20"
                                            width="20"
                                            viewBox="0 0 20 20"
                                            aria-hidden="true"
                                            focusable="false"
                                            className="css-8mmkcg"
                                          >
                                            <path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path>
                                          </svg>
                                        </div>
                                      </div>

                                      <input
                                        name="employment_type"
                                        type="hidden"
                                        value=""
                                      />
                                    </div>
                                  </div>
                                </label>
                                <p className={"error__feedback"}>
                                  {errors.city}
                                </p>
                              </label>
                            </div>
                          </div>
                  
                        </div>
                        <div className="relative my-6">
                          <button
                            type="submit"
                            className="space-x-2 app-btn app-btn-primary"
                            data-testid="submit"
                            data-category="unlock_credit_score"
                            data-action="Unlock credit score"
                            data-label="/personal-detail"
                          >
                            <img
                              style={{
                                width: "25px",
                                height: "25px",
                                marginTop: "-10px",
                              }}
                              className=""
                              src={unlock}
                              alt="unlock"
                            />
                            <span>Continue</span>
                          </button>
                        </div>
                      </form>
                    </dd>
                  </div>
                </div>

                <div className="py-[18px] sm:py-[26px] border-t border-b border-gray-light">
                  <button
                    className="flex items-center w-auto pl-0 text-base font-medium shadow-none text-primary bg-none hover:shadow-none disabled:opacity-50"
                    type="submit"
                    data-testid="submit"
                    disabled=""
                  >
                    <span className="">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="w-5 h-5 mr-3"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z"
                          clip-rule="evenodd"
                        ></path>
                      </svg>
                    </span>
                    <span>Add Co-applicant</span>
                  </button>
                </div>
                <form data-testid="form-personal-details">
                  <div className="grid grid-cols-6 mt-4 gap-x-8 gap-y-4 sm:gap-y-5">
                    <label
                      htmlhtmlFor="property_identified"
                      className="col-span-6 lg:col-span-2 relative block"
                    >
                      <div className="app-form-wrapper">
                        <span
                          data-testid="property_identified_label"
                          className="app-form-label"
                        >
                          Property Identified?
                        </span>
                      </div>
                      <div className="flex items-center overflow-hidden bg-white rounded-md shadow-app-card">
                        {/* <div
                          style={{ display: "flex", paddingTop: "15px" }}
                          className={`form__item ${
                            errors.radio && "input__error"
                          }`}
                        > */}
                        <p className={"error__feedback"}>{errors.radio}</p>

                        {/* <input id="toggle-on" name="radio" type="radio" />
                        <label
                          htmlFor="toggle-on"
                          style={{
                            padding: "10px 75px",
                            // backgroundColor: "orange",
                            border: "thin solid darkorange",
                            borderRadius: "10px",
                            margin: "5px",
                            display: "inline-block",
                          }}
                        >
                          YES
                        </label>
                        <input id="toggle-off" name="radio" type="radio" />
                        <label
                          htmlFor="toggle-off"
                          style={{
                            padding: "10px 75px",
                            // backgroundColor: "orange",
                            border: "thin solid darkorange",
                            borderRadius: "10px",
                            margin: "5px",
                            display: "inline-block",
                          }}
                        >
                          NO
                        </label> */}

                        <button
                          id="toggle-on"
                          name="radio"
                          type="button"
                          className="w-1/2 text-sm font-title font-bold rounded-md px-8 py-3 outline-none focus:outline-none uppercase bg-secondary text-white"
                          data-testid="property_identified_yes"
                        >
                          Yes
                        </button>
                        <button
                          id="toggle-off"
                          name="radio"
                          type="button"
                          value="Two"
                          className="w-1/2 text-sm font-title font-bold rounded-md px-8 py-3 outline-none focus:outline-none uppercase bg-white text-secondary"
                          data-testid="property_identified_no"
                        >
                          No
                        </button>

                        {/* </div> */}
                      </div>
                      <input
                        className="sr-only"
                        hidden=""
                        aria-hidden="true"
                        name="property_identified"
                      />
                    </label>
                    {/*                       
                    { if(choose == false) ? 
                    <h1>Yes</h1>:
                    <h1>No</h1>
                  } */}

                    <label
                      htmlhtmlFor="property_type"
                      className="col-span-6 lg:col-span-2 relative block"
                    >
                      <div className="app-form-wrapper">
                        <span
                          data-testid="property_type_label"
                          className="app-form-label"
                        >
                          Property Type
                        </span>
                      </div>
                      <div className="flex items-center overflow-hidden bg-white rounded-md shadow-app-card">
                        <button
                          type="button"
                          className="w-1/2 text-sm font-title font-bold rounded-md px-2 py-3 outline-none focus:outline-none uppercase bg-secondary text-white"
                          data-testid="property_type_1"
                        >
                          residential
                        </button>
                        <button
                          type="button"
                          className="w-1/2 text-sm font-title font-bold rounded-md px-2 py-3 outline-none focus:outline-none uppercase bg-white text-secondary"
                          data-testid="property_type_2"
                        >
                          commercial
                        </button>
                      </div>

                      <input
                        className="sr-only"
                        hidden=""
                        aria-hidden="true"
                        name="property_type"
                      />
                    </label>
                  </div>

                  <div className="grid grid-cols-6 mt-5 gap-x-8 gap-y-5">
                    <label
                      htmlhtmlFor="stage_of_construction"
                      className="col-span-6 lg:col-span-2 relative block"
                    >
                      <div className="app-form-wrapper">
                        <span
                          data-testid="stage_of_construction_label"
                          className="app-form-label"
                        >
                          Stage of Construction
                        </span>
                      </div>
                      <div className="grid items-center grid-cols-2 gap-x-6 lg:gap-x-8">
                        <button
                          type="button"
                          className="relative flex-col h-24 border btn btn-input shadow-input-shadow hover:border-secondary hover:text-secondary"
                          data-testid="stage_of_construction_1"
                        >
                          <img src={construction} alt="loan type" className="h-9" />
                          <div className="px-1 mt-2 font-medium leading-3 capitalize text-xxs text-gray text-opacity-70">
                            Under
                            <br />
                            Construction
                          </div>
                        </button>
                        <button
                          type="button"
                          className="relative flex-col h-24 border btn btn-input shadow-input-shadow hover:border-secondary hover:text-secondary border-green text-green"
                          data-testid="stage_of_construction_2"
                        >
                          <img src={house} alt="loan type" className="h-9" />
                          <div className="px-1 mt-2 font-medium leading-3 capitalize text-xxs text-gray text-opacity-70">
                            Ready
                            <br />
                            to move -in
                          </div>
                          <div className="absolute inline-flex items-center p-1 text-white rounded-full -bottom-3 -right-3 bg-green">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="w-6 h-6"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path
                                fill-rule="evenodd"
                                d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                clip-rule="evenodd"
                              ></path>
                            </svg>
                          </div>
                        </button>
                      </div>
                      <input
                        className="sr-only"
                        hidden=""
                        aria-hidden="true"
                        name="stage_of_construction"
                      />
                    </label>
                  
                    <div
                     
                      className={` col-span-6 lg:col-span-2 relative blockform__item ${errors.propname && "input__error"}`}
                    >
                      <label
                        htmlhtmlFor="property_developer_name"
                        className="col-span-6 lg:col-span-2 relative block"
                      >
                        <div className="app-form-wrapper">
                          <span
                            data-testid="property_developer_name_label"
                            className="app-form-label"
                          >
                            Developer Name
                          </span>
                        </div>
                        <div className="relative rounded-md" role="presentation">
                          <Input
                            className="block w-full px-5 py-[15px] shadow-none disabled:bg-disabled"
                            aria-invalid="false"
                            aria-describedby="property_developer_name-error"
                            name={"propname"}
                            type="text"
                            placeholder="Enter developer’s name"
                            data-testid="property_developer_name"
                          />
                        </div>
                        <p className={"error__feedback"}>{errors.propname}</p>
                      </label>
                    </div>
                

                    <div
                     
                      className={` col-span-6 lg:col-span-2 relative blockform__item ${errors.projname && "input__error"}`}
                    >
                    <label
                      htmlhtmlFor="project_name"
                      className="col-span-6 lg:col-span-2 relative block"
                    >
                      <div className="app-form-wrapper">
                        <span
                          data-testid="project_name_label"
                          className="app-form-label"
                        >
                          Project Name
                        </span>
                      </div>
                      <div className="relative rounded-md" role="presentation">
                        <Input
                          className="block w-full px-5 py-[15px] shadow-none disabled:bg-disabled"
                          aria-invalid="false"
                          aria-describedby="project_name-error"
                          name={"projname"}
                          type="text"
                          placeholder="Enter project name"
                          data-testid="project_name"
                        />
                      </div>
                      <p className={"error__feedback"}>{errors.projname}</p>
                    </label>
                    </div>



                    <div
                     
                      className={` col-span-6 lg:col-span-2 relative blockform__item ${errors.nameofbuild && "input__error"}`}
                    >
                    <label
                      htmlhtmlFor="name_of_building"
                      className="col-span-6 lg:col-span-2 relative block"
                    >
                      <div className="app-form-wrapper">
                        <span
                          data-testid="name_of_building_label"
                          className="app-form-label"
                        >
                          Name of Building
                        </span>
                      </div>
                      <div className="relative rounded-md" role="presentation">
                        <Input
                          className="block w-full px-5 py-[15px] shadow-none disabled:bg-disabled"
                          aria-invalid="false"
                          aria-describedby="name_of_building-error"
                          name={"nameofbuild"}
                          type="text"
                          placeholder="Optional Field"
                          data-testid="name_of_building"
                        />
                      </div>
                      <p className={"error__feedback"}>{errors.nameofbuild}</p>
                    </label>
                    </div>

                    <div
                     
                     className={` col-span-6 lg:col-span-2 relative blockform__item ${errors.locality && "input__error"}`}
                   >
                    <label
                      htmlhtmlFor="address.area"
                      className="col-span-6 lg:col-span-2 relative block"
                    >
                      <div className="app-form-wrapper">
                        <span
                          data-testid="address.area_label"
                          className="app-form-label"
                        >
                          Locality
                        </span>
                      </div>
                      <div className="relative rounded-md" role="presentation">
                        <Input
                          className="block w-full px-5 py-[15px] shadow-none disabled:bg-disabled"
                          aria-invalid="false"
                          aria-describedby="address.area-error"
                          name={"locality"}
                          type="text"
                          placeholder="Enter area/ locality"
                          data-testid="address.area"
                        />
                      </div>
                      <p className={"error__feedback"}>{errors.locality}</p>
                    </label>

                    </div>

                    <div className="grid col-md-4  gap-x-8 sm:gap-y-5 gap-y-4" style={{marginTop: "0px"}}>
                            <div
                              className={`form__item ${
                                errors.city && "input__error"
                              }`}
                            >
                              <label
                                htmlFor="employment_type"
                                className="col-span-6 lg:col-span-2"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="employment_type_label"
                                    className="app-form-label"
                                  >  City
                                  </span>
                                </div>
                                <label
                                  htmlFor="employment_type"
                                  className="col-span-6 col-start-1 lg:col-span-2"
                                >
                                  <div
                                    className="relative rounded-md app-select"
                                    role="presentation"
                                    data-testid="employment_type"
                                    style={{ width: "350px" }}
                                  >
                                    <div className="el-select-container css-2b097c-container">
                                      <div className="el-select__control css-yk16xz-control">
                                        <div className="el-select__value-container css-1hwfws3 boxwidth">
                                          <div className="el-select__placeholder css-1wa3eu0-placeholder">
                                            <div>
                                              <Field
                                                as="select"
                                                name={"city"}
                                                id="marriedstatus"
                                                style={{
                                                  border: "none",
                                                }}
                                                // onChange={(e) => handleChange(e)}
                                              >
                                                <option value="" selected>
                                                  Please Select any option
                                                </option>
                                                <option value="Married">
                                                 Kalyan
                                                </option>
                                                <option value=" Single">
                                                Mumbai
                                                </option>
                                                <option value=" Seperated">
                                                 Thane
                                                </option>
                                                <option value=" Divorced">
                                                Sangli
                                                </option>
                                              </Field>
                                            </div>
                                          </div>

                                          <div className="css-1g6gooi">
                                            <div
                                              className="el-select__input"
                                              style={{
                                                display: "inline-block",
                                              }}
                                            >
                                              <input
                                                autocapitalize="none"
                                                autocomplete="off"
                                                autocorrect="off"
                                                id="react-select-2-input"
                                                spellcheck="false"
                                                tabindex="0"
                                                type="text"
                                                aria-autocomplete="list"
                                                value=""
                                                style={{
                                                  boxsizing: "contentbox",
                                                  width: "2px",
                                                  background: "0px center",
                                                  border: "0px",
                                                  fontsize: "inherit",
                                                  opacity: "1",
                                                  outline: "0px",
                                                  padding: "0px",
                                                  color: "inherit",
                                                }}
                                              />

                                              {/* <div
                                        style={{
                                          position: "absolute",
                                          top: "0px",
                                          left: "0px",
                                          visibility: "hidden",
                                          height: "0px",
                                          overflow: "scroll",
                                          whiteSpace: "pre",
                                          fontSize: "16px",
                                          fontFamily: "DM Sans",
                                          fontWeight: "500",
                                          fontStyle: "normal",
                                          letterSpacing: "normal",
                                          textTransform: "none",
                                        }}
                                      ></div> */}
                                            </div>
                                          </div>
                                        </div>
                                        <div className="el-select__indicators css-1wy0on6">
                                          <span className="el-select__indicator-separator css-1okebmr-indicatorSeparator"></span>

                                          <svg
                                            height="20"
                                            width="20"
                                            viewBox="0 0 20 20"
                                            aria-hidden="true"
                                            focusable="false"
                                            className="css-8mmkcg"
                                          >
                                            <path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path>
                                          </svg>
                                        </div>
                                      </div>

                                      <input
                                        name="employment_type"
                                        type="hidden"
                                        value=""
                                      />
                                    </div>
                                  </div>
                                </label>
                                <p className={"error__feedback"}>
                                  {errors.city}
                                </p>
                              </label>
                            </div>
                          </div>
                  

                    <div className="relative flex items-start mt-2 col-span-full">
                      <div className="flex items-center h-5">
                        <input
                          id="consent"
                          type="checkbox"
                          className=""
                          name="consent"
                          data-testid="consent"
                        />
                      </div>
                      <div className="ml-4 text-sm">
                        <label
                          htmlhtmlFor="consent"
                          className="font-medium text-primary"
                        >
                          I agree to Terms of Use of Credit Score
                        </label>
                      </div>
                    </div>
                  </div>
                  <div
                    className={
                      "form__item button__items d-flex justify-content-between"
                    }
                  >
                    <Button
                      type={"default"}
                      onClick={prev}
                      className="w-auto space-x-2 app-btn app-btn-primary"
                      data-testid="submit"
                      data-category="unlock_credit_score"
                      data-action="Unlock credit score"
                      data-label="/personal-detail"
                    >
                      <span>Back</span>
                    </Button>
                    <Button
                      type={"primary"}
                      onClick={handleSubmit}
                      className="w-auto space-x-2 app-btn app-btn-primary"
                      data-testid="submit"
                      data-category="unlock_credit_score"
                      data-action="Unlock credit score"
                      data-label="/personal-detail"
                    >
                      Next
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        );
      }}
    </Formik>
  );
};
export default Personaldetails;
