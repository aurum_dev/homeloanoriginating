import React, { useState, useContext } from "react";
import { Formik, Field } from "formik";
import { Button } from "antd";
import { Input, InputNumber } from "formik-antd";
import MultiStepFormContext from "../MultiStepFormContext";
import "../../../assets/css/Requirement/Requirement.css";
import { useLocation } from "react-router-dom";
import loan from "../../../assets/images/loan.png";
import { Link } from "react-router-dom";


const Requirement = () => {
  const { requirement, setRequirement, next } =
    useContext(MultiStepFormContext);

  // const val1 = location.state?.val1;
  // const val2 = location.state?.val2;

  // const [selectedOption, setSelectedOption] = useState(null);
  // const handleChange = (e) => {
  //   setSelectedOption(e.target.value);
  //   console.log("çhange");
  // };

  const location = useLocation();
  const val1 = location.state?.val1;
  const val2 = location.state?.val2;
  const numWords = require('num-words')
  const amountInWords3 = numWords(val1)
  const amountInWords4 = numWords(val2)

  const [selectedOption, setSelectedOption] = useState(null);
  const handleChange = (e) => {
    setSelectedOption(e.target.value);
    console.log("çhange");
  };
  return (
    <Formik
      initialValues={requirement}
      onSubmit={(values) => {
        setRequirement(values);
        next();
      }}
      validate={(values) => {
        const errors = {};
        if (!values.slider) errors.slider = "Slider is required";
        if (!values.slidersubreuire)
          errors.slidersubreuire = "Tenure Slider is required";
        if (!values.emptype) errors.emptype = "type of employe required";
        if (!values.workexp) errors.workexp = "Over all experince required";

        if (!values.anuincome) errors.anuincome = "Annual Income is required";
        if (!values.monthincome) errors.monthincome = "Monthly Income  is required";
        if (!values.emi) errors.emi= "Existing Emi is required";
  
        return errors;
      }}
    >
      {({ handleSubmit, errors }) => {
        return (
          <div className={"details__wrapper"}>
            {/* <h1>{val1}</h1>
      <h1>{val2}</h1>  */}
            <div id="__next">
              <div className="container relative px-4 sm:px-0 mx-auto mt-24 sm:mt-28 sm:w-7/12">
                <h1 className="bg-white app-page-title mt-10">
                  Let’s understand your requirements
                </h1>
                <p className="app-page-sub-title mb-5">
                  Easiloan uses these details to check your loan eligibility and
                  unlock personalised offers for you
                </p>
                <form data-testid="loan-requirement-form">
                  <div className="grid grid-cols-6 mt-2 gap-x-8 gap-y-5">
                    <label htmlFor="loan_type" className="col-span-6 lg:col-span-3">
                      <div className="app-form-wrapper">
                        <span
                          data-testid="loan_type_label"
                          className="app-form-label"
                        >
                          Loan type
                        </span>
                      </div>
                      <div className="grid items-center grid-cols-3 gap-x-3 lg:gap-x-8">
                        <button
                          type="button"
                          className="relative flex-col w-[100px] h-[100px] border border-transparent btn btn-input rounded-10 shadow-app-card hover:border-green border-green text-secondary"
                          data-testid="loanType-1"
                        >
                          <div className="flex items-center h-9">
                            <img
                              src={loan}
                              alt="New Home Loan"
                              className="h-8 lg:h-9"
                            />
                          </div>
                          <div className="px-3 mt-2 font-medium leading-3 text-xxs text-gray text-opacity-70">
                            New Home Loan
                          </div>
                          <div className="absolute inline-flex items-center w-5 h-5 overflow-hidden rounded-full opacity-100 -bottom-2 md:-bottom-3 -right-2 md:-right-3 text-green md:w-8 md:h-8">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="bg-white"
                              width="32"
                              height="32"
                              viewBox="0 0 18 18"
                            >
                              <g fill="currentColor" fill-rule="evenodd">
                                <g fill="currentColor" fill-rule="nonzero">
                                  <path
                                    d="M24.996 16c4.968.011 8.993 4.034 9.004 9 0 2.695-1.209 5.248-3.292 6.957-2.084 1.71-4.824 2.396-7.468 1.87-4.538-.906-7.65-5.104-7.196-9.708.453-4.604 4.324-8.115 8.952-8.119zm4.722 6.386c-.304-.304-.796-.304-1.1 0L24.503 26.5l-2.042-2.042c-.304-.303-.796-.303-1.1 0-.303.304-.303.796 0 1.1l2.592 2.59c.304.304.796.304 1.1 0l4.665-4.663c.304-.304.304-.796 0-1.1z"
                                    transform="translate(-16 -16)"
                                  ></path>
                                </g>
                              </g>
                            </svg>
                          </div>
                        </button>
                      </div>
                      <input
                        className="sr-only"
                        hidden=""
                        aria-hidden="true"
                        name="loan_type"
                        data-testid="loan_type"
                      />
                    </label>
                  </div>

                  <div className="grid grid-cols-4 mt-5 gap-x-8 gap-y-5 ">
                    <div
                      className={` form__item ${
                        errors.slider && "input__error"
                      }`}
                    >
                      <label htmlhtmlFor="slider" className="col-span-6 lg:col-span-2">
                        <div className="app-form-wrapper">
                          <span
                            data-testid="loan_amount_needed_label"
                            className="app-form-label"
                          >
                            Required Loan Amount
                          </span>
                        </div>
                        <div className="relative rounded-md" role="presentation">
                          <div className="relative">
                            <div>
                              <input
                                style={{ borderRadius: "6px" }}
                                type="text"
                                className="block w-full opacity-90 border"
                                placeholder="Enter your loan amount requirement"
                                data-testid="loan_amount_needed_input"
                                defaultValue={val1}
                              />
                            </div>
                            <div className="absolute bottom-[1px] w-full">
                              <div className="range-wrapper">
                                <div className="range-slider">
                                  <div className="range-slider__track">
                                    <Input
                                      type="range"
                                      name={"slider"}
                                      min="0"
                                      max="60000000"
                                      step="100000"
                                      className="range-slider__track"
                                      data-testid="loan_amount_needed_slider"
                                       defaultValue={val1}
                                      style={{
                                        zIndex: "5",
                                        marginTop: "-30px",
                                      }}
                                    />
                                  </div>
                                  <p></p>
                                  <div
                                    className="range-slider__range"
                                    style={{ left: " 0px", width: "0%" }}
                                  ></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <p  style={{textTransform:"capitalize",fontSize: "14px",
    marginTop: "10px"}}>{amountInWords3}</p>
                        </div>
                        {/* <p className={"error__feedback"}>{errors.slider}</p> */}
                      </label>
                    </div>

                    <div
                      className={` form__item ${
                        errors.slidersubreuire && "input__error"
                      }`}
                    >
                      <label htmlhtmlFor="slider" className="col-span-6 lg:col-span-2">
                        <div className="app-form-wrapper">
                          <span
                            data-testid="preferred_loan_tenure_label"
                            className="app-form-label"
                          >
                            Preferred Loan Tenure (In YEARS)
                          </span>
                        </div>
                        <div className="relative rounded-md" role="presentation">
                          <div className="relative">
                            <div>
                              <input
                                style={{ borderRadius: "6px" }}
                                type="text"
                                className="block w-full opacity-90 border"
                                placeholder="Enter your loan tenure"
                                data-testid="preferred_loan_tenure_input"
                                defaultValue={val2}
                              />
                            </div>
                            <div className="absolute bottom-[1px] w-full">
                              <div className="range-wrapper">
                                <div className="range-slider">
                                  <div className="range-slider__track">
                                    <Input
                                      type="range"
                                      name={"slidersubreuire"}
                                      min="1"
                                      max="30"
                                      step="1"
                                      className="range-slider__track"
                                      data-testid="preferred_loan_tenure_slider"
                                   defaultValue={val2}
                                      style={{
                                        zIndex: "5",
                                        marginTop: "-30px",
                                      }}
                                    />
                                  </div>
                                  <div
                                    className="range-slider__range"
                                    style={{ left: "0px", width: "20%" }}
                                  ></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <p  style={{textTransform:"capitalize",fontSize: "14px",
    marginTop: "10px"}}>{amountInWords4}</p>
                        </div>
                        {/* <p className={"error__feedback"}>
                          {errors.slidersubreuire}
                        </p> */}
                      </label>
                    </div>

                    <div
                      className={`form__item ${
                        errors.emptype && "input__error"
                      }`}
                    >
                      <label
                        htmlFor="employment_type"
                        className="col-span-6 lg:col-span-2"
                      >
                        <div className="app-form-wrapper">
                          <span
                            data-testid="employment_type_label"
                            className="app-form-label"
                          >
                            Type of employment
                          </span>
                        </div>
                        <label
                          htmlFor="employment_type"
                          className="col-span-6 col-start-1 lg:col-span-2"
                        >
                          <div
                            className="relative rounded-md app-select"
                            role="presentation"
                            data-testid="employment_type"
                          >
                            <div className="el-select-container css-2b097c-container">
                              <div className="el-select__control css-yk16xz-control">
                                <div className="el-select__value-container css-1hwfws3 boxwidth">
                                  <div className="el-select__placeholder css-1wa3eu0-placeholder">
                                    <div>
                                      <Field
                                        as="select"
                                        name="emptype"
                                        id="employment_type"
                                        style={{
                                          border: "none",
                                        }}
                                      // onChange={(e) => handleChange(e)}
                                      >
                                        <option value="" selected>
                                          Please select one option
                                        </option>
                                        <option value=" Salaried">
                                          Salaried
                                        </option>
                                        <option value="Self Empoloyed professional">
                                          Self Empoloyed professional
                                        </option>
                                        <option value="Please select one option">
                                          Self Empoloyed Non-professional
                                        </option>
                                        <option value="Retired">Retired</option>
                                        <option value="Other">Other</option>
                                      </Field>
                                    </div>
                                  </div>

                                  <div className="css-1g6gooi">
                                    <div
                                      className="el-select__input"
                                      style={{ display: "inline-block" }}
                                    >
                                      <input
                                        autocapitalize="none"
                                        autocomplete="off"
                                        autocorrect="off"
                                        id="react-select-2-input"
                                        spellcheck="false"
                                        tabindex="0"
                                        type="text"
                                        aria-autocomplete="list"
                                        value=""
                                        style={{
                                          boxsizing: "contentbox",
                                          width: "2px",
                                          background: "0px center",
                                          border: "0px",
                                          fontsize: "inherit",
                                          opacity: "1",
                                          outline: "0px",
                                          padding: "0px",
                                          color: "inherit",
                                        }}
                                      />

                                      {/* <div
                                        style={{
                                          position: "absolute",
                                          top: "0px",
                                          left: "0px",
                                          visibility: "hidden",
                                          height: "0px",
                                          overflow: "scroll",
                                          whiteSpace: "pre",
                                          fontSize: "16px",
                                          fontFamily: "DM Sans",
                                          fontWeight: "500",
                                          fontStyle: "normal",
                                          letterSpacing: "normal",
                                          textTransform: "none",
                                        }}
                                      ></div> */}
                                    </div>
                                  </div>
                                </div>
                                <div className="el-select__indicators css-1wy0on6">
                                  <span className="el-select__indicator-separator css-1okebmr-indicatorSeparator"></span>

                                  <svg
                                    height="20"
                                    width="20"
                                    viewBox="0 0 20 20"
                                    aria-hidden="true"
                                    focusable="false"
                                    className="css-8mmkcg"
                                  >
                                    <path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path>
                                  </svg>
                                </div>
                              </div>

                              <input
                                name="employment_type"
                                type="hidden"
                                value=""
                              />
                            </div>
                          </div>
                        </label>
                        <p className={"error__feedback"}>{errors.emptype}</p>
                      </label>
                    </div>

                    <div
                      className={` form__item ${
                        errors.workexp && "input__error"
                      }`}
                    >
                      <label
                        htmlFor="overall_work_experience"
                        className="col-span-6 lg:col-span-2"
                      >
                        <div className="app-form-wrapper">
                          <span
                            data-testid="overall_work_experience_label"
                            className="app-form-label"
                          >
                            Overall Work Experience (In YEARS)
                          </span>
                        </div>
                        <div className="relative rounded-md" role="presentation">
                          <InputNumber 
                            name={"workexp"}
                            style={{ borderRadius: "6px" }}
                            className="block w-full px-5 py-[15px] shadow-none disabled:bg-disabled"
                            aria-invalid="false"
                            aria-describedby="overall_work_experience-error"
                            type="number"
                            placeholder="Enter total experience in years"
                            data-testid="overall_work_experience"
                            //  value={selectedOption}
                          />
                        </div>
                        <p className={"error__feedback"}>{errors.workexp}</p>
                      </label>
                    </div>

                    <div
                      className={` form__item ${
                        errors.anuincome && "input__error"
                      }`}
                    >
                      <label
                        htmlFor="other_annual_income"
                        className="col-span-6 col-start-1 lg:col-span-2"
                      >
                        <div className="app-form-wrapper">
                          <span
                            data-testid="other_annual_income_label"
                            className="app-form-label"
                          >
                            Other Annual Income
                          </span>
                        </div>
                        <div className="relative rounded-md" role="presentation">
                          <InputNumber 
                           name={"anuincome"}
                            style={{ borderRadius: "6px" }}
                            className="block w-full px-5 py-[15px] shadow-none disabled:bg-disabled"
                            aria-invalid="false"
                            aria-describedby="other_annual_income-error"
                            type="number"
                            placeholder="Enter your other annual income"
                            data-testid="other_annual_income"
                           
                          />
                        </div>
                        <p className={"error__feedback"}>
                          {errors.anuincome}
                        </p>
                      </label>
                    </div>

                    <div
                      className={` form__item ${
                        errors.monthincome && "input__error"
                      }`}
                    >
                      <label
                        htmlFor="Monthly Income"
                        className="col-span-6 lg:col-span-2"
                      >
                        <div className="app-form-wrapper">
                          <span
                            data-testid="existing_monthly_emi_label"
                            className="app-form-label"
                          >
                            Monthly Income
                          </span>
                        </div>
                        <div className="relative rounded-md" role="presentation">
                          <InputNumber 
                            name={"monthincome"}
                            style={{ borderRadius: "6px" }}
                            className="block w-full px-5 py-[15px] shadow-none disabled:bg-disabled"
                            aria-invalid="false"
                            aria-describedby="existing_monthly_emi-error"
                            type="number"
                            placeholder="Enter your existing monthly EMIs"
                            data-testid="existing_monthly_emi"
                           
                          />
                        </div>
                        <p className={"error__feedback"}>{errors.monthincome}</p>
                      </label>
                    </div>

                    <div
                      className={` form__item ${
                        errors.anuincome && "input__error"
                      }`}
                    >
                      <label
                        htmlFor="Other Annual Income"
                        className="col-span-6 lg:col-span-2"
                      >
                        <div className="app-form-wrapper">
                          <span
                            data-testid="existing_monthly_emi_label"
                            className="app-form-label"
                          >
                            Other Annual Income
                          </span>
                        </div>
                        <div className="relative rounded-md" role="presentation">
                          <InputNumber 
                          name={"anuincome"}
                            style={{ borderRadius: "6px" }}
                            className="block w-full px-5 py-[15px] shadow-none disabled:bg-disabled"
                            aria-invalid="false"
                            aria-describedby="existing_monthly_emi-error"
                            type="number"
                            placeholder="Enter your existing monthly EMIs"
                            data-testid="existing_monthly_emi"
                           
                          />
                        </div>
                        <p className={"error__feedback"}>
                          {errors.anuincome}
                        </p>
                      </label>
                    </div>
                    <div
                      className={` form__item ${
                        errors.emi && "input__error"
                      }`}
                    >
                      <label
                        htmlFor="existing_monthly_emi"
                        className="col-span-6 lg:col-span-2"
                      >
                        <div className="app-form-wrapper">
                          <span
                            data-testid="existing_monthly_emi_label"
                            className="app-form-label"
                          >
                            Existing EMIs (Specify Amount)
                          </span>
                        </div>
                        <div className="relative rounded-md" role="presentation">
                          <InputNumber 
                          name={"emi"}
                            style={{ borderRadius: "6px" }}
                            className="block w-full px-5 py-[15px] shadow-none disabled:bg-disabled"
                            aria-invalid="false"
                            aria-describedby="existing_monthly_emi-error"
                            type="number"
                            placeholder="Enter your existing monthly EMIs"
                            data-testid="existing_monthly_emi"
                           
                          />
                        </div>
                        <p className={"error__feedback"}>
                          {errors.emi}
                        </p>
                      </label>
                     
                    </div>
                  </div>

                  <div
                    className={
                      "form__item button__items d-flex justify-content-end"
                    }
                  >
                    <button
                      className="app-btn app-btn-primary"
                      type={"primary"}
                      onClick={handleSubmit}
                      data-testid="submit"
                      data-category="save_loan_req"
                      data-action="Submit Requirement"
                      data-label="-"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        x-description="Heroicon name: solid/chevron-right"
                        width="16"
                        fill="currentColor"
                        height="16"
                        viewBox="0 0 16 16"
                        aria-hidden="false"
                        className="bg-white rounded-full text-primary"
                        data-testid="default-submit-icon"
                      >
                        <g fill="none" fill-rule="evenodd">
                          <g>
                            <g transform="translate(-17 -17) translate(17 17)">
                              <circle cx="8" cy="8" r="8"></circle>
                              <path
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-width="2"
                                d="M7 5L10 8 7 11"
                              ></path>
                            </g>
                          </g>
                        </g>
                      </svg>
                      {/* <span>Save &amp; Proceed</span> */}
                      <span>Next</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        );
      }}
    </Formik>
  );
};
export default Requirement;
