import React from "react";
import works from "../../../assets/images/works.svg";
import works2 from "../../../assets/images/works2.svg";
import works3 from "../../../assets/images/works3.svg";
import works4 from "../../../assets/images/works4.svg";
import step1 from "../../../assets/images/step1.png";
import step2 from "../../../assets/images/step2.png";
import step3 from "../../../assets/images/step3.png";
import step4 from "../../../assets/images/step4.png";

function Workflow() {
  return (
    <div>
    <div id="howworks"></div>
    <section className="home2-about-section  howitworks" style={{ paddingbottom: "40px" }}>
    <div className="container">
      <div className="row">
        <div
          className=" container col-lg-12 col-md-6 col-sm-12 col-12 howdiv"
          style={{ display: "show",
          marginTop: "210px" }}
        >
          <span className="app-text-link">How It Works</span>
          <div>
            <ul className="" data-testid="customers-list">
              <li className="">
                <div className="flowmobileview" style={{ display: "flex" }}>
                  <div
                    style={{
                      // marginTop: "64px",
                      top: "2144px",
                      left: "150px",
                    }}
                    className="mobileview"
                  >
                    <img
                      className="md:h-180"
                      src={works}
                      alt=""
                      style={{
                        width: "400px",

                        height: "400px",

                        marginBottom: "144px",
                        marginRight: "-227",
                      }}
                    />
                  </div>
                  <div
                    style={{
                      marginTop: "64px",
                      top: "2144px",
                      left: "150px",
                    }}
                    className="mobileview2"
                  >
                    <img
                      className="md:h-180"
                      src={step1}
                      alt=""
                      style={{
                        width: "340px",

                        height: "340px",

                        marginBottom: "15x",
                        marginRight: "-227",
                      }}
                    />
                  </div>
                  <div style={{ marginTop: "116px", marginLeft: "390px" }} className="step1">
                    <button
                      style={{
                        fontSize: "16px",
                        width: "100px",
                        height: "50px",
                        // marginTop: "45px",
                        background:
                          "linear-gradient(to right, #7FCD91 0%, #2EB086 74.71%)",
                        borderRadius: "10px",
                        padding: "5px 10px 5px 10px",
                      }}
                      type="button"
                      className=" app-btn app-btn-primary"
                      data-testid="unlock-loan-offers"
                      data-category="unlock_loan"
                      data-action="Unlock Loan offers"
                      data-label="-"
                    >
                      {/* <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="12"
                    height="14"
                    viewBox="0 0 16 18"
                  >
                    <path
                      d="M14.667 6h-9V3.667a2.333 2.333 0 114.666 0 .664.664 0 00.667.666.665.665 0 00.667-.666A3.67 3.67 0 008 0a3.667 3.667 0 00-3.667 3.667V6h-3A1.333 1.333 0 000 7.333v9.334A1.333 1.333 0 001.333 18h13.334A1.333 1.333 0 0016 16.667V7.333A1.333 1.333 0 0014.667 6zM8 13a1 1 0 110-1.999A1 1 0 018 13z"
                      fill="#FFF"
                      fillRule="nonzero"
                    ></path>
                  </svg> */}
                      <span className="getstart2">Step 1</span>
                    </button>
                    <h3
                      className=" app-text-dm-large  whitespace-nowrap lg:"
                      style={{
                        fontfamily: "Lato",
                        Fontstyle: "Regular",
                        Fontsize: "30px",
                        marginTop: "12px",
                      }}
                    >
                      Sign Up
                    </h3>

                    <dl className="flex flex-col justify-between flex-grow mt-[5px]">
                      <dt className="sr-only">Description</dt>
                      <dd
                        className="  app-text-dm-small2  opacity-80"
                        style={{
                          fontfamily: "Roboto",
                          fontstyle: "Regular",
                          fontsize: "16px",
                          lineheight: "26px",
                          // lineheight: "139%",
                        }}
                      >
                        Sign Up/ Register with us in seconds and share your
                        loan requirements.
                      </dd>
                    </dl>
                  </div>
                </div>
              </li>
              <li className="">
                <div className="flowmobileview" style={{ display: "flex" }}>

                <div
                    style={{
                      marginTop: "64px",
                      top: "2144px",
                      left: "150px",
                    }}
                    className="mobileview2"
                  >
                    <img
                      className="md:h-180"
                      src={step2}
                      alt=""
                      style={{
                        width: "340px",

                        height: "340px",

                        marginBottom: "15x",
                        marginRight: "-227",
                      }}
                    />
                  </div>
                  <div style={{ marginTop: "-108px" }} className="step2">
                    <button
                      style={{
                        fontSize: "16px",
                        width: "100px",
                        height: "50px",
                        marginTop: "45px",
                        background:
                          "linear-gradient(to right, #7FCD91 0%, #2EB086 74.71%)",
                        borderRadius: "10px",
                        padding: "5px 10px 5px 10px",
                      }}
                      type="button"
                      className=" app-btn app-btn-primary"
                      data-testid="unlock-loan-offers"
                      data-category="unlock_loan"
                      data-action="Unlock Loan offers"
                      data-label="-"
                    >
                      {/* <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="12"
                    height="14"
                    viewBox="0 0 16 18"
                  >
                    <path
                      d="M14.667 6h-9V3.667a2.333 2.333 0 114.666 0 .664.664 0 00.667.666.665.665 0 00.667-.666A3.67 3.67 0 008 0a3.667 3.667 0 00-3.667 3.667V6h-3A1.333 1.333 0 000 7.333v9.334A1.333 1.333 0 001.333 18h13.334A1.333 1.333 0 0016 16.667V7.333A1.333 1.333 0 0014.667 6zM8 13a1 1 0 110-1.999A1 1 0 018 13z"
                      fill="#FFF"
                      fillRule="nonzero"
                    ></path>
                  </svg> */}
                      <span className="getstart2">Step 2</span>
                    </button>
                    <h3
                      className=" app-text-dm-large  whitespace-nowrap lg:"
                      style={{
                        fontfamily: "Lato",
                        Fontstyle: "Regular",
                        Fontsize: "30px",
                        marginTop: "12px",
                      }}
                    >
                      Compare Offers
                    </h3>

                    <dl className="flex flex-col justify-between flex-grow mt-[5px]">
                      <dt className="sr-only">Description</dt>
                      <dd
                        className="  app-text-dm-small2  opacity-80"
                        style={{
                          fontfamily: "Roboto",
                          fontstyle: "Regular",
                          fontsize: "16px",
                          lineheight: "26px",
                          // lineheight: "139%",
                        }}
                      >
                        Get customized options by our proprietary
                        <br />
                        AI-based Instant tool.
                      </dd>
                    </dl>
                  </div>
                  <div
                    className="mobileview"
                    style={{
                      // marginTop: "64px",
                      // top: "2144px",
                      marginTop: "-173px",
                      left: "150px",
                      marginLeft: "380px",
                    }}
                  >
                    <img
                      className="md:h-180"
                      src={works2}
                      alt=""
                      style={{
                        width: "400px",

                        height: "400px",
                      }}
                    />
                  </div>
                </div>
              </li>
              <li className="">
                <div className="flowmobileview" style={{ display: "flex" }}>

                <div
                    style={{
                      marginTop: "64px",
                      top: "2144px",
                      left: "150px",
                    }}
                    className="mobileview2 select view2"
                  >
                    <img
                      className="md:h-180"
                      src={step3}
                      alt=""
                      style={{
                        width: "340px",

                        height: "340px",

                        marginBottom: "15x",
                        marginRight: "-227",
                      }}
                    />
                  </div>
                  <div
                    style={{
                      marginTop: "2px",
                      // marginTop: "-25px",
                      left: "150px",
                    }}
                    className="mobileview"
                  >
                    <img
                      className="md:h-180"
                      src={works3}
                      alt=""
                      style={{
                        width: "400px",

                        height: "400px",

                        marginRight: "-25px",
                      }}
                    />
                  </div>
                  <div style={{ marginTop: "85px", marginLeft: "463px" }} className="step3">
                    <button
                      style={{
                        fontSize: "16px",
                        width: "100px",
                        height: "50px",
                        marginTop: "45px",
                        background:
                          "linear-gradient(to right, #7FCD91 0%, #2EB086 74.71%)",
                        borderRadius: "10px",
                        padding: "5px 10px 5px 10px",
                      }}
                      type="button"
                      className=" app-btn app-btn-primary"
                      data-testid="unlock-loan-offers"
                      data-category="unlock_loan"
                      data-action="Unlock Loan offers"
                      data-label="-"
                    >
                      {/* <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="12"
                    height="14"
                    viewBox="0 0 16 18"
                  >
                    <path
                      d="M14.667 6h-9V3.667a2.333 2.333 0 114.666 0 .664.664 0 00.667.666.665.665 0 00.667-.666A3.67 3.67 0 008 0a3.667 3.667 0 00-3.667 3.667V6h-3A1.333 1.333 0 000 7.333v9.334A1.333 1.333 0 001.333 18h13.334A1.333 1.333 0 0016 16.667V7.333A1.333 1.333 0 0014.667 6zM8 13a1 1 0 110-1.999A1 1 0 018 13z"
                      fill="#FFF"
                      fillRule="nonzero"
                    ></path>
                  </svg> */}
                      <span className="getstart2">Step 3</span>
                    </button>
                    <h3
                      className=" app-text-dm-large  whitespace-nowrap lg:"
                      style={{
                        fontfamily: "Lato",
                        Fontstyle: "Regular",
                        Fontsize: "30px",
                        marginTop: "12px",
                      }}
                    >
                      Select Offer
                    </h3>

                    <dl className="flex flex-col justify-between flex-grow mt-[5px]">
                      <dt className="sr-only">Description</dt>
                      <dd
                        className="  app-text-dm-small2  opacity-80"
                        style={{
                          fontfamily: "Roboto",
                          fontstyle: "Regular",
                          fontsize: "16px",
                          lineheight: "26px",
                          // lineheight: "139%",
                        }}
                      >
                        Choose one offer that suits your home loan
                        requirements the best.
                      </dd>
                    </dl>
                  </div>
                </div>
              </li>
              <li className="">
                <div className="flowmobileview" style={{ display: "flex" }}>

                <div
                    style={{
                      marginTop: "64px",
                      top: "2144px",
                      left: "150px",
                    }}
                    className="mobileview2"
                  >
                    <img
                      className="md:h-180 "
                      src={step4}
                      alt=""
                      style={{
                        width: "340px",

                        height: "340px",

                        marginBottom: "15x",
                        marginRight: "-227",
                      }}
                    />
                  </div>
                  <div className="step4">
                    <button
                      style={{
                        fontSize: "16px",
                        width: "100px",
                        height: "50px",
                        marginTop: "45px",
                        background:
                          "linear-gradient(to right, #7FCD91 0%, #2EB086 74.71%)",
                        borderRadius: "10px",
                        padding: "5px 10px 5px 10px",
                      }}
                      type="button"
                      className=" app-btn app-btn-primary mobilestep4"
                      data-testid="unlock-loan-offers"
                      data-category="unlock_loan"
                      data-action="Unlock Loan offers"
                      data-label="-"
                    >
                      {/* <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="12"
                    height="14"
                    viewBox="0 0 16 18"
                  >
                    <path
                      d="M14.667 6h-9V3.667a2.333 2.333 0 114.666 0 .664.664 0 00.667.666.665.665 0 00.667-.666A3.67 3.67 0 008 0a3.667 3.667 0 00-3.667 3.667V6h-3A1.333 1.333 0 000 7.333v9.334A1.333 1.333 0 001.333 18h13.334A1.333 1.333 0 0016 16.667V7.333A1.333 1.333 0 0014.667 6zM8 13a1 1 0 110-1.999A1 1 0 018 13z"
                      fill="#FFF"
                      fillRule="nonzero"
                    ></path>
                  </svg> */}
                      <span className="getstart2">Step 4</span>
                    </button>
                    <h3
                      className=" app-text-dm-large  whitespace-nowrap lg:"
                      style={{
                        fontfamily: "Lato",
                        Fontstyle: "Regular",
                        Fontsize: "30px",
                        marginTop: "12px",
                      }}
                    >
                      Just Relax
                    </h3>

                    <dl className="flex flex-col justify-between flex-grow mt-[5px]">
                      <dt className="sr-only">Description</dt>
                      <dd
                        className="  app-text-dm-small2  opacity-80"
                        style={{
                          fontfamily: "Roboto",
                          fontstyle: "Regular",
                          fontsize: "18px",
                          lineheight: "26px",
                          // lineheight: "139%",
                        }}
                      >
                        Just relax as we work on getting your home loan<br />
                        processed at the earliest
                      </dd>
                    </dl>
                  </div>
                  <div
                    className="mobileview"
                    style={{
                      // marginTop: "64px",
                      top: "2144px",
                      left: "150px",
                      marginLeft: "260px",
                    }}
                  >
                    <img
                      className="md:h-180"
                      src={works4}
                      alt=""
                      style={{
                        width: "400px",
                        height: "400px",

                        marginLeft: "40px",
                      }}
                    />
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  </div>
  );
}
export default Workflow;
