import React from "react";
// import "../../../assets/css/mainboard/mainboard.css";
import banner from "../../../assets/images/website-banner-illustration.svg";
import approval from "../../../assets/images/approval.jpg";
import bannermobile from "../../../assets/images/bannermobile.png";

function Mainboard() {
  return (
    <div id="getstarted" >
    {" "}
    <div ></div>
    <section
       
      className="home2-about-section"
      style={{ paddingbottom: "40px" }}
    >
      <div className="container">
        <div className="row">
          <div className="col-lg-6 " id="mobileviewcontainer">
            <div className="right-container deskmobview">
              {/* <div className="">
                              <img src={home2} alt="" />
                          </div> */}
              <div className="bannerimage">
                {/* <img src={banner} alt="" className="lg:h-[480px] ml-auto" /> */}
                <img
                  src={banner}
                  alt=""
                  className="img-responsive  rightimgmobile2"
                  style={{
                    width: "540px",
                    height: "538px",
                    margintop: "-30px",
                  }}
                />
              </div>
            </div>

            <div className="right-container bannermobile">
              {/* <div className="">
                              <img src={home2} alt="" />
                          </div> */}
              <div className="bannerimage">
                {/* <img src={banner} alt="" className="lg:h-[480px] ml-auto" /> */}
                <img
                  src={bannermobile}
                  alt=""
                  className="img-responsive  rightimgmobile"
                  style={{
                    width: "540px",
                    height: "538px",
                    margintop: "-30px",
                  }}
                />
              </div>
            </div>
          </div>
          <div className="col-lg-6 homelinespacing"  style={{ paddingTop: "200px" }}  >
            <div className="left-container " style={{marginLeft:"20px"}}>
              <div className="left-top">
                <div className="row">
                  <h1 className=" loanfasterline">
                    Home Loans Are Now
                    <br />A Whole Lot Faster
                  </h1>
                </div>
              </div>
              <div className="bottom-content margin-top-30">
                <p className="mt-3 app-text-dm-small  opacity-80">
                  Kuber is an online entirely digital platform for today's
                  younger generation that doesn't have the time to wait! It
                  makes it easier to apply, track and get bank approval for
                  home loans in less than 24 hours.
                </p>
              </div>
              {/* <div className="readmore-btn">
                <button
                  style={{
                    width: "129px",
                    height: "48px",
                    marginTop: "45px",
                    background:
                      "linear-gradient(to right, #7FCD91 0%, #2EB086 74.71%)",
                  }}
                  type="button"
                  className=" app-btn app-btn-primary"
                  data-testid="unlock-loan-offers"
                  data-category="unlock_loan"
                  data-action="Unlock Loan offers"
                  data-label="-"
                >
                  
                  <span className="getstart">Get Started</span>
                </button>
              </div> */}
            </div>
          </div>
          <div className="col-lg-6 bannermobile" style={{marginTop:"120px"}}>
            <div className="right-container">
              {/* <div className="">
                              <img src={home2} alt="" />
                          </div> */}
              <div className="bannerimage ">
                {/* <img src={banner} alt="" className="lg:h-[480px] ml-auto" /> */}
                <img
                  src={banner}
                  alt=""
                  className="img-responsive rightimgdesk"
                  style={{
                    width: "540px",
                    height: "538px",
                    margintop: "-30px",
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  );
}
export default Mainboard;
