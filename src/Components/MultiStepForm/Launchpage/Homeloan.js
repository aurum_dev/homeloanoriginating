

import React from "react";
import "../../../assets/css/mainboard/mainboard.css";
import "../../../assets/css/ExploreLoan/Exploreloan.css";
// import banner from "../../../assets/images/website-banner-illustration.svg";
import approval from "../../../assets/images/approval.jpg";
import approvalmob from "../../../assets/images/approvalmob.jpg";

function Homeloan() {
  
    // console.log("hi")
    return (

        <div className="relative">
    
        
        <div>
          <img
          className="approvaldesk"
            src={approval}
            alt=""
            style={{
              height: "534px",
              width: "100%",
            }}
          />
        </div>

        <div>
          <img
            className="approvalmob"
            src={approvalmob}
            alt=""
            style={{
              height: "534px",
              width: "100%",
            }}
          />
        </div>
    {/* <div
          className=" sticky  stickybar top-0 z-50  mx-auto lg:-mt-12 md:block " id="approvcardmobile" */}
           <div
          className="  mx-auto lg:-mt-12 md:block " id="approvcardmobile"
          style={{
            width: "769px",
            // height: "169px",
          display:"grid"}}
          data-testid="menu-section"
        >
          <div className="flex justify-start gap-8 px-4 py-6 overflow-auto bg-white lg:justify-around lg:py-10 shadow-card-shadow-secondary rounded-10 lg:gap-0 loancard">
            <div className="subcard">
              <h4 className="loanapprovalnum">
              50 %
              </h4>
              <p className="approvaltext">Faster Loan<br></br>
  Approval</p>
            </div>
  
            <div className="subcard">
              <h4 className="loanapprovalnum">
              2000 +
              </h4>
              <p className="approvaltext">Monthly Loan<br></br>
  Approvals</p>
            </div>
  
            <div className="subcard">
              <h4 className="loanapprovalnum">
              100 +
              </h4>
              <p className="approvaltext">Verified banks<br></br> 
  hosting loans </p>
            </div >
  
            <div className="subcard">
              <h4 className="loanapprovalnum">
              90 %
              </h4>
              <p className="approvaltext">Accurate <br></br>
  loan predictions</p>
            </div>
          </div>
        </div> 
  
        
      </div>
    );

}

export default Homeloan;