import React, { useState, useEffect } from "react";
// import "../../../assets/css/Loanform/form1.css";

import "../../../assets/css/ExploreLoan/Exploreloan.css";
import loan from "../../../assets/images/loan.png";
import { useDispatch } from "react-redux";
import { setval1, setval2 } from "../../../action/amount";
 import Arrow1 from "../../../assets/images/Arrow1.png";
import banner from "../../../assets/images/website-banner-illustration.svg";
import Familyframe from "../../../assets/images/Familyframe.jpg";

import { Link } from "react-router-dom";

function Form1() {
  const [rangeval1, setRangeval1] = useState(null);
  const [rangeval2, setRangeval2] = useState(null);
  const dispatch = useDispatch();
  const numWords = require("num-words");
  const amountInWords1 =
    numWords(rangeval1) == "zero" ? "" : numWords(rangeval1);
  //const amountInWords1 = numWords(rangeval1);
  const amountInWords2 =
    numWords(rangeval2) == "zero" ? "" : numWords(rangeval2);

  useEffect(() => {
    dispatch(setval1(rangeval1));
    dispatch(setval2(rangeval2));
  }, [rangeval1, rangeval2]);

  return (
    <div >
<div id="offering"></div>



<div className="relative" >
<div className="relative z-10 bg-floral" data-testid="top-section"  style={{height:"628px"}}>
  <div className="container relative px-4 pt-4 pb-0 mx-auto">
    <div className="absolute top-0 left-0 right-0 z-0 hidden w-screen h-full bg-light-100 ml-50per-50vw mr-50per-50vw"></div>
    <div className="grid items-center gap-0 pt-4 pb-4 lg:grid-cols-12 lg:pt-2 lg:pb-5  offerningdiv">
      <div className="col-span-5 px-0 lg:pl-[70px] pt-0 pb-3 lg:pb-0">
        {/* <h1 className="app-heading-big  leading-tight lg:leading-[54px] whitespace-nowrap"> */}
        <h1 className="app-heading-big  leading-tight lg:leading-[54px] whitespace-nowrap offeringlinemobile" style={{marginTop:"120px",marginBottom:"30px"}}>
        Our Offerings  
        </h1>
        <div
      className="familymobile"
        style={{
          // width: "402px",
          // height:"313px",
          borderRadius: "10px"
        }}
      >
        <img src={Familyframe} alt="" c />
      </div>
      <h1 className="subheadeing ">
      New Home Loan
        </h1>
        {/* <h2
              className="app-heading-primary text-secondary leading-none lg:leading-tight mt-[6px]"
            >
              Easy &amp; Fast with Easiloan!
            </h2> */}
        <p className="mt-3 app-text-dm-small  opacity-80 deamlinedesk " style={{fontSize:"15px"}}>
        Make your dream home a reality with Home Loan<br></br>
interests as low as 6.75%*

        </p>

        <p className="mt-3 app-text-dm-small  opacity-80 dreamline" style={{fontSize:"16px"}}>
        Make your dream home a reality with Home Loan<br></br>
interests as low as 6.75%*

        </p>
       
          {/* <svg
                xmlns="http://www.w3.org/2000/svg"
                width="12"
                height="14"
                viewBox="0 0 16 18"
              >
                <path
                  d="M14.667 6h-9V3.667a2.333 2.333 0 114.666 0 .664.664 0 00.667.666.665.665 0 00.667-.666A3.67 3.67 0 008 0a3.667 3.667 0 00-3.667 3.667V6h-3A1.333 1.333 0 000 7.333v9.334A1.333 1.333 0 001.333 18h13.334A1.333 1.333 0 0016 16.667V7.333A1.333 1.333 0 0014.667 6zM8 13a1 1 0 110-1.999A1 1 0 018 13z"
                  fill="#FFF"
                  fillRule="nonzero"
                ></path>
              </svg> */}
              <div style={{display:"flex"}} className="konwtab">
          {/* <span className="knowmore">Know More </span> */}
          <span >
            {/* <img src={Arrow1} alt="" className="arrow"  /> */}
            </span>
          </div>
      </div>
      <div
        className="col-span-7"
        style={{
        textAlign: "center",
        }}
      >
            <div className="relative overflow-hidden" >
    <div className="">
      <div className="mb-0 lg:mb-4 amtlinemobileview">
        <h1 className="amttenure">
        Get started by choosing the amount and  <br />
tenure and get started right away
        
       
        </h1>

        <h1 className="amttenure amtmobile">
        Get started by choosing the
        <br />amount and  tenure and get <br/>started right away
        
       
        </h1>
      </div>

      
          <button
            style={{
              width: "164px",
              height: "48px",
              marginTop: "45px",
              background: "linear-gradient(to right, #7FCD91 0%, #2EB086 74.71%)"
            }}
            type="button"
            className=" app-btn app-btn-primary comingbtn"
            data-testid="unlock-loan-offers"
            data-category="unlock_loan"
            data-action="Unlock Loan offers"
            data-label="-"
          >
               
          
            <span className="getstart">Coming Soon </span>
          </button>
         
      {/* <div className="relative mt-9">
        <form>
          <div className=" ">
       
            <div style={{ display: "flex" }}>
              <label className="col-span-6 lg:col-span-3">
                <div className="app-form-wrapper">
                  <span className="app-form-label">
                    Required Loan Amount
                  </span>
                </div>
                <div className="relative rounded-md" role="presentation">
                  <div className="relative" style={{marginTop:"44px"}}>
                    
                    <div className="absolute bottom-[1px] w-full" style={{width:"310px"}}>
                      <div className="range-wrapper">
                        <p style={{ marginLeft: "10px",marginbottom: "25px" }}>{rangeval1}</p>
                        <input
                         
                          min="1"
                          max="60000000"
                          step="1"
                          className="range-thumb range-thumb--left"
                          style={{ zIndex: "5" }}
                         
                          type="range"
                          defaultValue={0}
                          onChange={(event) =>
                            setRangeval1(event.target.value)
                          }
                        />

                        <output style={{ float: "right" }}></output>
                        <div className="range-slider" style={{marginbottom: "30px"}}>
                          <div className="range-slider__track"></div>
                          <div className="range-slider__range"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <p className="amtword"
                    
                  >
                    {amountInWords1}
                  </p>
                </div>
              </label>
              <label
              
                style={{ marginLeft: "20px" }}
                className=""
              >
                <div className="app-form-wrapper" style={{marginLeft:"192px"}}>
                  <span
                    
                    className="app-form-label"
                  >
                    Preferred Tenure (In YEARS)
                  </span>
                </div>
                <div className="relative rounded-md" role="presentation">
                  <div className="relative" style={{marginTop:"44px"}}>
                   
                    <div className="absolute bottom-[1px] w-full" style={{width:"310px",marginLeft:"200px"}}>
                      <div className="range-wrapper">
                        <p style={{ marginLeft: "10px",marginbottom: "25px"}}>{rangeval2}</p>
                        <input
                       
                          min="1"
                          max="40"
                          step="1"
                          className="range-thumb range-thumb--left"
                          style={{ zIndex: "5" }}
                      
                          type="range"
                          defaultValue={0}
                          onChange={(event) =>
                            setRangeval2(event.target.value)
                          }
                        />
                        <b></b> <output style={{ float: "right" }}></output>
                        <div className="range-slider"style={{marginbottom: "30px"}}>
                          <div className="range-slider__track"></div>
                          <div className="range-slider__range"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <p
                    className="amtword2"
                   
                  >
                    {amountInWords2}
                  </p>
                </div>
              </label>
            </div>
          </div>
          <Link
              to={{
                pathname: "/MultiStepForm",
                state: { val1: rangeval1, val2: rangeval2 },
              }}
            >
          <button
            style={{
              width: "164px",
              height: "48px",
              marginTop: "45px",
              background: "linear-gradient(to right, #7FCD91 0%, #2EB086 74.71%)"
            }}
            type="button"
            className=" app-btn app-btn-primary"
            data-testid="unlock-loan-offers"
            data-category="unlock_loan"
            data-action="Unlock Loan offers"
            data-label="-"
          >
               
          
            <span className="getstart">Find Loan Offers </span>
          </button>
          </Link>

        </form>
      </div> */}
    </div>
  </div>
      </div>
    </div>
  </div>
</div>



</div>
 </div>
  );
}
export default Form1;
