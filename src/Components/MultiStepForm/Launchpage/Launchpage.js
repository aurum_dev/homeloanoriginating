import React from "react";
import Form1 from "./Form1";
import Mainboard from "./Mainboard";
import Contactform from "./Contactform";
import Workflow  from "./Workflow";
import Homeloan  from "./Homeloan";
function Launchpage() {
   
  return (
    <div>
      <Mainboard />
      <Contactform/>
      <Homeloan />
      <Form1 />
      <Workflow />
      {/* <Contactform/> */}
      
    </div>
  );
}
export default Launchpage;
