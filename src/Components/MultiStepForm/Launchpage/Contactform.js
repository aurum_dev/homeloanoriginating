import React, { useState, Component } from "react";
// import contactformiamge from "../../../assets/images/contactformiamge.jpg";
import "../../../assets/css/Contactform/Contactform.css";
import contactmobimg from "../../../assets/images/contactmobimg.png";
import axios from "axios";
import validator from "validator";

function Contactform() {
  const [user, setUser] = useState("");
  const [number, setNumber] = useState("");
  const [email, setEmail] = useState("");
  const [msg, setMsg] = useState("");
  const [userErr, setUserErr] = useState(false);
  const [passErr, setPassErr] = useState(false);
  const [emailErr, setEmailErr] = useState(false);
  const [msgErr, setMsgErr] = useState(false);

  const submitHandle = async (e) => {
    e.preventDefault();
    if (userErr || passErr || emailErr || msgErr || user ==="" || number==="" || email ==="" || msg==="") {
      alert("Please Complete the form");
    } else {
      console.log(user);
      try {
        const response = await axios({
          method: "post",
          url: "https://vsitr_uat.recx.in/home-loan/api/kuber-submit-contactForm",
          headers: {
            "Content-Type": "application/json",
          },
          data: {
            name: user,
            number: number,
            email: email,
            message: msg,
          },
        });
        console.log(response);
        if (response.data.status == "error") {
          console.log("Not Submitted!!");
        } else {
          console.log("Submitted!!");
          setUser("");
          setNumber("");
          setEmail("");
          setMsg("");

          // window.Location.href = '/'
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  function nameHandler(e) {
    let item = e.target.value;
    if (
      item.length === 0 ||
      item.match(/^\s+$/) !== null ||
      item.match(/^[0-9]+$/)
    ) {
      setUserErr(true);
    } else {
      setUserErr(false);
    }
    setUser(item);
  }
  function numberHandler(e) {
    let item = e.target.value;
    const phoneno = new RegExp(/^[6-9][0-9]{9}$/)
    if (item.length < 10 || item.length > 10 ||  (phoneno.test(item) === false)) {
      setPassErr(true);
    } else {
      setPassErr(false);
    }
    setNumber(item);
  }

  function emailHandler(e) {
    let item = e.target.value;
    const regex = new RegExp(/^[a-zA-Z][a-zA-Z0-9\-\_\.]+@[a-zA-Z0-9]{2,}\.[a-zA-Z0-9]{2,}$/)
    // console.log(regex.test(item),"email match");

    if (
      item.length === 0 ||
      (regex.test(item) === false)
    ) {
      setEmailErr(true);
    } else {
      setEmailErr(false);
    }
    // if (item.length < 1) {
    //   setEmailErr(true);
    // } else {
    //   setEmailErr(false);
    // }
    // setEmail(item);
    // if (validator.isEmail(item)) {
    //   setEmail('Valid Email :)')
    // } else {
    //   setEmail('Enter valid Email!')
    // }
    setEmail(item);
  }

  function msgHandler(e) {
    let item = e.target.value;
    if (item.length < 1 || item.length > 100) {
      setMsgErr(true);
    } else {
      setMsgErr(false);
    }
    setMsg(item);
  }
  return (
    <div style={{ border: "none !important" }} class="contactmaindiv">
      {/* <div id="contact" style={{ paddingTop: "120px",border:"none !important" }}></div> */}
      <div id="contact" style={{ border: "none !important" }}></div>
      <div
        className="relative mobilespace"
        style={{ paddingTop: "120px", border: "none !important" }}
      >
        <section
          className="contact-form-area contactdiv"
          style={{ paddingbottom: "50px", border: "none !important" }}
        >
          <div
            className="contactdiv"
            style={{
              height: "777px",
              width: "100%",
              backgroundColor: "white",
              border: "none !important",
            }}
          >
            {/* <img
              className="approvaldeskcontact"
              // src={contactformiamge}
              alt=""
              style={{
                height: "777px",
                width: "100%",
                backgroundColor:"white",
                border:"none !important" ,
              }}
            /> */}
          </div>

          {/* <div    style={{
                height: "305px",
                width: "100%",
              }}>
            <img
              className="approvalmobcontact"
              src={contactmobimg}
              alt=""
              style={{
                height: "305px",
                width: "100%",
              }}
            />
          </div> */}

          <div className="container">
            <div className="top-left">
              {" "}
              <h1 style={{ color: "black" }}>Get In Touch</h1> <hr />
              <p style={{ color: "black" }}>
                Contact us if you’re looking for Home loan
              </p>
            </div>
            <div className="top-right">
              {" "}
              <div className="col-sm-12">
                <div className="box">
                  <form onSubmit={submitHandle}>
                    <div className="row">
                      <div className="col-lg-12 col-md-12 col-sm-12 form-group">
                        {" "}
                        <label
                          htmlFor="name"
                          className="col-form-label"
                          style={{ float: "left" }}
                        >
                          Name{" "}
                        </label>{" "}
                        <input
                          type="text"
                          className="form-control"
                          name="name"
                          id="name"
                          placeholder="Enter Name"
                          value={user}
                          onChange={nameHandler}
                        />
                        {userErr ? (
                          <span style={{ color: "red" }}>
                            Please Enter Name
                          </span>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                    {/* <input type="text" placeholder="Enter User Name" onChange={nameHandler} />{userErr?<span>User Not Valid</span>:""} */}

                    <div className="row">
                      <div className="col-lg-12 col-md-12 col-sm-12 form-group">
                        {" "}
                        <label
                          htmlFor="number"
                          className="col-form-label"
                          style={{ float: "left" }}
                        >
                          Mobile number{" "}
                        </label>{" "}
                        <input
                          className="form-control"
                          name="number"
                          id="number"
                          type="number"
                          placeholder="Enter Mobile number"
                          value={number}
                          onChange={numberHandler}
                        />
                        {passErr ? (
                          <span style={{ color: "red" }}>
                            Please enter 10 Digit mobile number
                          </span>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-lg-12 col-md-12 col-sm-12 form-group">
                        {" "}
                        <label
                          htmlFor="email"
                          className="col-form-label"
                          style={{ float: "left" }}
                        >
                          Email{" "}
                        </label>{" "}
                        <input
                          type="text"
                          className="form-control"
                          name="email"
                          id="email"
                          placeholder="Enter Email"
                          onChange={emailHandler}
                          value={email}
                        />
                        {emailErr ? (
                          <span style={{ color: "red" }}>
                            Please enter valid email address
                          </span>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-lg-12 col-md-12 col-sm-12 form-group">
                        {" "}
                        <label
                          htmlFor="msg"
                          className="col-form-label"
                          style={{ float: "left" }}
                        >
                          How can we help?{" "}
                        </label>{" "}
                        <textarea
                          type="text"
                          className="form-control"
                          name="msg"
                          id="msg"
                          placeholder="Enter Message"
                          onChange={msgHandler}
                          value={msg}
                        ></textarea>
                        {msgErr ? (
                          <span style={{ color: "red" }}>
                            Please Enter Message{" "}
                          </span>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>

                    <button
                      type="submit"
                      style={{
                        width: "115px",
                        height: "48px",
                        marginTop: "20px",
                        background:
                          "linear-gradient(to right, #7FCD91 0%, #2EB086 74.71%)",
                        float: "left",
                      }}
                    >
                      Submit
                    </button>
                  </form>
                  <div id="form-message-warning mt-4"></div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}

export default Contactform;
