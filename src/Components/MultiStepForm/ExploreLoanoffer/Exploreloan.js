import React, { useContext, useState } from "react";
import { useLocation } from "react-router-dom";
import Header from "../../MultiStepForm/header/Header";
import "../../../assets/css/ExploreLoan/Exploreloan.css";
import girt from  "../../../assets/images/girt-surprice.svg"
import hdfc from "../../../assets/images/hdfc.webp"
import bajaj from "../../../assets/images/bajaj.webp"
import pnb from "../../../assets/images/pnb.webp"
import icici from "../../../assets/images/icici.webp"
import axis from "../../../assets/images/axis.webp"
import lnt from "../../../assets/images/lnt.webp"
 import iifl from "../../../assets/images/iifl.webp"
 import { useSelector } from "react-redux";
 import { Formik, Field } from "formik";
import { Button } from "antd";
import { Input } from "formik-antd";
import MultiStepFormContext from "../MultiStepFormContext";


 import {
  Link
} from "react-router-dom";

const Exploreloan = () => {
  const { exploreloan, setExploreloan, next, prev } =
    useContext(MultiStepFormContext);
  const location = useLocation();
  const val1 = location.state?.val1;
  const val2 = location.state?.val2;
  const amt1 = useSelector((state) => state.setValue1.state);
  const amt2 = useSelector((state) => state.setValue2.state);

  const numWords = require('num-words')
  const amountInWords5 = numWords(amt1)
  const amountInWords6 = numWords(amt2)
  console.log(val1);

  const [selectedOption, setSelectedOption] = useState(null);
  const handleChange = (e) => {
    setSelectedOption(e.target.value);
    console.log("çhange");
  };
  return (
    <Formik
      initialValues={exploreloan}
      onSubmit={(values) => {
        setExploreloan(values);
        next();
      }}
      validate={(values) => {
        const errors = {};
        if (!values.explorename) errors.explorename = "Full name is required";
      
        return errors;
      }}
    >
      {({ handleSubmit, errors }) => {
        <Header />;
        return (
          <div className={"details__wrapper"}>
     {/* <h1>{val1}</h1>
      <h1>{val2}</h1> */}
      <div className="bg-floral-gradiant overflow-y-auto">
        <div className="container px-4 sm:px-0 mx-auto mt-24 sm:mt-28 sm:w-7/12">
          {/* <ol className="inline-flex items-center justify-between w-full rounded-md">
            <li className="flex w-auto sm:min-w-[106px] justify-center sm:min-h-[72px]">
              <button
                type="button"
                className="flex items-center group focus:outline-none group-hover:text-secondary"
              >
                <div className="flex flex-col sm:h-full sm:justify-between items-center text-xs font-medium">
                  <div className="text-white rounded-full">
                    <div className="text-secondary">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="transparent"
                        width="24"
                        height="24"
                        viewBox="0 0 20 20"
                      >
                        <g fill="currentColor" fill-rule="evenodd">
                          <g fill="currentColor" fill-rule="nonzero">
                            <path
                              d="M24.996 16c4.968.011 8.993 4.034 9.004 9 0 2.695-1.209 5.248-3.292 6.957-2.084 1.71-4.824 2.396-7.468 1.87-4.538-.906-7.65-5.104-7.196-9.708.453-4.604 4.324-8.115 8.952-8.119zm4.722 6.386c-.304-.304-.796-.304-1.1 0L24.503 26.5l-2.042-2.042c-.304-.303-.796-.303-1.1 0-.303.304-.303.796 0 1.1l2.592 2.59c.304.304.796.304 1.1 0l4.665-4.663c.304-.304.304-.796 0-1.1z"
                              transform="translate(-16 -16)"
                            ></path>
                          </g>
                        </g>
                      </svg>
                    </div>
                  </div>
                  <div className="font-body text-base sm:text-sm font-bold text-primary absolute sm:relative w-full sm:w-auto text-center left-0 top-8 sm:top-0 hidden sm:block">
                    Basic Details
                  </div>
                </div>
              </button>
            </li>
            <li className="flex-1 sm:flex-auto sm:min-h-[72px]">
              <div className="border-b-2 sm:h-[13px] sm:w-[102px] sm:m-auto  border-secondary"></div>
            </li>
            <li className="flex w-auto sm:min-w-[106px] justify-center sm:min-h-[72px]">
              <button
                type="button"
                className="flex items-center group focus:outline-none group-hover:text-secondary"
              >
                <div className="flex flex-col sm:h-full sm:justify-between items-center text-xs font-medium">
                  <div className="text-white rounded-full">
                    <div className="text-secondary">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="transparent"
                        width="24"
                        height="24"
                        viewBox="0 0 20 20"
                      >
                        <g fill="currentColor" fill-rule="evenodd">
                          <g fill="currentColor" fill-rule="nonzero">
                            <path
                              d="M24.996 16c4.968.011 8.993 4.034 9.004 9 0 2.695-1.209 5.248-3.292 6.957-2.084 1.71-4.824 2.396-7.468 1.87-4.538-.906-7.65-5.104-7.196-9.708.453-4.604 4.324-8.115 8.952-8.119zm4.722 6.386c-.304-.304-.796-.304-1.1 0L24.503 26.5l-2.042-2.042c-.304-.303-.796-.303-1.1 0-.303.304-.303.796 0 1.1l2.592 2.59c.304.304.796.304 1.1 0l4.665-4.663c.304-.304.304-.796 0-1.1z"
                              transform="translate(-16 -16)"
                            ></path>
                          </g>
                        </g>
                      </svg>
                    </div>
                  </div>
                  <div className="font-body text-base sm:text-sm font-bold text-primary absolute sm:relative w-full sm:w-auto text-center left-0 top-8 sm:top-0 hidden sm:block">
                    Personal Information
                  </div>
                </div>
              </button>
            </li>
            <li className="flex-1 sm:flex-auto sm:min-h-[72px]">
              <div className="border-b-2 sm:h-[13px] sm:w-[102px] sm:m-auto  border-secondary"></div>
            </li>
            <li className="flex w-auto sm:min-w-[106px] justify-center sm:min-h-[72px]">
              <button
                type="button"
                className="flex items-center group focus:outline-none group-hover:text-secondary"
              >
                <div className="flex flex-col sm:h-full sm:justify-between items-center text-xs font-medium">
                  <div className="text-white rounded-full">
                    <div className="text-secondary">
                      <svg
                        width="25"
                        height="24"
                        viewBox="0 0 25 24"
                        fill="none"
                      >
                        <circle
                          cx="12.666"
                          cy="12"
                          r="9.5"
                          fill="white"
                          stroke="currentColor"
                        ></circle>
                        <circle
                          cx="12.666"
                          cy="12"
                          r="6.5"
                          fill="currentColor"
                          stroke="currentColor"
                        ></circle>
                      </svg>
                    </div>
                  </div>
                  <div className="font-body text-base sm:text-sm font-bold text-primary absolute sm:relative w-full sm:w-auto text-center left-0 top-8 sm:top-0 block">
                    Explore Loan Offers
                  </div>
                </div>
              </button>
            </li>
            <li className="flex-1 sm:flex-auto sm:min-h-[72px]">
              <div className="border-b-2 sm:h-[13px] sm:w-[102px] sm:m-auto  border-secondary"></div>
            </li>
            <li className="flex w-auto sm:min-w-[106px] justify-center sm:min-h-[72px]">
              <button
                type="button"
                className="flex items-center group focus:outline-none group-hover:text-secondary"
              >
                <div className="flex flex-col sm:h-full sm:justify-between items-center text-xs font-medium">
                  <div className="text-white rounded-full">
                    <div className="text-secondary">
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                      >
                        <circle
                          cx="10"
                          cy="10"
                          r="9.5"
                          fill="white"
                          stroke="#003B70"
                        ></circle>
                      </svg>
                    </div>
                  </div>
                  <div className="font-body text-base sm:text-sm font-bold text-primary absolute sm:relative w-full sm:w-auto text-center left-0 top-8 sm:top-0 hidden sm:block">
                    Document Verification
                  </div>
                </div>
              </button>
            </li>
            <li className="flex-1 sm:flex-auto sm:min-h-[72px]">
              <div className="border-b-2 sm:h-[13px] sm:w-[102px] sm:m-auto  border-gray-300"></div>
            </li>
            <li className="flex w-auto sm:min-w-[106px] justify-center sm:min-h-[72px]">
              <button
                type="button"
                className="flex items-center group focus:outline-none group-hover:text-secondary"
              >
                <div className="flex flex-col sm:h-full sm:justify-between items-center text-xs font-medium">
                  <div className="text-white rounded-full">
                    <div className="text-secondary">
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                      >
                        <circle
                          cx="10"
                          cy="10"
                          r="9.5"
                          fill="white"
                          stroke="#003B70"
                        ></circle>
                      </svg>
                    </div>
                  </div>
                  <div className="font-body text-base sm:text-sm font-bold text-primary absolute sm:relative w-full sm:w-auto text-center left-0 top-8 sm:top-0 hidden sm:block">
                    Provisional Offer Letter
                  </div>
                </div>
              </button>
            </li>
          </ol> */}
          <div className="border mt-10 mx-auto bg-white border-gray-300 rounded-md pb-5">
            <div className="flex flex-col items-center justify-center px-4 sm:justify-start sm:flex-row">
              <div className="">
                <svg
                  width="200px"
                  height="174px"
                  viewBox="0 0 600 474"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                  className="flex-shrink-0"
                >
                  <defs>
                    <path
                      d="M147.270108,349.978664 C72.0692778,349.978664 -6.28012777,223.081522 120.74622,158.633892 C147.270108,145.176828 166.691015,133.997114 175.634119,113.188091 C186.59327,87.6927936 212.749334,59.533406 235.959181,40.1857785 C283.474507,0.577209331 368.080074,-1.10070034 420.359396,40.9084368 C472.638719,82.9175739 462.109915,90.2102015 481.105991,111.313873 C505.990783,138.959608 547.697499,166.394811 564.478415,191.971688 C581.607088,218.078604 589.130481,247.864867 586.62723,271.371433 C581.607088,318.530722 548.613425,350.391368 486.993179,350.882305 C478.126033,350.952951 466.601222,350.319976 457.832537,349.978664 L175.634119,349.978664 L147.270108,349.978664 Z"
                      id="path-1"
                    ></path>
                    <pattern
                      id="pattern-2"
                      width="512"
                      height="512"
                      x="-458.543779"
                      y="-502.06058"
                      patternUnits="userSpaceOnUse"
                    >
                      <use xlinkHref="https://i.imgur.com/w7GCRPb.png"></use>
                    </pattern>
                    <filter
                      x="-16.3%"
                      y="-16.3%"
                      width="132.7%"
                      height="132.7%"
                      filterUnits="objectBoundingBox"
                      id="filter-4"
                    >
                      <feOffset
                        dx="0"
                        dy="10"
                        in="SourceAlpha"
                        result="shadowOffsetOuter1"
                      ></feOffset>
                      <feGaussianBlur
                        stdDeviation="7.5"
                        in="shadowOffsetOuter1"
                        result="shadowBlurOuter1"
                      ></feGaussianBlur>
                      <feColorMatrix
                        values="0 0 0 0 0.148003598   0 0 0 0 0.206935823   0 0 0 0 0.259684245  0 0 0 0.15 0"
                        type="matrix"
                        in="shadowBlurOuter1"
                        result="shadowMatrixOuter1"
                      ></feColorMatrix>
                      <feMerge>
                        <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                      </feMerge>
                    </filter>
                    <filter
                      x="-29.8%"
                      y="-21.2%"
                      width="159.7%"
                      height="159.4%"
                      filterUnits="objectBoundingBox"
                      id="filter-6"
                    >
                      <feOffset
                        dx="0"
                        dy="2"
                        in="SourceAlpha"
                        result="shadowOffsetOuter1"
                      ></feOffset>
                      <feGaussianBlur
                        stdDeviation="2"
                        in="shadowOffsetOuter1"
                        result="shadowBlurOuter1"
                      ></feGaussianBlur>
                      <feColorMatrix
                        values="0 0 0 0 0.148003598   0 0 0 0 0.206935823   0 0 0 0 0.259684245  0 0 0 0.2 0"
                        type="matrix"
                        in="shadowBlurOuter1"
                      ></feColorMatrix>
                    </filter>
                    <polygon
                      id="path-7"
                      points="8.50365177e-13 64.1589043 47.4242709 64.1589043 47.4242709 -5.23902478e-14 8.38243965e-13 -5.23902478e-14 8.38243965e-13 64.1589043"
                    ></polygon>
                    <polygon
                      id="path-9"
                      points="4.8526684e-14 64.1589043 30.4921156 64.1589043 30.4921156 0 3.6395013e-14 0 0 64.1589043"
                    ></polygon>
                    <polygon
                      id="path-11"
                      points="0 64.1589043 13.5771404 64.1589043 13.5771404 0 0 0"
                    ></polygon>
                    <polygon
                      id="path-13"
                      points="0 64.1589043 7.15469432 64.1589043 7.15469432 0 0 0"
                    ></polygon>
                    <polygon
                      id="path-15"
                      points="0 64.1589043 4.03880954 64.1589043 4.03880954 0 0 0"
                    ></polygon>
                    <path
                      d="M36.180024,58.6672076 C34.1263723,60.1471754 26.608137,56.0500181 24.7345018,55.0975949 C21.5050177,53.4565207 18.2650378,51.597479 16.0191247,48.7670603 C13.700696,45.8434527 12.6807082,42.1648562 12.4824038,38.4057065 C12.4113195,37.0947425 12.4431245,35.7727222 12.5536471,34.4696556 C12.5792501,34.1569196 12.6110552,33.8441836 12.6458817,33.5346065 C12.8820341,31.4654947 13.2875484,29.423234 13.7491987,27.3920295 C14.1365841,25.6846174 14.5632488,23.9835232 14.9628791,22.2781644 C15.3063736,20.8141493 15.6287177,19.3455537 15.8874518,17.8649542 C15.9222783,17.6651506 15.9540833,17.4577656 15.9782552,17.2472216 C16.0887777,16.2521526 16.0130817,15.2030655 15.2640729,14.5897555 C14.8886144,14.2816 14.4180587,14.1584009 13.927625,14.1508195 C13.7961111,14.1478184 13.6644382,14.1538205 13.5311751,14.1674039 C13.2392048,14.197414 12.9485067,14.2605929 12.6746652,14.3447789 C11.5139402,14.70253 10.4939524,15.357854 9.48907203,16.052349 C8.73847299,16.5708906 7.99693839,17.1104391 7.21151283,17.5659596 C5.37429446,18.6286302 2.8605828,19.1471717 1.24281924,17.7779251 C-0.774574705,16.0717766 -0.00727801753,12.8221654 1.23073332,10.4968627 C2.64255923,7.84239759 4.46021749,5.27053897 6.64999478,3.33757786 C8.15723581,2.00592276 9.84163095,0.97768474 11.6925255,0.432134186 C14.0154069,-0.25177835 16.5987716,-0.173594353 19.4241727,1.02585872 C20.592531,1.52181377 21.564016,2.1967233 22.3677295,3.00841533 C23.6706231,4.31764189 24.5393779,5.98003899 25.1083701,7.80480609 C26.2115281,11.3505688 26.1827445,15.5126425 25.9890518,18.9021952 C25.9709229,19.2327792 25.9483414,19.5635212 25.9240105,19.8926837 C25.7909064,21.7068684 25.5835375,23.5159987 25.3671042,25.3244972 C25.0342644,28.1185879 24.6815464,30.9095196 24.5559165,33.7099282 C24.5105943,34.7555405 24.4954869,35.8011527 24.5242705,36.8499239 C24.552895,37.875003 24.6316125,38.8937641 24.7648757,39.8951511 C25.50482,45.481753 27.93234,50.5850357 32.8144143,53.8782404 C33.9660749,54.6537625 38.5908464,56.928206 36.180024,58.6672076"
                      id="path-17"
                    ></path>
                    <polygon
                      id="path-19"
                      points="28.5714286 49.7695853 4.39051921e-15 49.7695853 0 0 28.5714286 0"
                    ></polygon>
                  </defs>
                  <g
                    id="Assets"
                    stroke="none"
                    strokeWidth="1"
                    fill="none"
                    fillRule="evenodd"
                  >
                    <g id="1" transform="translate(-73.000000, -65.000000)">
                      <g
                        id="Group-4"
                        transform="translate(73.000000, 83.000000)"
                      >
                        <g
                          id="Fill-1"
                          opacity="0.150000006"
                          transform="translate(320.276498, 180.413589) scale(-1, 1) translate(-320.276498, -180.413589) "
                        >
                          <use
                            fill="#FBCF48"
                            xlinkHref="https://i.imgur.com/w7GCRPb.png"
                          ></use>
                          <use
                            fillOpacity="0.229999989"
                            fill="url(#pattern-2)"
                            xlinkHref="https://i.imgur.com/w7GCRPb.png"
                          ></use>
                        </g>
                        <g
                          id="Group"
                          transform="translate(90.322581, 57.266223)"
                        >
                          <path
                            d="M442.650691,10.6442396 L442.650691,297.804608 C442.650691,303.683871 437.885714,308.44977 432.007373,308.44977 L10.6442396,308.44977 C4.76589862,308.44977 0,303.683871 0,297.804608 L0,10.6442396 C0,4.76589862 4.76589862,0 10.6442396,0 L432.006452,0 C437.885714,0 442.650691,4.76589862 442.650691,10.6442396"
                            id="Fill-41"
                            fill="#1A3E6F"
                          ></path>
                          <polygon
                            id="Fill-42"
                            fill="#FFFFFE"
                            points="31.5797235 299.865438 401.247926 299.865438 401.247926 10.8700461 31.5797235 10.8700461"
                          ></polygon>
                          <path
                            d="M420.407373,142.38894 C426.943779,142.38894 432.242396,147.687558 432.242396,154.223963 C432.242396,160.76129 426.943779,166.058986 420.407373,166.058986 C413.870968,166.058986 408.57235,160.76129 408.57235,154.223963 C408.57235,147.687558 413.870968,142.38894 420.407373,142.38894"
                            id="Fill-43"
                            fill="#FFFFFF"
                          ></path>
                          <path
                            d="M17.7926267,108.953917 L17.7926267,199.614747 C17.7926267,200.23318 17.2912442,200.734562 16.6728111,200.734562 L14.0230415,200.734562 C13.4046083,200.734562 12.9032258,200.23318 12.9032258,199.614747 L12.9032258,108.953917 C12.9032258,108.335484 13.4046083,107.834101 14.0230415,107.834101 L16.6728111,107.834101 C17.2912442,107.834101 17.7926267,108.335484 17.7926267,108.953917"
                            id="Fill-44"
                            fill="#FFFFFF"
                          ></path>
                        </g>
                        <path
                          d="M149.116264,106.272486 L151.130106,105.205746 C151.362533,105.09612 151.619204,105.300087 151.564918,105.55096 L150.96514,107.741889 C150.943531,107.841501 150.971992,107.944802 151.042089,108.019115 L152.440341,109.505384 C152.616374,109.691958 152.501478,109.999225 152.245861,110.025578 L150.216208,110.231652 C150.115016,110.241666 150.024891,110.301222 149.975876,110.390293 L148.927582,112.548018 C148.795821,112.952262 148.522284,112.8848 148.418983,112.650265 L147.61682,110.2891 C147.57571,110.195286 147.491383,110.128351 147.391244,110.109378 L145.022174,109.782083 C144.770246,109.734121 144.799234,109.521195 144.990551,109.350432 L146.758789,107.834122 C146.835211,107.766133 146.872631,107.665467 146.859982,107.564275 L146.370884,105.272153 C146.338207,105.01759 146.611744,104.836287 146.83363,104.965413 L148.828497,106.260364 C148.916514,106.311487 149.024031,106.316231 149.116264,106.272486"
                          id="Fill-1-Copy-2"
                          fill="#FF9715"
                        ></path>
                        <path
                          d="M471.696909,199.360043 L473.710751,198.293303 C473.943178,198.183678 474.199849,198.387644 474.145563,198.638518 L473.545785,200.829447 C473.524177,200.929058 473.552637,201.032359 473.622734,201.106673 L475.020986,202.592941 C475.197019,202.779515 475.082123,203.086783 474.826506,203.113135 L472.796854,203.31921 C472.695661,203.329224 472.605536,203.38878 472.556521,203.477851 L471.508227,205.635575 C471.376466,206.039819 471.102929,205.972358 470.999628,205.737822 L470.197465,203.376658 C470.156355,203.282844 470.072028,203.215909 469.971889,203.196935 L467.602819,202.86964 C467.350891,202.821679 467.379879,202.608753 467.571197,202.43799 L469.339435,200.92168 C469.415856,200.853691 469.453276,200.753025 469.440627,200.651832 L468.951529,198.359711 C468.918852,198.105148 469.192389,197.923844 469.414275,198.05297 L471.409143,199.347921 C471.497159,199.399045 471.604676,199.403788 471.696909,199.360043"
                          id="Fill-1-Copy-4"
                          fill="#FF9715"
                        ></path>
                        <g
                          id="Group-13"
                          filter="url(#filter-4)"
                          transform="translate(164.055300, 18.556545)"
                        >
                          <g id="Group-3">
                            <path
                              d="M296.650199,148.325099 C296.650199,230.242237 230.242886,296.650199 148.324222,296.650199 C66.4073128,296.650199 0,230.242237 0,148.325099 C0,66.407962 66.4073128,0 148.324222,0 C230.242886,0 296.650199,66.407962 296.650199,148.325099 Z"
                              id="Fill-1"
                              fill="#EDF1F7"
                            ></path>
                            <path
                              d="M248.126689,146.989824 C248.126689,199.889535 203.790572,242.773811 149.097001,242.773811 C94.4060465,242.773811 50.0690568,199.889535 50.0690568,146.989824 C50.0690568,94.0892415 94.4060465,51.2049653 149.097001,51.2049653 C203.790572,51.2049653 248.126689,94.0892415 248.126689,146.989824"
                              id="Fill-3"
                              fill="#FFFFFF"
                            ></path>
                            <g
                              id="Group-2"
                              transform="translate(8.725006, 7.852505)"
                            >
                              <path
                                d="M57.845829,135.85471 C58.3239593,114.981879 66.8386925,96.0660661 80.4558091,82.0022292 L39.6629168,41.7563947 C15.6141832,66.1288261 0.646435683,99.2681433 0.156090355,135.85471 L57.845829,135.85471 Z"
                                id="Fill-198"
                                fill="#FFA600"
                              ></path>
                              <path
                                d="M57.816862,141.204709 C58.0253896,132.102783 63.6390584,108.403922 63.6390584,108.403922 L12.3918641,81.2490867 C12.3918641,81.2490867 -0.349261984,108.049687 0.155915854,139.570516 L57.816862,141.204709 Z"
                                id="Fill-200"
                                fill="#FE9000"
                              ></path>
                              <path
                                d="M83.1185064,79.3756534 C97.3742935,65.9417619 116.545749,57.5439438 137.70127,57.071921 L137.70127,0.153472853 C100.618251,0.637710677 67.0295957,15.4030381 42.3264866,39.128074 L83.1185064,79.3756534 Z"
                                id="Fill-202"
                                fill="#FFD602"
                              ></path>
                              <path
                                d="M80.4558091,81.2490867 C90.712926,70.5679345 107.018042,63.7022274 107.018042,63.7022274 L85.442848,10.895002 C85.442848,10.895002 56.0491758,24.0531834 38.0407637,42.9035585 L80.4558091,81.2490867 Z"
                                id="Fill-204"
                                fill="#F9BD05"
                              ></path>
                              <path
                                d="M80.4515339,193.421863 C66.8361623,179.358026 58.3231741,160.442213 57.8459163,139.570254 L0.156177605,139.570254 C0.645650433,176.156821 15.6125255,209.294393 39.6586416,233.669442 L80.4515339,193.421863 Z"
                                id="Fill-206"
                                fill="#F86014"
                              ></path>
                              <path
                                d="M80.4515339,193.421863 C74.8221601,188.914525 63.7352952,167.884643 63.7352952,167.884643 L9.50938387,187.387649 C9.50938387,187.387649 22.9729404,219.92145 39.6586416,233.669442 L80.4515339,193.421863 Z"
                                id="Fill-208"
                                fill="#FB3E28"
                              ></path>
                              <path
                                d="M141.466983,57.0711357 C162.619887,57.5431585 181.789598,65.9427217 196.044512,79.3748682 L236.835659,39.1281612 C212.134295,15.4039978 178.546513,0.638670428 141.466983,0.153560103 L141.466983,57.0711357 Z"
                                id="Fill-210"
                                fill="#EBE400"
                              ></path>
                              <path
                                d="M137.700921,0.153560103 L137.700921,57.0711357 C151.371261,56.4534053 170.895206,62.1647941 170.895206,62.1647941 L192.582953,10.428127 C192.582953,10.428127 165.859133,-1.20928581 137.700921,0.153560103"
                                id="Fill-212"
                                fill="#FFDF00"
                              ></path>
                              <path
                                d="M198.491353,81.5935499 C212.144242,95.6966494 220.682533,114.661322 221.161535,135.591739 L279.007452,135.591739 C278.515361,98.9057065 263.507479,65.6756492 239.393308,41.2386529 L198.491353,81.5935499 Z"
                                id="Fill-214"
                                fill="#AAD000"
                              ></path>
                              <path
                                d="M196.94452,78.741037 C207.979908,88.1439758 216.280418,109.840931 216.280418,109.840931 L267.31385,82.1163523 C267.31385,82.1163523 254.740244,55.8750247 235.806981,37.668555 L196.94452,78.741037 Z"
                                id="Fill-216"
                                fill="#D7DD01"
                              ></path>
                              <path
                                d="M221.161884,139.316618 C220.683754,160.24529 212.147208,179.211707 198.495192,193.313062 L239.396274,233.669704 C263.509573,209.230962 278.514838,176.001778 279.007801,139.316618 L221.161884,139.316618 Z"
                                id="Fill-218"
                                fill="#24BD1C"
                              ></path>
                              <path
                                d="M221.161884,135.59139 C221.092957,152.210781 214.85894,168.660034 214.85894,168.660034 L268.703569,189.557296 C268.703569,189.557296 277.741802,166.144615 278.850751,134.225054 L221.161884,135.59139 Z"
                                id="Fill-220"
                                fill="#6CC509"
                              ></path>
                              <path
                                d="M39.554814,233.766464 C15.0768101,208.956037 0,175.072477 0,137.71462 C0,61.6569988 62.4919819,0 139.580899,0 C216.670688,0 279.16267,61.6569988 279.16267,137.71462 C279.16267,175.103015 264.061429,209.011005 239.549398,233.824922"
                                id="Stroke-222"
                                stroke="#FFFFFF"
                                strokeWidth="3"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                              ></path>
                            </g>
                            <path
                              d="M219.733775,215.204189 C236.812102,197.796057 247.326606,174.079746 247.326606,147.937884 C247.326606,109.027848 224.041311,75.4828176 190.470978,60.1276798 C189.732843,59.7908946 188.990345,59.4619618 188.242612,59.1434991 C176.301569,54.0350082 163.127683,51.2037438 149.281971,51.2046163 C95.1345845,51.2046163 51.2373351,94.5138003 51.2373351,147.937884 C51.2373351,173.367183 61.1881038,196.501358 77.4516843,213.766698"
                              id="Fill-263"
                              fill="#FFFFFF"
                            ></path>
                            <path
                              d="M215.340124,211.082322 C231.506688,194.603403 241.460174,172.153963 241.460174,147.408101 C241.460174,110.575489 219.418192,78.821703 187.639976,64.2858433 C186.941103,63.9673806 186.23874,63.6567704 185.530269,63.3540127 C174.227024,58.5194869 161.756373,55.8391651 148.64967,55.8400376 C97.3937502,55.8400376 55.8400374,96.8362226 55.8400374,147.408101 C55.8400374,172.153963 65.7952691,194.603403 81.9635774,211.082322"
                              id="Stroke-264"
                              stroke="#EDF1F7"
                              strokeWidth="3"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            ></path>
                            <g id="Fill-265">
                              <use
                                fill="black"
                                fillOpacity="1"
                                filter="url(#filter-6)"
                                xlinkHref="https://i.imgur.com/w7GCRPb.png"
                              ></use>
                              <use
                                fill="#D92A27"
                                fillRule="evenodd"
                                xlinkHref="https://i.imgur.com/w7GCRPb.png"
                              ></use>
                            </g>
                          </g>
                          <text
                            id="780"
                            fontFamily="IBMPlexSans-Bold, IBM Plex Sans"
                            fontSize="48"
                            fontWeight="bold"
                            line-spacing="52"
                            letterSpacing="-1"
                            fill="#1A3E6F"
                          >
                            <tspan x="102" y="173"></tspan>
                          </text>
                        </g>
                        <path
                          d="M419.907159,2.00162852 C419.907159,2.00162852 420.605916,9.12666004 415.802439,11.6302215 C415.802439,11.6302215 423.026747,11.2763882 425.072107,17.8630331 C425.072107,17.8630331 424.191342,11.4965794 428.424614,7.94042757 C428.424614,7.94042757 422.037796,8.06643295 419.907159,2.00162852"
                          id="Fill-9-Copy"
                          fill="#FFD93B"
                          transform="translate(422.113526, 9.932331) rotate(50.000000) translate(-422.113526, -9.932331) "
                        ></path>
                        <path
                          d="M557.234348,143.937112 C557.234348,143.937112 557.933105,151.062144 553.129627,153.565705 C553.129627,153.565705 560.353936,153.211872 562.399296,159.798517 C562.399296,159.798517 561.518531,153.432063 565.751803,149.875911 C565.751803,149.875911 559.364985,150.001917 557.234348,143.937112"
                          id="Fill-9-Copy-3"
                          fill="#FFD93B"
                          transform="translate(559.440715, 151.867815) rotate(50.000000) translate(-559.440715, -151.867815) "
                        ></path>
                        <path
                          d="M61.3818136,149.467066 C61.3818136,149.467066 62.0805707,156.592098 57.2770929,159.095659 C57.2770929,159.095659 64.5014014,158.741826 66.5467615,165.328471 C66.5467615,165.328471 65.6659966,158.962017 69.8992682,155.405865 C69.8992682,155.405865 63.5124501,155.531871 61.3818136,149.467066"
                          id="Fill-9-Copy-2"
                          fill="#FFD93B"
                          transform="translate(63.588181, 157.397769) scale(-1, 1) rotate(50.000000) translate(-63.588181, -157.397769) "
                        ></path>
                        <path
                          d="M181.48527,17.9853445 C181.48527,17.9853445 185.516509,20.4940846 185.754615,23.1085587 C185.992722,25.7230329 184.601756,28.2027501 182.349616,27.476109 C180.097477,26.7494679 178.771832,21.7822207 183.700452,22.8301962 C188.629072,23.8781717 188.984357,28.5397593 187.369486,32.0889926"
                          id="Line-Copy-7"
                          stroke="#F5CF0E"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          transform="translate(184.281753, 25.037169) scale(-1, 1) rotate(-80.000000) translate(-184.281753, -25.037169) "
                        ></path>
                        <path
                          d="M466.277897,92.6397223 C466.277897,92.6397223 470.309136,95.1484625 470.547242,97.7629366 C470.785348,100.377411 469.394383,102.857128 467.142243,102.130487 C464.890103,101.403846 463.564459,96.4365986 468.493079,97.4845741 C473.421699,98.5325496 473.776984,103.194137 472.162113,106.743371"
                          id="Line-Copy-7"
                          stroke="#F5CF0E"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          transform="translate(469.074379, 99.691546) rotate(-80.000000) translate(-469.074379, -99.691546) "
                        ></path>
                        <g
                          id="Group-10"
                          transform="translate(23.963134, 389.985117)"
                        >
                          <g
                            id="Group"
                            transform="translate(11.059908, 0.000000)"
                          >
                            <polygon
                              id="Fill-224"
                              fill="#EDF1F7"
                              points="553.917051 64.9669039 0 64.9669039 47.1436304 0.946063495 507.611455 0.946063495"
                            ></polygon>
                            <g
                              id="Group-228"
                              transform="translate(58.936316, 32.887452) rotate(-2.000000) translate(-58.936316, -32.887452) translate(35.224181, 0.808000)"
                            >
                              <mask id="mask-8" fill="white">
                                <use xlinkHref="https://i.imgur.com/w7GCRPb.png"></use>
                              </mask>
                              <g id="Clip-227"></g>
                              <polygon
                                id="Fill-226"
                                fill="#FFFFFF"
                                mask="url(#mask-8)"
                                points="47.4242709 -5.23902478e-14 2.0982681 64.1590771 8.38243965e-13 64.1590771 45.3089437 -5.23902478e-14"
                              ></polygon>
                            </g>
                            <g
                              id="Group-231"
                              transform="translate(85.909556, 0.808000)"
                            >
                              <mask id="mask-10" fill="white">
                                <use xlinkHref="https://i.imgur.com/w7GCRPb.png"></use>
                              </mask>
                              <g id="Clip-230"></g>
                              <polygon
                                id="Fill-229"
                                fill="#FFFFFF"
                                mask="url(#mask-10)"
                                points="30.4921156 0 1.87641162 64.1590771 0 64.1590771 28.6139966 0"
                              ></polygon>
                            </g>
                            <g
                              id="Group-234"
                              transform="translate(138.385501, 0.808000)"
                            >
                              <mask id="mask-12" fill="white">
                                <use xlinkHref="https://i.imgur.com/w7GCRPb.png"></use>
                              </mask>
                              <g id="Clip-233"></g>
                              <polygon
                                id="Fill-232"
                                fill="#FFFFFF"
                                mask="url(#mask-12)"
                                points="13.5771404 0 1.72924487 64.1590771 0 64.1590771 11.8478955 0"
                              ></polygon>
                            </g>
                            <g
                              id="Group-237"
                              transform="translate(183.530091, 0.808000)"
                            >
                              <mask id="mask-14" fill="white">
                                <use xlinkHref="https://i.imgur.com/w7GCRPb.png"></use>
                              </mask>
                              <g id="Clip-236"></g>
                              <polygon
                                id="Fill-235"
                                fill="#FFFFFF"
                                mask="url(#mask-14)"
                                points="7.15469432 0 1.76637092 64.1590771 0 64.1590771 5.3883234 0"
                              ></polygon>
                            </g>
                            <g
                              id="Group-240"
                              transform="translate(222.589823, 0.808000)"
                            >
                              <mask id="mask-16" fill="white">
                                <use xlinkHref="https://i.imgur.com/w7GCRPb.png"></use>
                              </mask>
                              <g id="Clip-239"></g>
                              <polygon
                                id="Fill-238"
                                fill="#FFFFFF"
                                mask="url(#mask-16)"
                                points="4.03880954 0 1.62201186 64.1590771 0 64.1590771 2.41679767 0"
                              ></polygon>
                            </g>
                            <polygon
                              id="Fill-241"
                              fill="#FFFFFF"
                              points="523.098825 64.9669039 521.050443 64.9669039 480.115511 0.946063495 482.165614 0.946063495"
                            ></polygon>
                            <polygon
                              id="Fill-243"
                              fill="#FFFFFF"
                              points="479.304505 64.9669039 477.422713 64.9669039 448.48628 0.946063495 450.369798 0.946063495"
                            ></polygon>
                            <polygon
                              id="Fill-245"
                              fill="#FFFFFF"
                              points="428.211131 64.9669039 426.455145 64.9669039 414.424031 0.946063495 416.180017 0.946063495"
                            ></polygon>
                            <polygon
                              id="Fill-247"
                              fill="#FFFFFF"
                              points="383.605805 64.9669039 381.992264 64.9669039 377.117758 0.946063495 378.731299 0.946063495"
                            ></polygon>
                            <polygon
                              id="Fill-249"
                              fill="#FFFFFF"
                              points="343.055509 64.9669039 341.420416 64.9669039 339.000479 0.946063495 340.635572 0.946063495"
                            ></polygon>
                            <polygon
                              id="Fill-251"
                              fill="#FFFFFF"
                              points="265.198939 64.9669039 266.820951 64.9669039 266.820951 0.946063495 265.198939 0.946063495"
                            ></polygon>
                            <polygon
                              id="Fill-253"
                              fill="#FFFFFF"
                              points="303.316218 64.9669039 304.93823 64.9669039 304.93823 0.946063495 303.316218 0.946063495"
                            ></polygon>
                          </g>
                          <g
                            id="Group-9"
                            transform="translate(518.894009, 1.090217)"
                            stroke="#45C1C5"
                            strokeLinecap="round"
                          >
                            <line
                              x1="0.501253133"
                              y1="0.397465438"
                              x2="10.5263158"
                              y2="0.397465438"
                              id="Line-4"
                            ></line>
                            <line
                              x1="2.50626566"
                              y1="3.57718894"
                              x2="12.5313283"
                              y2="3.57718894"
                              id="Line-4-Copy"
                            ></line>
                            <line
                              x1="5.51378446"
                              y1="6.75691244"
                              x2="15.5388471"
                              y2="6.75691244"
                              id="Line-4-Copy-2"
                            ></line>
                            <line
                              x1="7.51879699"
                              y1="9.93663594"
                              x2="17.5438596"
                              y2="9.93663594"
                              id="Line-4-Copy-3"
                            ></line>
                            <line
                              x1="9.52380952"
                              y1="13.1163594"
                              x2="19.5488722"
                              y2="13.1163594"
                              id="Line-4-Copy-4"
                            ></line>
                            <line
                              x1="11.5288221"
                              y1="16.2960829"
                              x2="21.5538847"
                              y2="16.2960829"
                              id="Line-4-Copy-5"
                            ></line>
                            <line
                              x1="14.5363409"
                              y1="19.4758065"
                              x2="24.5614035"
                              y2="19.4758065"
                              id="Line-4-Copy-6"
                            ></line>
                            <line
                              x1="16.5413534"
                              y1="22.65553"
                              x2="26.566416"
                              y2="22.65553"
                              id="Line-4-Copy-7"
                            ></line>
                            <line
                              x1="18.5463659"
                              y1="25.8352535"
                              x2="28.5714286"
                              y2="25.8352535"
                              id="Line-4-Copy-8"
                            ></line>
                            <line
                              x1="23.5588972"
                              y1="32.1947005"
                              x2="33.5839599"
                              y2="32.1947005"
                              id="Line-4-Copy-9"
                            ></line>
                            <line
                              x1="25.5639098"
                              y1="35.374424"
                              x2="35.5889724"
                              y2="35.374424"
                              id="Line-4-Copy-11"
                            ></line>
                            <line
                              x1="28.5714286"
                              y1="38.5541475"
                              x2="38.5964912"
                              y2="38.5541475"
                              id="Line-4-Copy-12"
                            ></line>
                            <line
                              x1="30.5764411"
                              y1="41.733871"
                              x2="40.6015038"
                              y2="41.733871"
                              id="Line-4-Copy-13"
                            ></line>
                            <line
                              x1="32.5814536"
                              y1="44.9135945"
                              x2="42.6065163"
                              y2="44.9135945"
                              id="Line-4-Copy-14"
                            ></line>
                            <line
                              x1="34.5864662"
                              y1="48.093318"
                              x2="44.6115288"
                              y2="48.093318"
                              id="Line-4-Copy-15"
                            ></line>
                            <line
                              x1="37.593985"
                              y1="51.2730415"
                              x2="47.6190476"
                              y2="51.2730415"
                              id="Line-4-Copy-16"
                            ></line>
                            <line
                              x1="40.6015038"
                              y1="55.2476959"
                              x2="50.6265664"
                              y2="55.2476959"
                              id="Line-4-Copy-17"
                            ></line>
                            <line
                              x1="43.6090226"
                              y1="59.2223502"
                              x2="53.6340852"
                              y2="59.2223502"
                              id="Line-4-Copy-18"
                            ></line>
                            <line
                              x1="46.6165414"
                              y1="63.1970046"
                              x2="56.641604"
                              y2="63.1970046"
                              id="Line-4-Copy-19"
                            ></line>
                            <line
                              x1="21.5538847"
                              y1="29.014977"
                              x2="31.5789474"
                              y2="29.014977"
                              id="Line-4-Copy-10"
                            ></line>
                          </g>
                          <g
                            id="Group-9"
                            transform="translate(28.571429, 32.887452) scale(-1, 1) translate(-28.571429, -32.887452) translate(0.000000, 1.090217)"
                            stroke="#45C1C5"
                            strokeLinecap="round"
                          >
                            <line
                              x1="0.501253133"
                              y1="0.397465438"
                              x2="10.5263158"
                              y2="0.397465438"
                              id="Line-4"
                            ></line>
                            <line
                              x1="2.50626566"
                              y1="3.57718894"
                              x2="12.5313283"
                              y2="3.57718894"
                              id="Line-4-Copy"
                            ></line>
                            <line
                              x1="5.51378446"
                              y1="6.75691244"
                              x2="15.5388471"
                              y2="6.75691244"
                              id="Line-4-Copy-2"
                            ></line>
                            <line
                              x1="7.51879699"
                              y1="9.93663594"
                              x2="17.5438596"
                              y2="9.93663594"
                              id="Line-4-Copy-3"
                            ></line>
                            <line
                              x1="9.52380952"
                              y1="13.1163594"
                              x2="19.5488722"
                              y2="13.1163594"
                              id="Line-4-Copy-4"
                            ></line>
                            <line
                              x1="11.5288221"
                              y1="16.2960829"
                              x2="21.5538847"
                              y2="16.2960829"
                              id="Line-4-Copy-5"
                            ></line>
                            <line
                              x1="14.5363409"
                              y1="19.4758065"
                              x2="24.5614035"
                              y2="19.4758065"
                              id="Line-4-Copy-6"
                            ></line>
                            <line
                              x1="16.5413534"
                              y1="22.65553"
                              x2="26.566416"
                              y2="22.65553"
                              id="Line-4-Copy-7"
                            ></line>
                            <line
                              x1="18.5463659"
                              y1="25.8352535"
                              x2="28.5714286"
                              y2="25.8352535"
                              id="Line-4-Copy-8"
                            ></line>
                            <line
                              x1="23.5588972"
                              y1="32.1947005"
                              x2="33.5839599"
                              y2="32.1947005"
                              id="Line-4-Copy-9"
                            ></line>
                            <line
                              x1="25.5639098"
                              y1="35.374424"
                              x2="35.5889724"
                              y2="35.374424"
                              id="Line-4-Copy-11"
                            ></line>
                            <line
                              x1="28.5714286"
                              y1="38.5541475"
                              x2="38.5964912"
                              y2="38.5541475"
                              id="Line-4-Copy-12"
                            ></line>
                            <line
                              x1="30.5764411"
                              y1="41.733871"
                              x2="40.6015038"
                              y2="41.733871"
                              id="Line-4-Copy-13"
                            ></line>
                            <line
                              x1="32.5814536"
                              y1="44.9135945"
                              x2="42.6065163"
                              y2="44.9135945"
                              id="Line-4-Copy-14"
                            ></line>
                            <line
                              x1="34.5864662"
                              y1="48.093318"
                              x2="44.6115288"
                              y2="48.093318"
                              id="Line-4-Copy-15"
                            ></line>
                            <line
                              x1="37.593985"
                              y1="51.2730415"
                              x2="47.6190476"
                              y2="51.2730415"
                              id="Line-4-Copy-16"
                            ></line>
                            <line
                              x1="40.6015038"
                              y1="55.2476959"
                              x2="50.6265664"
                              y2="55.2476959"
                              id="Line-4-Copy-17"
                            ></line>
                            <line
                              x1="43.6090226"
                              y1="59.2223502"
                              x2="53.6340852"
                              y2="59.2223502"
                              id="Line-4-Copy-18"
                            ></line>
                            <line
                              x1="46.6165414"
                              y1="63.1970046"
                              x2="56.641604"
                              y2="63.1970046"
                              id="Line-4-Copy-19"
                            ></line>
                            <line
                              x1="21.5538847"
                              y1="29.014977"
                              x2="31.5789474"
                              y2="29.014977"
                              id="Line-4-Copy-10"
                            ></line>
                          </g>
                        </g>
                        <g
                          id="Group-7"
                          transform="translate(520.063304, 328.761337) scale(-1, 1) translate(-520.063304, -328.761337) translate(461.683823, 249.104377)"
                        >
                          <g
                            id="Group-5"
                            transform="translate(78.322169, 84.159031) scale(-1, -1) rotate(10.000000) translate(-78.322169, -84.159031) translate(66.064544, 44.467673)"
                          >
                            <path
                              d="M24.4720575,1.16739289 C24.4720575,1.16739289 25.6418133,31.8941809 15.8064358,52.7916361 C5.97253152,73.6875917 0,78.2153237 0,78.2153237 C0,78.2153237 11.0242855,4.05440318 24.4720575,1.16739289"
                              id="Fill-254"
                              fill="#70C059"
                            ></path>
                            <line
                              x1="0.350004978"
                              y1="79.1401544"
                              x2="24.5152507"
                              y2="-2.09560991e-13"
                              id="Stroke-255"
                              stroke="#FFFFFF"
                              strokeWidth="0.5"
                            ></line>
                          </g>
                          <g
                            id="Group-5"
                            transform="translate(82.494283, 100.561943) rotate(20.000000) translate(-82.494283, -100.561943) translate(73.155139, 70.244929)"
                          >
                            <path
                              d="M18.6453771,1.09699048 C18.6453771,1.09699048 19.5366196,24.8404176 12.0429987,40.9884512 C4.5505002,57.1353259 -8.38243965e-13,60.6340279 -8.38243965e-13,60.6340279 C-8.38243965e-13,60.6340279 8.3994556,3.32786207 18.6453771,1.09699048"
                              id="Fill-254"
                              fill="#70C059"
                            ></path>
                            <line
                              x1="2.51520925"
                              y1="58.1954893"
                              x2="18.2790144"
                              y2="4.19121982e-13"
                              id="Stroke-255"
                              stroke="#FFFFFF"
                              strokeWidth="0.5"
                            ></line>
                          </g>
                          <g
                            id="Group-6"
                            transform="translate(38.529453, 78.313199) rotate(-4.000000) translate(-38.529453, -78.313199) translate(25.104435, 30.450090)"
                          >
                            <path
                              d="M2.32554784,0 C2.32554784,0 22.3101684,28.7926373 25.4477484,57.4998207 C28.5834913,86.207004 25.4808142,95.726217 25.4808142,95.726217 C25.4808142,95.726217 -9.13359945,14.3455804 2.32554784,0"
                              id="Fill-258"
                              fill="#70C059"
                            ></path>
                            <path
                              d="M2.33478578,1.04780496e-13 C2.33478578,1.04780496e-13 6.20454651,16.1220835 10.1577485,31.2615758 C14.1109505,46.4010682 25.6826436,95.726217 25.6826436,95.726217"
                              id="Stroke-259"
                              stroke="#FFFFFF"
                              strokeWidth="0.5"
                            ></path>
                          </g>
                          <path
                            d="M50.704792,75.4263268 C50.704792,75.4263268 51.9286767,83.0613724 47.4030113,87.8314852 C47.3131862,88.023571 47.2037903,88.254868 47.0892645,88.4961487 L46.9154333,88.861904 C46.6543306,89.410808 46.4038107,89.9362715 46.3283636,90.1053726 C46.2158694,90.3568079 46.100902,90.6070146 45.9835018,90.8559734 C46.4171802,90.5810338 47.0457565,90.1394338 47.8059278,89.4830232 C49.2981907,88.1929062 51.0231025,85.7056623 56.1997848,85.2818226 C53.6980786,88.3710559 51.8690101,89.5564366 49.7751697,90.3072103 C48.1707886,90.8807652 46.4856732,91.319014 45.620947,91.6025268 C44.8458487,93.1678783 43.9723818,94.6827789 43.0126478,96.1415421 C43.728751,96.0366927 45.0342548,95.7914255 46.3788845,95.2602614 C48.3899837,94.4664185 51.9887416,93.7538197 54.8730044,94.6847008 C52.0364395,97.050568 48.7511249,97.2502522 47.8020341,97.2150138 C47.2239631,97.1927094 46.467527,97.1201221 45.6886053,97.0426275 L44.9079619,96.9651776 C44.001938,96.8768322 43.136032,96.8023942 42.5577751,96.813918 C42.2257531,97.3045531 41.881263,97.7871826 41.527424,98.2630626 C40.6341422,99.4641455 39.6797445,100.619026 38.6693503,101.72182 C39.7689975,101.863833 41.0613265,101.473372 43.2415308,101.453313 C44.716272,101.441566 47.9655697,102.247156 50.1499389,103.198592 C46.2484453,105.161176 42.1289042,103.956218 40.1255924,103.069385 C39.2366747,102.67562 38.396282,102.550016 37.925841,102.510152 C37.0328147,103.44102 36.097256,104.330633 35.1242242,105.176641 C33.5196606,106.571861 31.8170421,107.830769 30.0403741,108.980489 C30.4894582,108.960658 31.1694532,108.912806 32.0556918,108.794353 C33.7776833,108.563345 37.8339517,106.592931 41.1630704,109.096816 C39.7749642,109.934707 36.371865,110.708973 34.0560833,110.593469 C32.4944731,110.515252 30.0624041,109.921717 28.9891783,109.642503 C28.2988234,110.066887 27.5973696,110.476082 26.8870889,110.871559 C26.0881987,111.316254 25.281629,111.746224 24.4702741,112.167037 C25.4666239,111.992197 26.7045929,111.810859 27.49733,111.937129 C28.9117187,112.161284 31.4903256,112.417741 33.9199983,114.673978 C30.21319,116.163779 26.2893076,113.598228 24.6802336,113.288913 C23.6788191,113.0958 23.0435032,113.06306 22.7064727,113.065873 C21.5383978,113.653218 20.3646883,114.229921 19.193894,114.81236 L18.460946,115.180327 L18.44733,115.357998 C18.4255061,115.588202 18.3919028,115.786432 18.3465625,115.943637 C18.1993983,116.453889 18.0269584,117.067 17.8648327,117.648942 C21.8194669,118.620825 24.1437782,121.338734 24.1437782,121.338734 C20.5509494,121.898292 18.3008093,120.852991 16.8758811,119.906703 L16.2536954,120.197696 C16.2536954,120.197696 16.3875956,120.000935 16.5161498,119.659475 C16.0007532,119.29087 15.6111605,118.96343 15.3138261,118.786887 C14.68211,118.411242 13.7355045,118.188138 13.0419877,118.06607 C11.4262676,119.009006 9.85399574,120.024713 8.36278191,121.155299 C8.19437912,121.282549 7.93058053,121.199347 7.82545046,121.031965 C7.70669243,120.839132 7.78359312,120.622808 7.94907563,120.491642 C8.29950919,120.218545 8.65286302,119.949363 9.01108398,119.685075 C11.3121069,117.990134 13.8045952,116.574167 16.3399324,115.257734 C16.0657093,112.789709 15.9433524,109.770805 16.5340423,108.195104 C17.7417194,109.137236 18.3817224,112.097161 18.4676265,114.177441 C19.4751631,113.671165 20.48682,113.17317 21.4961538,112.67051 C21.530712,112.304195 21.5526141,111.730593 21.5024827,110.908363 C21.4031931,109.265862 19.3745723,105.024529 21.3204519,101.51635 C23.2342085,104.227749 23.1563343,106.832455 23.1981917,108.272335 C23.229632,109.405858 22.550058,111.336966 22.266852,112.285518 C24.0052556,111.412851 25.7315089,110.518238 27.4146861,109.543267 C27.7502188,109.349101 28.0835023,109.151383 28.4143753,108.949994 C28.3668062,108.273238 28.2674166,106.999398 28.086253,105.271002 C27.7056433,101.636552 28.6527873,97.9786104 29.8569159,96.111954 C31.1486529,100.569612 31.0766193,103.466013 30.2365522,105.607724 C29.7421935,106.869703 29.4253046,107.829171 29.2407097,108.435083 C31.1462845,107.225024 32.9651464,105.886001 34.6654851,104.395047 L35.211946,103.904327 L35.2220773,103.798397 C35.258121,103.3133 35.2296756,102.562406 34.8050377,101.58301 C34.0545258,99.8475182 31.4739721,96.9413289 31.898386,92.4053635 C34.6648642,95.090334 35.7911187,97.840887 36.0763327,99.2553173 C36.2873219,100.298094 35.9126179,102.349592 35.7233708,103.43218 C37.3246598,101.927513 38.8146176,100.299301 40.1724913,98.5687224 C40.1320907,98.0942968 39.9303893,97.2697897 39.1808198,96.0905841 C37.9195218,94.1095187 35.8193796,90.7786002 36.1539066,86.6483382 C38.8412396,89.2279506 39.8559377,92.6424542 40.0641327,94.6043046 C40.2049295,95.9239958 40.5091479,97.1984304 40.6930482,97.8907867 C40.9642623,97.5316176 41.2292824,97.1681364 41.4884869,96.8006689 C42.2920591,95.6627923 43.1966221,94.1635805 44.0624476,92.5963422 C43.8871456,92.2099488 43.5136949,91.535844 42.7269775,90.3712267 C41.0828601,87.9387981 39.7044881,86.5253467 39.8436881,80.4398704 C42.7591006,84.3063062 43.4268712,86.5958235 44.0926949,89.7134432 C44.2362359,90.7613508 44.3433235,91.4847913 44.4151137,91.9495584 C45.2779841,90.3508334 46.0811326,88.7239429 46.6804709,87.3720445 L46.7097234,87.3045064 C46.6163796,86.2748914 46.5932486,84.5267793 47.0602831,82.2185288 C47.8028888,78.5583904 50.4303517,75.7157317 50.6849225,75.4471085 L50.704792,75.4263268 Z M16.4549331,116.199368 L15.9923219,116.439545 C15.439419,116.729925 14.8888349,117.025744 14.3419765,117.328547 C15.1492322,117.280574 15.9147533,117.316196 16.635806,117.414192 C16.5750193,117.050283 16.5135264,116.639816 16.4549331,116.199368 Z"
                            id="Combined-Shape"
                            fill="#71BF59"
                            transform="translate(31.983865, 98.460982) scale(-1, 1) rotate(-10.000000) translate(-31.983865, -98.460982) "
                          ></path>
                          <path
                            d="M70.1472663,64.5936528 C72.083201,64.8435292 74.3579012,64.1563436 78.1951977,64.1210375 C80.7910338,64.100362 86.5104405,65.518358 90.3553622,67.1930751 C83.487962,70.6476097 76.2367549,68.526646 72.71053,66.9656444 C71.1462477,66.2727127 69.6673364,66.0515619 68.8391496,65.9813357 C67.267039,67.6194064 65.6200741,69.185506 63.9071328,70.6748324 C62.3864973,71.9970743 60.81583,73.2497639 59.2017259,74.4403531 L59.3636413,74.4399199 C60.0096582,74.4349566 61.4848939,74.3873627 63.6770005,74.0943693 C66.7080461,73.6877508 73.8478807,70.2194325 79.7077881,74.6267639 C77.2644466,76.1016176 71.274319,77.4644789 67.1980852,77.2611696 C64.1737601,77.1096888 59.2946294,75.8601494 57.7872233,75.4576165 C55.0973484,77.3565644 52.2941423,79.0921194 49.4081456,80.6990132 C43.9697403,83.7262537 38.3291511,86.3658283 32.7810867,89.1845907 L33.1216264,89.014875 C33.1114281,89.0522104 33.1060819,89.0718084 33.1060819,89.0718084 L31.8574946,89.6558257 C26.635585,92.3353375 21.4909591,95.2452004 16.8017018,98.80043 C16.5052796,99.0244147 16.0409419,98.8779632 15.8558922,98.583337 C15.6468546,98.2439139 15.782215,97.8631398 16.0734969,97.6322631 C16.6903293,97.1515573 17.3123019,96.6777434 17.9428417,96.2125442 C19.7015074,94.9171056 21.523714,93.7142489 23.3880459,92.578117 C23.5557681,91.8766593 23.6415706,90.995137 23.4653749,90.0011703 C22.7131821,85.7351216 21.7433846,77.0117756 23.1843735,73.167852 C26.073205,75.4214836 27.1166798,84.2344235 26.3747675,86.8068027 C26.1742507,87.5020403 25.947075,88.3057652 25.7222977,89.1085513 L25.5879516,89.5892349 C25.3884346,90.3043355 25.1955097,91.0011095 25.0300838,91.6008233 C27.3305297,90.2598639 29.6843717,89.0075552 32.0527333,87.7963521 C32.15891,87.1752468 32.1884422,86.4427536 32.045473,85.6362263 C31.2932802,81.3701776 30.3234826,72.6468316 31.7644716,68.8029081 C34.6533031,71.0565396 35.6967778,79.8694795 34.9548656,82.4418587 C34.7543488,83.1370963 34.5271731,83.9408212 34.3023958,84.7436073 L34.1680497,85.2242909 C33.9974173,85.8358645 33.8316064,86.4340337 33.683695,86.9695487 L33.8314151,86.894777 C36.4114934,85.5959502 39.0090955,84.3278674 41.5908275,83.0291399 C41.6752814,82.560979 41.8230527,81.3772477 41.6922589,79.2320392 C41.5174898,76.3409125 37.9467158,68.8753272 41.3718488,62.7002389 C44.7404388,67.4728378 44.603365,72.0576341 44.6770422,74.5921081 C44.742869,76.9653767 43.0379759,81.3249626 42.842743,82.3975811 C45.3723852,81.1091809 47.8806585,79.7836751 50.336821,78.3609566 C50.9271706,78.0193343 51.5135643,77.6714644 52.0957191,77.3171379 C52.0129384,76.1279882 51.8379755,73.8849632 51.5189116,70.8409258 C50.8489632,64.4435756 52.516124,58.0048745 54.6356285,54.7191899 C56.9093411,62.5655493 56.7825478,67.6637877 55.3038635,71.433624 C54.4333397,73.6558611 53.8754527,75.3451848 53.5505865,76.411638 C57.0336168,74.1989694 60.3524656,71.7420839 63.4440085,68.9951259 L64.063165,68.4357264 C64.1463994,67.5939258 64.1496591,66.2045223 63.3453014,64.3493297 C62.0242521,61.2945218 57.4819671,56.1790539 58.2290196,48.1948576 C63.0985684,52.9209367 65.0809991,57.762454 65.5830321,60.2521311 C65.9546555,62.0888139 65.294006,65.7033495 64.9611018,67.6079389 C67.9566999,64.7909338 70.7322055,61.7285474 73.2424932,58.4651687 C72.9891009,57.6923855 72.5526055,56.7254839 71.813422,55.5626173 C69.593288,52.0755504 65.8966225,46.2124752 66.4854561,38.9423972 C71.2156936,43.4830251 73.0017629,49.4932268 73.3682274,52.9464716 C73.5377046,54.5349858 73.8417016,56.0862474 74.1205002,57.2980777 C74.4561989,56.8467624 74.7854286,56.3908834 75.1094938,55.9314645 C76.5239233,53.9285998 78.1161125,51.2897286 79.6401205,48.5311141 C79.3322046,47.8519671 78.6748895,46.6652628 77.2894822,44.6143747 C74.3955104,40.3328193 71.9693031,37.8448652 72.2143226,27.1332235 C77.3460251,33.9389151 78.5214335,37.9689183 79.6934149,43.4565456 C79.9462683,45.3024785 80.1348585,46.5764127 80.261225,47.3944237 C81.8046315,44.5326199 83.2406335,41.6192673 84.3012308,39.2158066 C84.1356238,37.4058752 84.0946354,34.3282199 84.9169569,30.2640139 C86.2911224,23.4910585 91.3320135,18.3083951 91.3320135,18.3083951 C91.3320135,18.3083951 93.4868603,31.7511276 85.5170767,40.1472464 C85.3551701,40.4943418 85.1575467,40.9127394 84.9512493,41.3477752 L84.7430621,41.7863325 C84.2552197,42.8131254 83.7692295,43.8312991 83.628634,44.1464188 C83.4303468,44.5896097 83.2276942,45.030632 83.0207476,45.4694514 C83.7831589,44.9849479 84.890293,44.2073182 86.2294391,43.0509608 C88.8561169,40.7800997 91.8923029,36.4020586 101.004288,35.6560169 C96.6007898,41.0936784 93.3812676,43.1801828 89.6956943,44.5016931 C86.8727153,45.5108881 83.9077335,46.2820946 82.3851377,46.78115 C81.0186541,49.5377948 79.4816178,52.2035277 77.7928213,54.7705064 C79.056452,54.5860292 81.3526264,54.1542539 83.7175607,53.2200425 C87.2574931,51.822722 93.5920187,50.5684072 98.6688917,52.2069421 C93.6759764,56.3713362 87.8931731,56.72282 86.2225854,56.6607934 C85.099022,56.6174416 83.5926502,56.4661725 82.0728862,56.3145203 L81.3135984,56.2391602 C79.6545974,56.0761133 78.0534461,55.9341272 76.9962239,55.9546954 C76.4081043,56.8171727 75.8013074,57.6673184 75.1780307,58.5055666 L74.6168384,59.2498697 C74.6645268,59.4195109 74.7005239,59.5422921 74.7208858,59.6107515 L74.7376342,59.6665865 C74.7390677,59.6713108 74.7398006,59.6737035 74.7398006,59.6737035 L73.5247753,60.7746156 C73.5247753,60.7746156 73.5410514,60.7190458 73.5549439,60.6076911 L73.0978715,61.1781185 C72.1476417,62.3452784 71.1635462,63.4844339 70.1472663,64.5936528 Z M66.722477,82.8211011 C64.2791355,84.2959548 58.2890079,85.6588161 54.2127741,85.4555068 C51.927697,85.3410531 48.5837485,84.5997397 46.435327,84.0705461 L45.8472241,83.9236293 C44.9298453,83.6912354 44.3434564,83.5275147 44.3434564,83.5275147 L46.0020501,82.6298526 C46.0020501,82.6298526 47.6623572,82.6936021 50.6916894,82.2887065 C53.7227351,81.882088 60.8625696,78.4137697 66.722477,82.8211011 Z"
                            id="Combined-Shape-Copy"
                            fill="#71BF59"
                            transform="translate(58.379480, 58.608785) rotate(-49.000000) translate(-58.379480, -58.608785) "
                          ></path>
                          <polygon
                            id="Fill-260"
                            fill="#F17574"
                            points="85.3552263 116.935625 75.7618204 159.313919 37.4517028 159.219821 27.2907102 116.917606"
                          ></polygon>
                          <line
                            x1="30.0556871"
                            y1="127.977514"
                            x2="83.5119083"
                            y2="127.977514"
                            id="Stroke-261"
                            stroke="#FFFFFF"
                          ></line>
                          <line
                            x1="23.6040742"
                            y1="158.85309"
                            x2="88.1202033"
                            y2="158.85309"
                            id="Stroke-261"
                            stroke="#1A3E6F"
                            strokeLinecap="round"
                          ></line>
                          <line
                            x1="28.2123692"
                            y1="122.44756"
                            x2="85.3552263"
                            y2="122.44756"
                            id="Stroke-262"
                            stroke="#FFFFFF"
                          ></line>
                        </g>
                        <g
                          id="Group-11"
                          transform="translate(0.000000, 191.828435)"
                        >
                          <path
                            d="M83.8709677,115.410813 C72.6165124,119.346322 57.9333628,115.829484 48.3053859,108.78974 C38.677409,101.749995 31.7497257,91.6630523 25.6086181,81.4316982 C22.9883152,77.0653819 20.4298234,72.5133941 19.6008284,67.4869122 C18.7730454,62.4604304 19.9935102,56.8065484 23.8851811,53.5263503 C29.9353902,48.4294831 39.2046207,51.0155364 46.0753409,54.9328429 C67.5770955,67.1956627 82.259033,90.6873663 83.8709677,115.410813"
                            id="Fill-284"
                            fill="#26A383"
                          ></path>
                          <path
                            d="M71.8894009,142.289347 C65.1255268,151.213587 53.5043252,155.18571 42.3185244,155.703602 C31.1339365,156.221495 20.0803327,153.129909 9.62585991,149.118974 C5.83338469,147.662326 1.68312879,145.667168 0.381777364,141.818757 C-0.603029121,138.905462 0.409672129,135.584645 2.36230568,133.207436 C4.31493922,130.831439 7.08137222,129.268059 9.91087164,128.062474 C20.2016143,123.680403 31.9913973,123.474217 42.8182046,126.28078 C53.6450118,129.089769 63.1037628,135.367543 71.8894009,142.289347"
                            id="Fill-285"
                            fill="#26A383"
                          ></path>
                          <path
                            d="M84.6592868,67.2780021 C75.182625,67.463585 68.0110305,59.2967203 63.7201538,50.9794484 C57.7787504,39.4635991 55.7658226,26.0264199 58.0770532,13.3145912 C58.9436106,8.54886843 60.7741052,3.41197965 65.1068922,1.11827087 C69.5111732,-1.21061377 75.2405598,0.323781576 78.9335981,3.64001627 C82.6266365,6.95625096 84.6469602,11.6904368 86.1224498,16.3906597 C91.2083898,32.5848987 90.1125583,51.1989943 84.6592868,67.2780021"
                            id="Fill-286"
                            fill="#26A383"
                          ></path>
                          <path
                            d="M114.938256,118.894009 C99.1524898,91.9442511 96.5221309,58.9695482 105.845212,29.1432324 C107.357909,24.3058794 110.212282,18.7690192 115.256212,18.4476566 C120.843316,18.0924663 124.345643,24.3409152 125.88845,29.7388405 C131.300919,48.6774879 130.149535,68.7952703 128.953589,88.461212 C128.215306,100.573924 124.067432,110.931224 114.938256,118.894009"
                            id="Fill-287"
                            fill="#26A383"
                          ></path>
                          <path
                            d="M130.875576,130.246746 C136.650813,111.568035 151.349203,95.9007575 169.560417,89.0097963 C173.794536,87.4081002 179.178108,86.493544 182.335756,89.7507336 C185.432587,92.9455671 184.478967,98.2825163 182.491449,102.270862 C179.281498,108.713104 174.070648,113.887438 168.87926,118.855141 C164.157384,123.371679 159.249406,127.906558 153.27712,130.53285 C147.304835,133.15792 136.335778,133.82672 130.875576,130.246746"
                            id="Fill-288"
                            fill="#26A383"
                          ></path>
                          <path
                            d="M32.3965709,60.1996576 C32.4042049,60.1887786 32.5128891,60.251635 32.716974,60.3833917 C32.9548717,60.5429503 33.2495267,60.739981 33.6118075,60.984154 C34.4390152,61.5607408 35.5753692,62.3524899 37.0160391,63.355775 C37.7562994,63.8791755 38.5750539,64.4569711 39.4662646,65.086744 C40.3453993,65.7406925 41.2631772,66.5010134 42.2703177,67.2915537 C44.2580315,68.9040625 46.6056109,70.643493 48.9640587,72.8446764 C50.169246,73.9180706 51.4287755,75.0434422 52.7414395,76.2147473 C54.0468578,77.3933051 55.3039721,78.7277952 56.6613173,80.0526151 C59.4146511,82.6647827 62.0810376,85.7205716 64.9442632,88.8803153 C67.2409488,91.5664794 69.5612797,94.4260046 71.8623616,97.4487376 C72.2325487,97.6345543 72.4230728,97.748573 72.4091212,97.7802095 C72.3978325,97.8066366 72.2818506,97.7847768 72.0698828,97.722531 C75.2499619,101.914947 78.3912062,106.420159 81.3797339,111.210057 C86.5180828,119.619521 90.6758584,128.054368 93.9883121,135.915047 C95.5340434,139.893133 97.0725291,143.647595 98.1762778,147.281179 C98.7523042,149.088301 99.3826727,150.810809 99.8524301,152.506723 C100.312527,154.205055 100.754509,155.836904 101.175963,157.395018 C102.086495,160.490697 102.593688,163.370004 103.144355,165.872173 C103.401574,167.125675 103.670869,168.288519 103.861671,169.366748 C104.024697,170.447395 104.175647,171.437384 104.312106,172.334296 C104.315252,172.355961 104.31839,172.377568 104.321519,172.399119 C104.408103,172.551517 104.488005,172.690919 104.562874,172.816819 C104.91836,173.445436 105.093685,173.780296 105.071921,173.794004 C105.054691,173.803669 104.927278,173.632007 104.697334,173.291594 L104.528932,173.03979 L104.528932,173.03979 L104.433497,172.895396 C104.416969,172.871414 104.400246,172.846903 104.383319,172.821871 C104.602197,174.33061 104.778767,175.539628 104.90987,176.44293 C104.965419,176.875672 105.010101,177.227427 105.046329,177.511489 C105.072896,177.754454 105.080142,177.877749 105.068066,177.880175 C105.054782,177.881375 105.022177,177.761706 104.971457,177.522368 C104.9147,177.240723 104.845867,176.892595 104.761334,176.463479 C104.582609,175.472282 104.336258,174.108781 104.024697,172.382647 C104.015958,172.332214 104.007165,172.281487 103.998319,172.230467 C103.65838,171.692752 103.240946,171.024157 102.702013,170.264877 C101.184547,167.993394 98.8424497,164.797119 95.6116371,161.139052 C89.2165144,153.826545 78.6897724,144.860293 64.8935255,139.124164 C56.9425251,135.815206 48.9951429,134.032817 41.8319029,133.235589 C40.6328087,133.996757 36.6603346,136.182746 33.0685923,140.206083 C28.6647065,145.019821 26.911428,150.208892 26.6915293,150.092283 C26.6135007,150.064346 26.939802,148.743999 27.8477709,146.759228 C28.7486464,144.775672 30.3115825,142.160487 32.5342148,139.695921 C34.7627583,137.236214 37.185191,135.442147 39.0436899,134.370808 C40.0233445,133.804242 40.8355635,133.416435 41.3897853,133.187539 C36.3007371,132.647426 31.6199888,132.603371 27.6303582,132.858546 C22.7563291,133.143841 18.8375179,133.734983 16.1556532,134.260845 C14.806257,134.484488 13.7797001,134.758903 13.0687279,134.885835 C12.3625922,135.028483 11.9877599,135.092554 11.9816396,135.067167 C11.9756685,135.041781 12.3372003,134.929355 13.0336629,134.739561 C13.7325438,134.565482 14.753055,134.24513 16.0976147,133.97555 C18.7710154,133.361439 22.6970815,132.685676 27.5965024,132.330266 C37.3421422,131.57109 51.2605119,132.642157 65.1812999,138.435103 C79.1081336,144.225631 89.6771954,153.344203 96.0082339,160.790895 C99.2100272,164.516659 101.497713,167.777004 102.946259,170.105305 C103.324179,170.67629 103.635787,171.198455 103.89899,171.655399 C103.77856,170.964738 103.649541,170.223209 103.512673,169.433231 C103.300135,168.359837 103.012726,167.204246 102.736185,165.957996 C102.148083,163.471542 101.608284,160.609157 100.666354,157.530401 C100.23041,155.981958 99.7751436,154.360987 99.3005558,152.672326 C98.8175147,150.986081 98.1762778,149.273244 97.5893829,147.47821 C96.465105,143.866383 94.9097129,140.134888 93.3543208,136.179769 C90.0189226,128.365024 85.861147,119.97611 80.7457426,111.60291 C77.5685905,106.512008 74.2260541,101.740436 70.8521935,97.3189054 C70.0498152,97.0425878 68.9584517,96.6627356 67.6332235,96.2286216 C64.6949555,95.2641211 60.5796832,94.0923427 55.9407593,93.2165043 C51.3030475,92.3514491 47.0411043,91.9512713 43.9488924,91.7823339 C40.8554683,91.6121984 38.9329876,91.6110003 38.9293489,91.4947809 C38.928139,91.4444592 39.4069409,91.3881467 40.2772695,91.3318342 C41.1463859,91.268333 42.4082411,91.2407758 43.9670747,91.2587479 C47.0835298,91.2934939 51.3975957,91.6145947 56.0813695,92.4892349 C60.7639311,93.3722622 64.8973857,94.6374952 67.8089862,95.7361872 C68.8487004,96.1257352 69.7386723,96.4830011 70.4485858,96.7919164 C68.4396221,94.1774732 66.4221913,91.6883511 64.4237865,89.3299804 C61.5859205,86.1702367 58.947309,83.1108215 56.2229576,80.4889837 C54.8801036,79.1605375 53.6374805,77.8212123 52.3465534,76.6329843 C51.0495883,75.4544264 49.8045501,74.3205934 48.6150615,73.2399465 C46.2843886,71.0206315 43.9694145,69.2570255 42.0094756,67.6179236 C41.0192415,66.8128779 40.1159548,66.0392605 39.2501038,65.3695979 C38.3745919,64.7229021 37.5715362,64.1293924 36.8433519,63.5902778 C35.4401177,62.5398503 34.3315386,61.7118379 33.5260677,61.1086581 C33.1806934,60.8391008 32.9005296,60.6215209 32.6735003,60.4462482 C32.4839067,60.2939422 32.3897137,60.2105366 32.3965709,60.1996576 Z M169.575331,97.7915035 L169.584909,97.7932086 C169.596588,97.816275 169.311128,98.0190168 168.755865,98.3832235 C168.134357,98.7814229 167.321338,99.2998105 166.30958,99.9468845 C165.231576,100.597601 163.948812,101.451058 162.480559,102.476907 C160.997852,103.477262 159.312794,104.620871 157.522947,105.991503 C156.618387,106.664071 155.671671,107.368204 154.686412,108.10026 C153.726447,108.871164 152.730347,109.672419 151.698114,110.500382 C151.368875,110.753981 151.040169,111.015254 150.711175,111.282996 C151.829138,110.903268 154.138701,110.347768 156.86551,110.144276 C160.754275,109.834895 163.8933,110.366723 163.868583,110.568716 C163.850928,110.798834 160.735444,110.628802 156.917298,110.931791 L156.609326,110.955673 C153.527507,111.203473 150.990228,111.669168 150.224027,111.68385 C148.616065,113.020084 146.990111,114.497854 145.251772,115.975624 C140.851831,119.994039 136.182088,124.598826 131.606293,129.743853 C127.078678,134.925302 123.101507,140.146813 119.715732,145.059962 C118.133054,147.587557 116.494971,149.907554 115.22666,152.243333 C114.5642,153.391798 113.92342,154.501415 113.30673,155.570969 C112.747854,156.667231 112.209454,157.719789 111.695144,158.727427 C111.195288,159.739922 110.666524,160.678362 110.258207,161.610731 C109.848687,162.539458 109.463255,163.417196 109.099504,164.242732 C108.738162,165.059769 108.399705,165.823389 108.086542,166.531164 C107.81674,167.243795 107.569823,167.900582 107.344586,168.501523 C106.914589,169.626922 106.568906,170.531368 106.305126,171.224575 C106.059413,171.844941 105.91849,172.167871 105.8944,172.159536 C105.869107,172.150874 105.961851,171.813376 106.162998,171.177228 C106.394257,170.470667 106.69658,169.54801 107.07358,168.399545 C107.280749,167.79132 107.509599,167.123607 107.756516,166.398836 C108.054021,165.680134 108.374411,164.906802 108.716482,164.077625 C109.065779,163.242377 109.436757,162.351285 109.82821,161.411631 C110.222073,160.465908 110.736383,159.515328 111.224195,158.489479 C111.727665,157.470914 112.25402,156.405002 112.802055,155.295386 C113.40911,154.214906 114.040254,153.090721 114.693078,151.930115 C115.944526,149.568841 117.57177,147.224564 119.148425,144.672689 C122.518542,139.713406 126.500531,134.451833 131.049827,129.24489 C135.647302,124.076796 140.354383,119.464725 144.797686,115.45845 C147.08739,113.535439 149.1904,111.616069 151.313887,110.014773 C152.358165,109.194094 153.367514,108.402551 154.339524,107.638931 C155.339237,106.917802 156.297997,106.224595 157.215806,105.561739 C159.030947,104.214174 160.742504,103.097273 162.251709,102.126055 C163.746461,101.131771 165.054519,100.312305 166.159021,99.6967959 C167.202095,99.0994968 168.039204,98.6199579 168.681188,98.2521091 C169.26054,97.9303931 169.571294,97.7713562 169.584909,97.7932086 Z M69.9632934,11.9816345 C69.97655,11.9780774 70.0223456,12.1147119 70.099475,12.3819352 C70.1826302,12.692688 70.2886832,13.0856633 70.4224546,13.5777893 C70.7044592,14.6829569 71.0973373,16.2258385 71.5974736,18.1870877 C72.6182337,22.2462206 74.074052,28.0283992 75.8673118,35.1503214 C79.476729,49.4751791 84.6154787,69.2243056 89.8867946,91.1450742 C92.5043753,102.112109 94.8568235,112.595482 96.8633944,122.158687 C98.8314005,131.730357 100.489684,140.374606 101.696037,147.665809 C102.344406,151.302947 102.821644,154.612404 103.268754,157.514376 C103.72671,160.41272 104.067767,162.918089 104.322053,164.970025 C104.572724,166.978431 104.770368,168.557587 104.911371,169.690565 C104.968012,170.197201 105.011398,170.602268 105.047552,170.921485 C105.07286,171.198381 105.080091,171.342302 105.06804,171.342302 C105.054783,171.344689 105.022244,171.205636 104.971628,170.929949 C104.917396,170.61436 104.848703,170.21292 104.763137,169.711121 C104.588391,168.582979 104.344951,167.009869 104.034023,165.009927 C103.737557,162.966455 103.35432,160.46955 102.857799,157.578461 C102.372124,154.686163 101.858731,151.38517 101.176618,147.756496 C99.9051871,140.483429 98.1962877,131.857319 96.1921272,122.301368 C94.1494019,112.752672 91.7788764,102.2826 89.1625008,91.3191925 C85.2351993,74.9881483 81.4250703,59.8454898 78.2985587,47.2466926 C77.4013115,46.0836926 75.6302645,43.5923681 73.3810644,40.8671837 L73.1722728,40.6133215 C70.1165818,36.9123228 67.3701777,34.2049916 67.5157935,34.0524499 C67.6396993,33.9106241 70.7754687,36.5284935 73.9517458,40.4062496 C75.751791,42.5879854 77.1585885,44.6756745 78.0133912,46.0975545 C77.0541484,42.2272277 76.1628246,38.6080618 75.3563292,35.2797009 C73.6124805,28.1009485 72.2241504,22.2861227 71.3142639,18.2560095 C70.8731798,16.2802505 70.5260973,14.7264865 70.2766318,13.6116457 C70.1729891,13.1134739 70.0898339,12.7144528 70.0247559,12.4000725 C69.9729345,12.1255943 69.9512419,11.9841232 69.9632934,11.9816345 Z M115.75919,38.1331287 C115.772066,38.1331287 115.793136,38.2467299 115.821229,38.4690797 C115.849322,38.7361412 115.884438,39.0624155 115.927748,39.4599867 C116.010856,40.3940977 116.124399,41.6689841 116.269546,43.2761868 C116.417034,44.9341433 116.578568,46.9642942 116.695622,49.3267613 C116.82087,51.6868117 117.010497,54.3743446 117.0819,57.3494823 C117.31952,63.2997577 117.386241,70.3919929 117.259823,78.2660779 C117.090094,86.1389544 116.702645,94.7900554 116.007345,103.850812 C114.573433,121.963866 112.240546,138.252201 110.272868,149.979947 C109.290785,155.844424 108.429267,160.579026 107.779617,163.84056 C107.461231,165.419969 107.208394,166.673104 107.024619,167.592714 C106.937999,167.983035 106.867767,168.302058 106.80924,168.564286 C106.757736,168.783011 106.724961,168.892977 106.712085,168.890599 C106.700379,168.888143 106.708573,168.773343 106.736666,168.549785 C106.776465,168.283932 106.823286,167.960075 106.880643,167.56492 C107.032813,166.63806 107.241169,165.377674 107.502199,163.786181 C108.0664,160.511354 108.851832,155.764669 109.769536,149.894149 C111.606113,138.15311 113.855892,121.875651 115.287462,103.790391 C115.695086,98.469064 116.003881,93.2899048 116.231004,88.3274401 L116.258393,87.7272524 C115.573962,86.7543142 113.556785,83.1309611 111.280119,78.7538404 L111.122121,78.4496694 C108.501825,73.3983999 106.541886,69.2801452 106.717905,69.1895485 C106.898634,69.0971399 109.233841,73.228279 111.932883,78.4173783 C114.003704,82.3987411 115.68711,85.8449704 116.278401,87.276929 C116.413744,84.1727286 116.51755,81.1583478 116.593785,78.2503684 C116.75649,70.3907845 116.742444,63.3106335 116.570374,57.3664003 C116.532917,54.3936794 116.378406,51.7097717 116.291786,49.3473045 C116.21336,46.9872542 116.093964,44.9571034 115.987445,43.2979385 C115.901996,41.685902 115.832934,40.4098072 115.782601,39.4732793 C115.768554,39.0720829 115.756849,38.7446002 115.747485,38.4751218 C115.742802,38.2515636 115.747485,38.1331287 115.75919,38.1331287 Z M124.640481,51.2096496 L124.656189,51.2109141 C124.84309,51.304396 123.357716,54.6722068 121.33746,58.7325222 L121.196821,59.0145657 C119.233627,62.9430564 117.522511,66.0084113 117.338755,65.9171033 C117.150624,65.8223913 118.635998,62.4558105 120.657484,58.3930351 C122.67774,54.3339497 124.468058,51.1162021 124.656189,51.2109141 Z M77.2432132,17.7025899 C77.4619938,17.735434 77.1208003,20.5060718 76.4813882,23.8913642 L76.4247261,24.1883758 C75.8001796,27.4351877 75.1388295,29.9923701 74.9264837,29.9604919 C74.7064009,29.9264748 75.0475944,27.154664 75.6870065,23.7705446 C76.3277209,20.3852523 77.0231304,17.6685727 77.2432132,17.7025899 Z"
                            id="Combined-Shape"
                            fill="#0B264B"
                          ></path>
                          <path
                            d="M132.718894,164.976959 L123.907104,211.3088 C123.324965,214.37312 120.644954,216.589862 117.524112,216.589862 L91.4897161,216.589862 C88.3121092,216.589862 85.6006962,214.292225 85.0801533,211.157878 L77.4193548,164.976959 L132.718894,164.976959"
                            id="Fill-301"
                            fill="#1A3E6F"
                          ></path>
                          <polyline
                            id="Fill-302"
                            fill="#45C1C5"
                            points="129.855304 179.723502 79.8590404 179.723502 77.4193548 164.976959 132.718894 164.976959 129.917603 179.723502"
                          ></polyline>
                          <polygon
                            id="Fill-303"
                            fill="#FBCF48"
                            points="129.953917 179.723502 128.841627 185.253456 80.2313179 185.253456 79.2626728 179.723502"
                          ></polygon>
                        </g>
                        <g
                          id="Group-8"
                          transform="translate(176.497696, 382.151015) scale(-1, 1) translate(-176.497696, -382.151015) translate(116.129032, 334.685578)"
                        >
                          <path
                            d="M119.815668,24.7695853 C120.047926,24.4654378 120.322581,24.1935484 120.629493,23.9631336"
                            id="Stroke-343"
                            stroke="#1E1A19"
                            strokeWidth="0.252"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          ></path>
                          <path
                            d="M4.21722953,47.1471184 C6.32698506,48.7929375 9.39833937,48.4602483 11.7720479,47.2444637 C14.1459122,46.0286792 16.066151,44.0790727 18.2118789,42.4821645 C20.1634183,41.0299244 22.8035715,39.8487586 24.9151957,41.0458046 C26.6820459,42.0475247 27.2903058,44.6969695 26.1429277,46.3942403 C24.5940918,48.6854288 21.2604974,48.5107472 18.8145327,49.7443175 C16.2765348,51.0240991 14.7558851,53.8250405 12.267719,55.2034379 C9.50251906,56.7342847 6.12672335,56.2277078 3.02640428,55.6798425 C2.02649727,55.5035728 0.736164062,54.9255352 0.943900545,53.9123814 L4.21722953,47.1471184"
                            id="Fill-72"
                            fill="#F17574"
                            transform="translate(13.824885, 48.387097) scale(-1, 1) translate(-13.824885, -48.387097) "
                          ></path>
                          <path
                            d="M16.1140262,83.0336119 C17.6417072,82.7671759 19.169227,82.5007399 20.6969081,82.235909 C22.105748,81.9903384 23.7119577,81.791314 24.8273229,82.682109 C26.1352204,83.7269874 26.0483067,85.8600803 25.0661339,87.2131257 C24.0837999,88.5661711 22.4767839,89.3076977 20.9128216,89.9111913 C16.2918848,91.6927813 11.3968232,92.7681554 6.4516129,93.0875576 L16.1140262,83.0336119"
                            id="Fill-73"
                            fill="#F17574"
                            transform="translate(16.129032, 87.557604) scale(-1, 1) translate(-16.129032, -87.557604) "
                          ></path>
                          <path
                            d="M53.3628084,14.5814061 C55.255905,16.0860763 54.0253294,21.5489099 53.4635518,23.4868001 C52.5104128,26.7748534 50.6270454,29.8054997 48.0048933,32.0052159 C47.4355834,32.4827837 46.8245326,32.9294186 46.1263904,33.1797235 L36.0718258,21.3192796 C38.0546813,18.6692987 40.8094317,16.6386094 43.873003,15.4303279 C45.9716662,14.602712 51.3118492,12.9514256 53.3628084,14.5814061 Z M21.5239544,1.82112321 C23.4169939,3.86762499 24.722434,6.45640296 25.1930236,9.20109127 C25.8380121,12.9619725 24.906557,16.9658391 22.6661547,20.0621584 L10.2629598,16.8438706 C9.58481323,11.2937476 11.7090028,5.49492936 15.7418545,1.61588618 C18.0673838,-0.620943595 19.358636,-0.519593937 21.5239544,1.82112321 Z"
                            id="Combined-Shape"
                            fill="#E53F3E"
                          ></path>
                          <path
                            d="M32.5655206,33.1797235 C30.9451015,35.4875498 29.3246824,37.795376 27.7042633,40.1030446 C22.0089995,48.2142754 16.1733332,56.6479614 14.3239867,66.368944 C12.4744816,76.0883491 15.7416551,87.5083739 24.5187929,92.1653588 C28.4280103,94.2398625 32.9707047,94.7778137 37.3998078,94.9040193 C42.6200829,95.0523108 48.0048748,94.63741 52.7022498,92.3688652 C57.3997835,90.1003204 61.3032896,85.6626177 61.7129158,80.485035 C62.1649008,74.7726562 58.5161815,69.6692192 55.7146365,64.6604363 C50.8879643,56.0342869 48.2290428,46.2177035 48.0450125,36.3466941 L32.5655206,33.1797235"
                            id="Fill-76"
                            fill="#F6B0AC"
                            transform="translate(37.788018, 64.055300) scale(-1, 1) translate(-37.788018, -64.055300) "
                          ></path>
                          <path
                            d="M28.5910959,48.8761669 C32.4144008,51.1321862 37.0219938,51.8666638 41.4628836,51.5385098 C44.1192661,51.3422174 47.0897155,50.5403088 48.3173734,48.2043829 C49.6508391,45.6669533 48.2718217,42.5313296 46.3832648,40.3650078 C41.7568118,35.057693 33.5616639,32.7610887 26.7862902,34.8909314 C18.2788336,37.565434 22.945404,45.5445667 28.5910959,48.8761669"
                            id="Fill-77"
                            fill="#FFD93B"
                            transform="translate(35.483871, 42.857143) scale(-1, 1) translate(-35.483871, -42.857143) "
                          ></path>
                          <path
                            d="M46.0379127,39.4973164 C44.258478,41.8886494 41.8972745,43.7017112 39.4290828,45.1895351 C31.7671958,49.8087075 22.2715863,51.460025 14.5373155,47.6716802 C6.80304479,43.8833354 1.72586209,33.8655331 4.41326879,24.8647949 C5.86491286,20.000382 8.39215456,16.2438453 11.5478335,13.4832203 C16.3696094,9.25910448 22.6582681,7.35825238 28.818509,7.37327189 C44.9170752,7.41264423 60.1438612,20.5331677 46.0379127,39.4973164"
                            id="Fill-78"
                            fill="#F6B0AC"
                            transform="translate(27.649770, 28.571429) scale(-1, 1) translate(-27.649770, -28.571429) "
                          ></path>
                          <g
                            id="Fill-93-+-Fill-94-+-Fill-95-+-Fill-96-Mask"
                            transform="translate(51.612903, 31.336406)"
                          >
                            <mask id="mask-18" fill="white">
                              <use xlinkHref="https://i.imgur.com/w7GCRPb.png"></use>
                            </mask>
                            <use
                              id="Mask"
                              fill="#F17574"
                              transform="translate(18.433180, 29.493088) scale(-1, 1) translate(-18.433180, -29.493088) "
                              xlinkHref="https://i.imgur.com/w7GCRPb.png"
                            ></use>
                            <path
                              d="M29.8470478,14.1252912 C29.5577676,14.1562125 29.269748,14.2213099 28.9984297,14.3080521 C27.8483994,14.676666 26.8378096,15.3518886 25.8421881,16.0674716 C25.8166634,15.3597003 25.7536394,14.6457448 25.6712355,13.9472499 C25.5018585,12.508435 25.0759739,10.7444587 24.6741959,9.22671328 C24.1972619,7.42693343 23.6965362,5.63350057 23.2407152,3.82932665 C23.1687103,3.54273543 23.098281,3.25467952 23.0292697,2.9666236 C24.5226232,1.59453355 26.1914986,0.535073666 28.0253392,-0.0270422215 C28.0267573,-0.00848946781 28.0297509,0.0085985948 28.0327446,0.0271513485 C28.1873109,0.9966141 28.380637,1.95842791 28.5845196,2.91714959 C28.9834615,4.78332877 29.4227387,6.6309552 29.6790888,8.52512625 C29.9340209,10.4067661 30.0435251,12.2374672 29.8470478,14.1252912"
                              id="Fill-93"
                              fill="#F6B0AC"
                              mask="url(#mask-18)"
                              transform="translate(26.488533, 8.020215) scale(-1, 1) translate(-26.488533, -8.020215) "
                            ></path>
                            <path
                              d="M23.0292697,7.17733366 C22.9972777,7.20657046 22.9651327,7.23580726 22.9331408,7.26649782 C21.7517348,8.37943455 20.5995655,9.53226902 19.4894909,10.7220937 C18.3547718,11.9395399 17.2768422,13.2138443 16.1929428,14.4789417 C15.5359599,15.2460442 14.9183163,16.0730741 14.2409748,16.833877 C14.3473595,15.8162426 14.2744975,14.7433651 13.5535307,14.1161469 C13.1921288,13.8010033 12.7391902,13.6750104 12.2671177,13.667257 C12.576016,13.0631375 12.9081812,12.4745248 13.2271822,11.8994806 C14.0458087,10.4300487 14.98827,9.04509671 15.968693,7.69083523 C17.3466428,5.78769727 18.8193438,3.98309216 20.3912352,2.27217404 C21.6453501,3.61109023 22.4815798,5.31118589 23.0292697,7.17733366"
                              id="Fill-94"
                              fill="#F6B0AC"
                              mask="url(#mask-18)"
                              transform="translate(17.648194, 9.553026) scale(-1, 1) translate(-17.648194, -9.553026) "
                            ></path>
                            <path
                              d="M23.0292697,19.1330933 C22.8948013,20.9396669 22.6853069,22.7412075 22.4666551,24.5421189 C21.6900479,25.0124006 20.8597817,25.4008941 20.0158599,25.713891 C18.1414955,26.4153814 16.0393221,26.6953484 14.0457516,26.7834279 C13.0932268,26.8258948 12.1407021,26.7661265 11.1944429,26.6544543 C11.0414991,26.637153 10.8855029,26.6182788 10.7296674,26.6009775 C11.1210235,24.9007284 11.5520614,23.2067706 11.9557879,21.5085662 C12.0581253,21.5219354 12.1590168,21.5368775 12.1697807,21.5398659 C12.3180654,21.5563808 12.4679568,21.5699073 12.6162415,21.5832765 C13.0916203,21.6252716 13.5687663,21.6521673 14.0457516,21.6716706 C15.6845954,21.7360001 16.9887944,21.6461904 18.4350126,21.3034668 C19.1062301,21.1433508 19.7682902,20.9637315 20.4179799,20.7301635 C20.4456126,20.7211983 20.4669797,20.7136486 20.4836879,20.7076718 C20.4836879,20.7076718 20.4852944,20.7076718 20.486901,20.7062562 C20.4883469,20.7062562 20.4897928,20.7046834 20.4913993,20.7046834 C20.6565529,20.6313887 20.8200999,20.5551055 20.9805944,20.4742611 C21.2635083,20.3290872 21.5401565,20.1719596 21.8062016,19.9997327 C22.3382916,19.6554362 22.6379136,19.4698401 23.0292697,19.1330933"
                              id="Fill-95"
                              fill="#F6B0AC"
                              mask="url(#mask-18)"
                              transform="translate(16.879469, 22.965120) scale(-1, 1) translate(-16.879469, -22.965120) "
                            ></path>
                            <path
                              d="M24.56672,39.3146078 C22.3714255,40.4659533 19.7397075,40.8165755 17.3556507,40.4577993 C15.6348815,40.1985019 14.0696943,39.2819917 12.7401028,38.1616314 C12.5951575,38.0393213 12.4500534,37.9121189 12.3049493,37.7767624 C12.2339849,36.4231975 12.2657363,35.058217 12.3760726,33.7128061 C13.0906387,34.2183545 13.7569426,34.8054428 14.5772411,35.1430187 C15.1694054,35.3843772 15.7918924,35.5344109 16.4129505,35.6681366 C16.4732782,35.6795522 16.5321772,35.689337 16.5910761,35.7007526 C16.6017128,35.7007526 16.6123495,35.7023834 16.6244151,35.7040142 C16.770948,35.7186914 16.9189097,35.7284762 17.0670301,35.7431535 C17.3677163,35.7725079 17.665386,35.8067547 17.9675009,35.8181703 C18.3164493,35.8312167 18.6639688,35.8165395 19.0129171,35.7937083 C19.0884855,35.7888159 19.1610375,35.7822927 19.1942178,35.7774003 L19.2048545,35.7774003 C19.2094585,35.7757695 19.2123161,35.7757695 19.2139037,35.7757695 C19.3590077,35.7496767 19.5009367,35.7186914 19.6444532,35.6860754 C19.9481557,35.6175818 20.2472542,35.5327801 20.5433364,35.4333013 C20.6914569,35.3843772 20.8379898,35.3321916 20.9845227,35.2751136 C21.0858097,35.2376051 21.2564737,35.1625883 21.2806048,35.1544343 L21.2821924,35.1544343 C21.8365725,34.8918753 22.3669803,34.5787615 22.8640491,34.2150929 C22.8805598,34.2036773 22.9742266,34.131922 23.0453498,34.0781056 C23.0513826,34.074844 23.0574153,34.0683208 23.0634481,34.0634284 C23.0664645,34.0601668 23.0709097,34.0569052 23.0739261,34.0552744 C23.1434617,33.9949347 23.2477652,33.9068715 23.2658635,33.8905635 C23.5317818,33.6573589 23.7977001,33.4225236 24.0545693,33.1795342 C24.1512524,33.0865785 24.2539683,33.0034077 24.358113,32.9283908 C24.3128672,34.0079811 24.2977853,35.0875714 24.3265203,36.1704233 C24.3550966,37.2288132 24.4336815,38.2806799 24.56672,39.3146078"
                              id="Fill-96"
                              fill="#F6B0AC"
                              mask="url(#mask-18)"
                              transform="translate(18.416919, 36.760418) scale(-1, 1) translate(-18.416919, -36.760418) "
                            ></path>
                          </g>
                          <path
                            d="M15.8265136,32.591884 C16.7820702,34.4221461 16.8455363,36.8169424 15.9885304,38.7096774 C14.7545788,38.6690049 13.6921995,37.263689 13.8383854,35.8653688 C13.996694,34.3505625 14.1441635,32.8616239 14.1692647,31.3364055 C15.2479027,31.9004516 14.747733,32.0278379 15.8265136,32.591884"
                            id="Fill-82"
                            fill="#E53F3E"
                            transform="translate(15.207373, 35.023041) scale(-1, 1) translate(-15.207373, -35.023041) "
                          ></path>
                          <path
                            d="M16.4545766,26.3549269 C17.8448779,26.3915516 20.8463508,27.9767478 19.3991259,29.517417 C18.8539606,30.0977385 18.0069992,30.1748774 17.2823681,29.8988773 C17.1562663,30.1533557 17.0148013,30.4073097 16.8590802,30.6543395 C16.5663201,31.7460449 16.5339362,32.8964258 17.0751541,33.8917871 C17.6873221,35.0173412 19.2500912,35.9325457 20.5691818,35.3178633 C21.3756134,34.9420903 22.0857623,36.05898 21.2803496,36.4673243 C19.3665815,37.437884 17.0552862,36.540008 15.995327,34.8304174 C15.4897605,34.0149325 15.3501741,33.1286011 15.4615754,32.2546297 C15.0546495,32.5781667 14.6027297,32.8241648 14.1124074,32.9535288 C12.4841227,33.3832223 10.5559688,32.7216252 10.0328965,31.0404105 C9.7279743,30.0599029 11.2484036,29.6612218 11.5770227,30.6195038 C11.9608762,31.7387056 13.4103893,31.8500055 14.374989,31.4933633 C15.192825,31.1910702 15.7683063,30.6150664 16.2923446,29.9807384 C16.3881935,29.8194507 16.4908675,29.6624367 16.5987372,29.5098132 C15.9024975,28.9858791 15.4347822,28.1997277 15.1663051,27.4328482 C15.0070188,26.9778508 15.3563834,26.5576545 15.804116,26.4072044 L16.4545766,26.3549269 Z M36.6438228,25.8131025 C38.3369181,26.4986279 39.2325484,28.1994986 38.3861718,29.6646382 C37.8775591,30.545467 36.2997312,29.7703609 36.8316025,28.8920043 C37.3145622,28.0943576 36.3655738,27.1873525 35.4997009,26.9645641 C34.0798952,26.5991154 32.8150333,27.364769 31.528794,27.7078225 C31.1778615,27.8013297 30.783148,27.418721 31.0205234,27.1454706 C32.2108207,25.7754379 34.7966388,25.0650451 36.6438228,25.8131025 Z M13.9829235,16.0608987 C15.5982192,16.9368315 15.7536519,18.6579344 14.9602816,20.0877401 C14.8199755,20.3408392 14.3769847,20.1670523 14.469596,19.8922298 C14.756228,19.0406447 14.0792393,17.8621886 13.2190345,17.5044091 C12.3949481,17.1615007 11.3921219,17.5289026 10.9219651,18.2196764 C10.2857253,19.1543644 8.7763152,18.3300428 9.34047248,17.3465138 C10.2102471,15.8303978 12.3787411,15.1910893 13.9829235,16.0608987 Z"
                            id="Combined-Shape"
                            fill="#0B264B"
                          ></path>
                          <path
                            d="M41.4697069,12.6330069 C41.418463,13.507504 40.8376585,14.410351 39.9835126,14.5309627 C38.7159914,14.7108855 37.8978018,13.2838128 37.1087197,12.2591106 C36.224121,11.1089474 34.9590457,10.2652872 33.5693464,9.89847838 C33.507707,11.2119026 34.5575336,12.2591106 35.3943128,13.2613069 C36.2323151,14.2636274 36.9224562,15.768787 36.150863,16.8242016 C35.3466157,17.9235089 33.5752169,17.5779626 32.5637926,16.672753 C31.5535914,15.7675435 30.9367083,14.4719002 29.9113418,13.5843473 C28.7789625,12.6070194 27.3147823,12.2472981 25.8064516,12.150187 C29.5214482,8.84766443 34.3666227,7.36152925 39.1128561,7.37327189 C39.772789,8.21463948 40.3209393,9.17194827 40.7853139,10.1280137 C41.1659127,10.9148496 41.5209508,11.7573908 41.4697069,12.6330069"
                            id="Fill-85"
                            fill="#F17574"
                            transform="translate(33.640553, 12.442396) scale(-1, 1) translate(-33.640553, -12.442396) "
                          ></path>
                          <path
                            d="M30.1375854,35.6600809 C30.8120137,35.9822829 31.4587989,36.3505556 32.1128586,36.7051831 C33.4655951,37.4389408 34.8062074,38.188251 36.1440715,38.9441637 C37.1686465,39.5231295 36.2095421,41.0125065 35.1978996,40.4135865 C33.8333622,39.6058808 32.4752911,38.7915727 31.1272427,37.9615652 C30.4520061,37.5457545 29.7691716,37.1396275 29.1241646,36.6852289 C28.6372571,36.3423391 28.2749863,36.0864556 28.2004629,35.5121849 C28.1863988,35.4037572 28.2942233,35.3100018 28.3963898,35.2786032 C29.0546526,35.0768602 29.5624137,35.385417 30.1375854,35.6600809 Z M32.3330694,34.0800635 C33.1179351,34.2855296 33.8966765,34.5144011 34.666554,34.7740341 C36.1626336,35.2782537 37.5984382,35.9110359 39.0530988,36.526264 C40.1930074,37.0084156 39.4729294,38.7499459 38.3994202,38.1347177 C37.0453254,37.3588281 35.5934046,36.7384174 34.1448681,36.1803653 C33.4294639,35.904683 32.7092247,35.6412048 31.9823779,35.3996275 L31.6913746,35.3085569 L31.6913746,35.3085569 L31.099329,35.1363209 C30.7045792,35.018085 30.3156032,34.8826066 29.9676745,34.6673723 C29.5201238,34.3905196 29.7368885,33.7289822 30.221668,33.6945428 C30.9269189,33.6445555 31.6537658,33.9021823 32.3330694,34.0800635 Z M1.45138325,23.1107119 C2.84304496,23.6701987 4.21658178,24.2613332 5.59918104,24.8363614 L6.05558167,25.0228143 L6.05558167,25.0228143 L6.99016535,25.3970517 C7.93101171,25.7771909 8.87412826,26.1850637 9.67085431,26.7109248 C10.124306,27.0101655 9.8956029,27.7220983 9.2518399,27.5634358 C7.81536029,27.2092354 6.44363596,26.5341778 5.06680371,26.0357259 C3.63345476,25.5170703 2.16748102,25.0775341 0.692444843,24.6543869 C-0.642041086,24.2715057 0.140294905,22.5837205 1.45138325,23.1107119 Z M1.69363463,18.6244943 C3.22899618,19.7043617 4.7789864,20.7605861 6.29077951,21.8758342 C7.00791006,22.404701 7.71902659,22.9424548 8.42380403,23.4885927 C9.13784629,24.0417733 9.99931318,24.582713 10.5683689,25.2953585 C10.8125054,25.6010414 10.5943755,26.1958069 10.1521016,26.0329884 C9.34817425,25.7368633 8.6238919,25.1752987 7.89213267,24.7255775 C7.09714502,24.2367866 6.29614336,23.7582242 5.48945277,23.2902258 C3.87265823,22.352049 2.23944705,21.4385214 0.559261542,20.6267764 C-0.746267139,19.9959594 0.4848178,17.7743502 1.69363463,18.6244943 Z"
                            id="Combined-Shape"
                            fill="#0B264B"
                          ></path>
                          <path
                            d="M33.3215557,57.1990176 C32.4339406,60.6383469 30.692306,63.8676835 28.5696874,66.7352534 C29.4298258,67.0525248 30.2607171,67.4507099 31.0449213,67.9114668 C35.1341493,70.313001 37.9000779,74.3305809 40.4273469,78.2145635 C40.9638459,79.040292 39.6415166,79.8032459 39.106958,78.9839558 C37.025963,75.793714 34.818847,72.518163 31.7240339,70.2083765 C28.9480804,68.1368118 25.2744046,66.820153 21.8046243,67.6668065 C18.7963172,68.4007874 16.4976828,70.7620814 15.2859519,73.5322157 C14.003561,76.4633107 13.9833493,79.6664293 14.9284476,82.6989295 C16.084556,86.4074649 18.6191012,89.6282892 21.5845595,92.0909884 C21.6220724,92.1231806 21.5727559,92.1875649 21.5321708,92.1585919 C15.65623,88.1667657 11.0625184,80.9895265 13.6336063,73.7623895 C14.723582,70.6976971 16.9315064,67.9645838 19.979105,66.6752884 C22.4199623,65.6425763 25.0884099,65.6800499 27.579318,66.4058113 L27.2092794,66.8489007 C29.7162312,63.9168977 31.5948955,60.6299704 33.2383556,57.1706144 C33.262581,57.1199494 33.3356448,57.14413 33.3215557,57.1990176 Z"
                            id="Combined-Shape"
                            fill="#F17574"
                          ></path>
                        </g>
                        <g
                          id="Page-1"
                          transform="translate(390.783410, 338.372214)"
                        >
                          <path
                            d="M23.957806,32.9532272 C23.957806,32.9532272 32.6403203,31.3120024 29.8688082,38.4810217 C25.5849884,50.1690389 0.54354651,45.5245849 0.0101992693,43.1016713 C-0.524410331,40.6787575 20.1012951,17.5021665 28.0838307,24.2774463 C31.4814736,27.2980463 23.957806,32.9532272 23.957806,32.9532272 Z"
                            id="Fill-1"
                            fill="#71BF59"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M23.95564,33.3368793 C23.95564,33.3368793 17.6612942,33.8259145 0,43.1887845 C0.533526153,45.5409004 25.5833682,50.0496429 29.8686251,38.703151 C32.6410669,31.7436112 23.95564,33.3368793 23.95564,33.3368793"
                            id="Fill-3"
                            fill="#71BF59"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M8.49898726,38.7096774 L8.29493088,38.3384552 C26.8864573,30.3492025 38.6700908,30.2326112 43.3179724,37.9908811 L42.9055047,38.1839167 C38.3982229,30.6599292 26.8217565,30.836466 8.49898726,38.7096774"
                            id="Fill-5"
                            fill="#35685E"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M1.15210548,42.3963134 L0.921658986,41.9798209 C2.42305277,41.1481057 3.90032269,40.3582937 5.31156936,39.6313364 L5.52995392,40.0548128 C4.12251627,40.7792304 2.64969023,41.5671378 1.15210548,42.3963134"
                            id="Fill-7"
                            fill="#35685E"
                            fillRule="nonzero"
                          ></path>
                          <g
                            id="Fill-9-Clipped"
                            transform="translate(32.258065, 36.866359)"
                          >
                            <mask id="mask-20" fill="white">
                              <use xlinkHref="https://i.imgur.com/w7GCRPb.png"></use>
                            </mask>
                            <g id="path-18"></g>
                            <path
                              d="M20.629147,0 L14.2856762,0 L7.94282346,0 C7.94282346,0 -0.725844977,36.5408414 0.0490149738,41.760693 C0.824492837,46.9811714 3.99622818,49.7697733 3.99622818,49.7697733 L14.2856762,49.7697733 L24.5757423,49.7697733 C24.5757423,49.7697733 27.7480955,46.9811714 28.5223376,41.760693 C29.2978154,36.5408414 20.629147,0 20.629147,0"
                              id="Fill-9"
                              fill="#FFE163"
                              fillRule="nonzero"
                              mask="url(#mask-20)"
                            ></path>
                          </g>
                          <polygon
                            id="Fill-12"
                            stroke="#1A3E6F"
                            strokeWidth="0.5"
                            fill="#1A3E6F"
                            fillRule="nonzero"
                            points="38.7096774 39.459575 52.5345622 39.459575 52.5345622 38.8814388 38.7096774 38.8814388"
                          ></polygon>
                          <polygon
                            id="Fill-14"
                            stroke="#1A3E6F"
                            strokeWidth="0.5"
                            strokeLinejoin="round"
                            points="31.3364055 79.0909115 59.9078341 79.0909115 59.9078341 78.5127752 31.3364055 78.5127752"
                          ></polygon>
                          <path
                            d="M69.9840328,41.1454502 C69.9840328,41.1454502 62.3462354,35.6695792 60.4266062,39.6548748 C56.2266902,48.9295661 78.842766,65.6141961 81.0202845,64.4591535 C83.1978034,63.3047243 86.0908202,34.959869 73.5860844,34.1391324 C65.8746043,33.5018041 69.9840328,41.1454502 69.9840328,41.1454502"
                            id="Fill-15"
                            fill="#71BF59"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M69.5850629,40.7779836 C69.5850629,40.7779836 62.2496862,35.2160133 60.4060653,39.2639713 C56.3724461,48.6844923 78.0930321,65.6314614 80.1843318,64.4582576 C79.5834479,58.9268167 73.3244476,43.0539865 69.5850629,40.7779836"
                            id="Fill-16"
                            fill="#71BF59"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M40.7875233,16.5453936 C40.7875233,16.5453936 41.1430984,23.6807817 35.2380575,22.9950337 C29.331769,22.3086843 34.0284785,1.84331797 34.0284785,1.84331797 C34.0284785,1.84331797 53.0966577,12.5656492 47.9738819,17.2311416 C44.3364117,20.9961386 40.7875233,16.5453936 40.7875233,16.5453936"
                            id="Fill-17"
                            fill="#71BF59"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M40.8271763,17.0772326 L34.1013825,1.84331797 C34.1013825,1.84331797 53.0757562,12.9535233 47.9781818,17.7877871 C44.358606,21.6889806 40.8271763,17.0772326 40.8271763,17.0772326"
                            id="Fill-18"
                            fill="#71BF59"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M53.4562212,22.7788791 C58.6715073,15.9380328 67.7739517,11.8325198 78.3410138,8.29493088 C78.3410138,8.29493088 73.4005773,24.9908931 66.9319522,27.3203869 C60.6048029,29.1472931 53.4562212,22.7788791 53.4562212,22.7788791"
                            id="Fill-19"
                            fill="#71BF59"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M31.3364055,17.1792007 C31.3364055,17.1792007 22.2969409,6.81674842 15.6682028,1.84331797 C15.6682028,1.84331797 16.9352471,14.0482878 20.9563656,17.2096402 C24.9908533,19.9802497 31.3364055,17.1792007 31.3364055,17.1792007"
                            id="Fill-20"
                            fill="#71BF59"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M51.0929603,26.7281106 L50.6912442,26.4874879 C56.4169538,17.2964373 67.730283,12.3122847 79.1032423,8.29493088 L79.2626728,8.72940571 C67.9713127,12.7178355 56.7427204,17.6589099 51.0929603,26.7281106"
                            id="Fill-21"
                            fill="#35685E"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M47.4445447,36.8663594 L47.0046083,36.8014777 C47.3706307,33.5310312 48.1671285,30.4525267 49.371632,27.6497696 L49.7695853,27.8734765 C48.5869604,30.6255447 47.804654,33.6513328 47.4445447,36.8663594"
                            id="Fill-22"
                            fill="#35685E"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M45.6137031,36.8663594 C44.4976749,23.1295456 39.2381022,10.8556755 35.0230415,2.97800316 L35.4395916,2.76497696 C39.6753544,10.6800977 44.9600205,23.0153587 46.0829493,36.8295249 L45.6137031,36.8663594 Z"
                            id="Fill-23"
                            fill="#35685E"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M79.6457188,62.6728111 C79.2641404,61.1373178 78.825325,59.6278395 78.3410138,58.1861214 L78.8708209,58.0645161 C79.3588012,59.5165193 79.8012855,61.0362826 80.1843318,62.5820608 L79.6457188,62.6728111 Z"
                            id="Fill-24"
                            fill="#35685E"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M76.9797086,54.3778802 C72.7928372,42.6251854 65.2859851,34.5073447 57.8529243,33.697378 C54.2601024,33.3060254 51.1987008,34.7787471 49.2429258,37.8459581 L48.8479263,37.605452 C50.9041563,34.3819422 54.1168608,32.8316769 57.9050121,33.2454445 C65.5141795,34.0754029 73.1735746,42.3119821 77.4193548,54.2288512 L76.9797086,54.3778802 Z"
                            id="Fill-25"
                            fill="#1A3E6F"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M35.3678566,45.0117073 C35.3678566,45.0117073 36.8813434,38.7819102 33.0006547,39.7294127 C24.1082406,42.1248055 28.5357179,65.4235995 30.5156364,66.3121208 C32.4949508,67.2006422 49.9106211,55.3993531 43.9092385,45.9515993 C40.3022462,40.0490521 35.3678566,45.0117073 35.3678566,45.0117073"
                            id="Fill-26"
                            fill="#71BF59"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M35.3003924,45.2478395 C35.3003924,45.2478395 31.3436945,58.2476076 30.4147465,66.3126443 C32.4076958,67.1913376 49.943338,55.5205852 43.9006135,46.1773352 C40.2687736,40.3400775 35.3003924,45.2478395 35.3003924,45.2478395"
                            id="Fill-27"
                            fill="#71BF59"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M43.814093,36.8663594 C40.0566022,27.5004129 34.6178059,18.9260254 27.6497696,11.3817918 L27.984559,11.0599078 C34.9912249,18.6460991 40.4606796,27.2687066 44.2396313,36.6885092 L43.814093,36.8663594 Z"
                            id="Fill-28"
                            fill="#1A3E6F"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M25.4707438,9.21658986 C22.47937,6.14306245 19.1810086,3.17600055 15.6682028,0.397915006 L15.9573199,0 C19.4867198,2.79139373 22.8003986,5.77309465 25.8064516,8.86126108 L25.4707438,9.21658986 Z"
                            id="Fill-29"
                            fill="#1A3E6F"
                            fillRule="nonzero"
                          ></path>
                          <path
                            d="M30.8735383,64.516129 L30.4147465,64.4349962 C30.622384,63.2718859 35.5801951,35.9295047 42.6255711,34.1476792 C43.7936874,33.9934649 44.7510578,34.2257154 45.4983041,34.8264699 C46.8280543,35.89668 46.99777,37.7435356 47.0046083,37.8215718 L46.5402215,37.8605898 C46.5383565,37.8438678 46.3773442,36.1289304 45.2030113,35.1863033 C44.5595837,34.6697783 43.7215737,34.4734492 42.7126048,34.6035094 C35.9836584,36.3066794 30.9238936,64.2343318 30.8735383,64.516129"
                            id="Fill-30"
                            fill="#1A3E6F"
                            fillRule="nonzero"
                          ></path>
                        </g>
                        <line
                          x1="78.3410138"
                          y1="408.033586"
                          x2="129.493088"
                          y2="407.957467"
                          id="Stroke-261"
                          stroke="#1A3E6F"
                          strokeLinecap="round"
                        ></line>
                      </g>
                    </g>
                  </g>
                </svg>
              </div>
              <div className="w-full pt-0 pl-0 sm:pl-6">
                <h1
                  className="font-bold font-title text-[25px] text-primary leading-8]"
                  data-testid="cibilScore"
                >
                  Bingo! Your Bureau Score is undefined
                </h1>
                <p className="mt-3 app-text-dm-small text-gray text-opacity-80">
                  {" "}
                  Good score means you get access to the best rates and widest
                  range of Home Loan Offers. Scroll down to see your Offers
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container mx-auto px-4 sm:px-0 mt-10 sm:w-7/12">
        <div className="pb-4 bg-white sm:pb-8">
          <div className="container mx-auto">
            <div className="flex items-center">
              <img className="w-12" src={girt} alt={girt} /> 
              <div className="ml-4">
                <p className="flex text-sm font-body text-secondary">
                  Your Personalised Offers are now Unlocked!
                </p>
              </div>
            </div>
            <div className="grid grid-cols-6 mt-5 sm:grid-cols-12 gap-9">
              <label
                htmlhtmlFor="loan_amount"
                className="relative col-span-6 sm:col-span-4"
              >
                <div className="app-form-wrapper">
                  <span className="app-form-label">Required Loan Amount</span>
                </div>
                <div className="relative rounded-md">
                <input
                        style={{ borderRadius: "6px" }}
                        type="text"
                        className="block w-full opacity-90 border"
                        placeholder="Enter your loan amount requirement"
                        data-testid="loan_amount_needed_input"
                        defaultValue={amt1}
                      />
                  <input
                    className="absolute bottom-0 w-full  rounded outline-none appearance-none bg-secondary"
                    type="range"
                    min="1"
                    max="30"
                    step="1"
                    data-testid="loan_amount_needed_slider"
                    defaultValue={amt1}
                    style={{ zIndex: "5",height:"0px" }}

                  />
                   <div className="range-slider">
                          <div className="range-slider__track"></div>
                          <div
                            className="range-slider__range"
                            style={{ left: " 0px", width: "0%" }}
                          ></div>
                        </div>
                </div>
                <p  style={{textTransform:"capitalize",fontSize: "12px",
    marginTop: "10px"}}>{amountInWords5}</p>

              </label>
              <label
                htmlhtmlFor="loan_amount"
                className="relative col-span-6 sm:col-span-4"
              >
                <div className="app-form-wrapper">
                  <span className="app-form-label">
                    Preferred Loan Tenure (In YEARS)
                  </span>
                </div>
                <div className="relative rounded-md">
                  <input
                    type="text"
                    className="block w-full text-left"
                    data-testid="preferred_loan_tenure_input"
                    defaultValue={amt2}
                  />
                  <input
                    className="absolute bottom-0 w-full  rounded outline-none appearance-none bg-secondary"
                    type="range"
                    min="1"
                    max="30"
                    step="1"
                    data-testid="preferred_loan_tenure_slider"
                    defaultValue={amt2}
                    style={{ zIndex: "5",height:"0px" }}
                  />
                    <div className="range-slider">
                          <div className="range-slider__track"></div>
                          <div
                            className="range-slider__range"
                            style={{ left: "0px", width: "20%" }}
                          ></div>
                        </div>
                </div>
                <p  style={{textTransform:"capitalize",fontSize: "12px",
    marginTop: "10px"}}>{amountInWords6}</p>
              </label>
            </div>
          </div>
        </div>

        <div className="grid col-md-4 mt-4 gap-x-8 sm:gap-y-5 gap-y-4 ">
                            <div
                              className={` form__item ${
                                errors.explorename && "input__error"
                              }`}
                            >
                              <label
                                htmlhtmlFor="name"
                                className="col-span-6 lg:col-span-2 relative block"
                              >
                                <div className="app-form-wrapper">
                                  <span
                                    data-testid="name_label"
                                    className="app-form-label"
                                  >
                                    Full Name
                                  </span>
                                </div>
                                <div
                                  className="relative rounded-md"
                                  role="presentation"
                                >
                                  <Input
                                    name={"explorename"}
                                    className="block w-full px-5 py-[15px] border shadow-none disabled:bg-disabled"
                                    aria-invalid="false"
                                    aria-describedby="name-error"
                                    type="text"
                                    placeholder="Enter name"
                                    data-testid="name"
                                  />
                                </div>
                                <p className={"error__feedback"}>
                                  {errors.explorename}
                                </p>
                              </label>
                            </div>
                          </div>
        <div className="mx-auto">
          <div className="grid grid-cols-12 my-4">
            <div className="col-span-12 space-y-4 sm:space-y-8">
              <div className="w-full" data-testid="offers-container-0">
                <div className="relative border border-gray-300 rounded-10">
                  <div className="hidden absolute -left-2 top-[10px] bottom-[10px] w-2 rounded-tl-full rounded-bl-full bg-secondary"></div>
                  <div className="hidden absolute -right-2 top-[10px] bottom-[10px] w-2 rounded-tr-full rounded-br-full bg-secondary"></div>
                  <div className="py-[18px]">
                    <div data-testid="container-hdfc-bank">
                      <div className="px-4 pb-5">
                        <div className="relative w-20 h-10">
                          <div
                            style={{
                              display: "block",
                              overflow: " hidden",
                              position: "absolute",
                              inset: "0px",
                              boxSizing: "border-box",
                              margin: "0px",
                            }}
                          >
                            <img
                        alt="description of image"
                     src="{hdfc}"
                        decoding="async"
                        style={{
                          position:" absolute",
                          inset: "0px",
                          boxSizing: "border-box",
                          padding: "0px",
                          border: "none",
                          margin: "auto",
                          display: "block",
                          width: "0px",
                          height: "0px",
                          minWidth: "100%",
                          maxWidth: "100%",
                          minHeight: "100%",
                          maxHeight: "100%",
                          objectFit: "contain",
                          objectPosition: "left center"
                        }}
                        sizes="100vw"
                         srcSet=  {`${hdfc} 640w, ${hdfc} 750w,${hdfc} 828w,${hdfc} 1080w,${hdfc} 1200w,${hdfc} 1920w,${hdfc} 2048w,${hdfc} 3840w`}
                         
                      /> 
                          </div>
                        </div>
                      </div>
                      <div className="grid grid-cols-4 px-4 pt-4 pb-3 border-t gap-x-1 gap-y-5 border-gray-light">
                        <div className="col-span-2 sm:col-span-1">
                          <img
                            // src="/icons/loan-amount.svg"
                            alt="Loan Amount"
                            className="h-8 hidden"
                          />
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Loan Amount
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="offers_hdfc-bank"
                          >
                            4, 00, 00, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            indian rupees
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          <img
                            src="/icons/rate-of-interest.svg"
                            alt="Rate of Interest"
                            className="h-8 hidden"
                          />
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Rate of Interest
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            6.7%
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 sm:col-span-1">
                          <img
                            src="/icons/monthly-emi.svg"
                            alt="Monthly EMI"
                            className="h-8 hidden"
                          />
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Monthly EMI
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="emi_hdfc-bank"
                          >
                            2, 58, 111/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          <img
                            src="/icons/processing-fees.svg"
                            alt="Processing Fee"
                            className="h-8 hidden"
                          />
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Processing Fee
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            5, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            + GST
                          </div>
                        </div>
                        <div className="absolute top-2 right-5">
                          <div className="flex items-center h-full mt-4 sm:mt-3">
                        
                            <button
                            type={"primary"}
                            onClick={handleSubmit}
                              className="w-auto h-9 app-btn app-btn-primary"
                              data-testid="grab-this-offer-hdfc-bank"
                              data-category="loan_offer"
                              data-action="Grab This Offer"
                              data-label="/cibil-score"
                            >
                              <svg width="17" height="15" viewBox="0 0 17 15">
                                <g fill="none" fillRule="evenodd">
                                  <g fill="#FFF" fillRule="nonzero">
                                    <path
                                      d="M14.53 14.398V22h-4.366c-1.02 0-1.846-.82-1.846-1.833v-5.77h6.211zm7.34 0v5.769c0 1.013-.825 1.833-1.846 1.833h-4.365v-7.602h6.212zM12.037 7.5c.668-.666 1.833-.667 2.5 0 .244.242.466.747.652 1.283.186-.536.409-1.04.652-1.283.668-.666 1.832-.666 2.5 0 .335.332.519.775.519 1.246s-.184.914-.518 1.247c-.071.07-.165.14-.276.208l3.943-.001c.647 0 1.18.339 1.18.75v1.5c0 .411-.533.75-1.18.75H8.18c-.647 0-1.18-.339-1.18-.75v-1.5c0-.411.533-.75 1.18-.75h4.132c-.11-.067-.205-.136-.276-.208-.335-.332-.518-.775-.518-1.246s.183-.914.518-1.246zm1.25.481c-.205 0-.397.079-.542.224-.146.145-.225.336-.225.54 0 .206.08.398.225.542.176.176.944.449 1.758.67-.223-.81-.497-1.576-.674-1.752-.144-.144-.337-.224-.542-.224zm3.804 0c-.205 0-.397.08-.542.224-.177.176-.45.941-.674 1.753.815-.222 1.582-.495 1.759-.67.145-.145.225-.338.225-.542 0-.205-.081-.396-.226-.54-.145-.145-.337-.225-.542-.225z"
                                      transform="translate(-7.000000, -7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </svg>
                              <span className="ml-2">Grab this Offer</span>
                            </button>
                          
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="px-4 sm:mb-3 mb-2 text-xs">
                      *Terms and conditions apply. All loans at the sole
                      discretion of HDFC Ltd.
                    </div>
                  </div>
                  <div className="relative px-4 lg:absolute bottom-4 lg:right-8 lg:-bottom-4 lg:px-0">
                    <button
                      type="button"
                      className="inline-flex items-center justify-end w-full mt-0 sm:mt-4 cursor-pointer"
                      data-testid="hdfc-bank"
                    >
                      <div className="relative inline-flex items-center h-10 px-3 font-medium tracking-normal text-white rounded-l-full rounded-r-full app-text-dm-xs bg-secondary">
                        <div className="inline-flex items-center">
                          <div>
                            <div
                              style={{
                                display: "inline-block",
                                maxWidth: "100%",
                                overflow: "hidden",
                                position: "relative",
                                boxSizing: "border-box",
                                margin: "0px",
                              }}
                            >
                              <div
                                style={{
                                  boxSizing: "border-box",
                                  display: "block",
                                  maxWidth: "100%",
                                }}
                              >
                                <img
                                  alt="description of image"
                                  aria-hidden="true"
                                  role="presentation"
                                  src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjIiIGhlaWdodD0iMjIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+"
                                  style={{
                                    maxWidth: " 100%",
                                    display: "block",
                                    margin: "0px",
                                    border: "none",
                                    padding: "0px",
                                  }}
                                />
                              </div>
                              {/* <img
                          alt="description of image"
                          src="/_next/image?url=%2Ficons%2Fgift-open.svg&amp;w=48&amp;q=75"
                          decoding="async"
                          className="flex-shrink-0"
                          style={{
                            position: "absolute",
                            inset: "0px",
                            boxSizing: "border-box",
                            padding: "0px",
                            border: "none",
                            margin: "auto",
                            display: "block",
                            width: "0px",
                            height: "0px",
                            minWidth: "100%",
                            maxWidth: "100%",
                            minHeight: "100%",
                            maxHeight: "100%",
                          }}
                          srcSet="
                            /_next/image?url=%2Ficons%2Fgift-open.svg&amp;w=32&amp;q=75 1x,
                            /_next/image?url=%2Ficons%2Fgift-open.svg&amp;w=48&amp;q=75 2x
                          "
                        /> */}
                            </div>
                          </div>
                          <span className="ml-[6px]">
                            Bigger loan amount unlocked! Click to select Offer
                          </span>
                          <div className="ml-6">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              className="transform rotate-90"
                            >
                              <g fill="none" fillRule="evenodd">
                                <g fill="#FFF">
                                  <g>
                                    <path
                                      d="M8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8 3.582-8 8-8zm-.554 4.397c-.293-.218-.71-.194-.976.073l-.073.084c-.218.293-.194.71.073.976L8.939 8l-2.47 2.47-.072.084c-.218.293-.194.71.073.976.293.293.767.293 1.06 0L11.06 8 7.53 4.47z"
                                      transform="translate(-7.000000, -7.000000) translate(7.000000, 7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                          </div>
                        </div>
                      </div>
                    </button>
                  </div>
                </div>
              </div>
              <div className="w-full" data-testid="offers-container-1">
                <div className="relative border border-gray-300 rounded-10">
                  <div className="hidden absolute -left-2 top-[10px] bottom-[10px] w-2 rounded-tl-full rounded-bl-full bg-secondary"></div>
                  <div className="hidden absolute -right-2 top-[10px] bottom-[10px] w-2 rounded-tr-full rounded-br-full bg-secondary"></div>
                  <div className="py-[18px]">
                    <div data-testid="container-bajaj">
                      <div className="px-4 pb-5">
                        <div className="relative w-20 h-10">
                          <div
                            style={{
                              display: "block",
                              overflow: "hidden",
                              position: "absolute",
                              inset: "0px",
                              boxSizing: " border-box",
                              margin: " 0px",
                            }}
                          >
                            <img
                        alt="description of image"
                        src={bajaj}
                        decoding="async"
                        style={{
                          position: "absolute",
                          inset: "0px",
                          boxSizing: "border-box",
                          padding: "0px",
                          border: "none",
                          margin: "auto",
                          display: "block",
                          width: "0px",
                          height: "0px",
                          minWidth: "100%",
                          maxWidth: "100%",
                          minHeight: "100%",
                          maxHeight: "100%",
                          objectFit: "contain",
                          objectPosition: "left center"
                        }}
                        sizes="100vw"
                      
                        srcSet=  {`${bajaj} 640w, ${bajaj} 750w,${bajaj} 828w,${bajaj} 1080w,${bajaj} 1200w,${bajaj} 1920w,${bajaj} 2048w,${bajaj} 3840w`}
                      /> 
                          </div>
                        </div>
                      </div>
                      <div className="grid grid-cols-4 px-4 pt-4 pb-3 border-t gap-x-1 gap-y-5 border-gray-light">
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                      src="/icons/loan-amount.svg"
                      alt="Loan Amount"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Loan Amount
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="offers_bajaj"
                          >
                            4, 00, 00, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            indian rupees
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/rate-of-interest.svg"
                      alt="Rate of Interest"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Rate of Interest
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            6.8%
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                      src="/icons/monthly-emi.svg"
                      alt="Monthly EMI"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Monthly EMI
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="emi_bajaj"
                          >
                            2, 60, 770/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/processing-fees.svg"
                      alt="Processing Fee"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Processing Fee
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            5, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            + GST
                          </div>
                        </div>
                        <div className="absolute top-2 right-5">
                          <div className="flex items-center h-full mt-4 sm:mt-3">
                         
                            <button
                              // type="button"
                              type={"primary"}
                              onClick={handleSubmit}
                              className="w-auto h-9 app-btn app-btn-primary"
                              data-testid="grab-this-offer-bajaj"
                              data-category="loan_offer"
                              data-action="Grab This Offer"
                              data-label="/cibil-score"
                            >
                              <svg width="17" height="15" viewBox="0 0 17 15">
                                <g fill="none" fillRule="evenodd">
                                  <g fill="#FFF" fillRule="nonzero">
                                    <path
                                      d="M14.53 14.398V22h-4.366c-1.02 0-1.846-.82-1.846-1.833v-5.77h6.211zm7.34 0v5.769c0 1.013-.825 1.833-1.846 1.833h-4.365v-7.602h6.212zM12.037 7.5c.668-.666 1.833-.667 2.5 0 .244.242.466.747.652 1.283.186-.536.409-1.04.652-1.283.668-.666 1.832-.666 2.5 0 .335.332.519.775.519 1.246s-.184.914-.518 1.247c-.071.07-.165.14-.276.208l3.943-.001c.647 0 1.18.339 1.18.75v1.5c0 .411-.533.75-1.18.75H8.18c-.647 0-1.18-.339-1.18-.75v-1.5c0-.411.533-.75 1.18-.75h4.132c-.11-.067-.205-.136-.276-.208-.335-.332-.518-.775-.518-1.246s.183-.914.518-1.246zm1.25.481c-.205 0-.397.079-.542.224-.146.145-.225.336-.225.54 0 .206.08.398.225.542.176.176.944.449 1.758.67-.223-.81-.497-1.576-.674-1.752-.144-.144-.337-.224-.542-.224zm3.804 0c-.205 0-.397.08-.542.224-.177.176-.45.941-.674 1.753.815-.222 1.582-.495 1.759-.67.145-.145.225-.338.225-.542 0-.205-.081-.396-.226-.54-.145-.145-.337-.225-.542-.225z"
                                      transform="translate(-7.000000, -7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </svg>
                              <span className="ml-2">Grab this Offer</span>
                            </button>
                          
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="relative px-4 lg:absolute bottom-4 lg:right-8 lg:-bottom-4 lg:px-0">
                    <button
                      type="button"
                      className="inline-flex items-center justify-end w-full mt-0 sm:mt-4 cursor-pointer"
                      data-testid="bajaj"
                    >
                      <div className="relative inline-flex items-center h-10 px-3 font-medium tracking-normal text-white rounded-l-full rounded-r-full app-text-dm-xs bg-secondary">
                        <div className="inline-flex items-center">
                          <div>
                            <div
                              style={{
                                display: "inline-block",
                                maxWidth: "100%",
                                overflow: "hidden",
                                position: "relative",
                                boxSizing: "border-box",
                                margin: "0px",
                              }}
                            >
                              <div
                                style={{
                                  boxSizing: " border-box",
                                  display: "block",
                                  maxWidth: "100%",
                                }}
                              >
                                {/* <img
                             alt="description of image"
                            aria-hidden="true"
                            role="presentation"
                            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjIiIGhlaWdodD0iMjIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+"
                            style={{
                              maxWidth: "100%",
                              display: "block",
                              margin: "0px",
                              border: "none",
                              padding: "0px"
                            }}
                          /> */}
                              </div>
                              {/* <img
                          alt="description of image"
                          src="/_next/image?url=%2Ficons%2Fgift-open.svg&amp;w=48&amp;q=75"
                          decoding="async"
                          className="flex-shrink-0"
                          style={{
                            position: "absolute",
                            inset: "0px",
                            boxSizing:" border-box",
                            padding: "0px",
                            border: "none",
                            margin: "auto",
                            display: "block",
                            width: "0px",
                            height: "0px",
                            minwidth: "100%",
                            maxwidth: "100%",
                            minHeight: "100%",
                            maxHeight: "100%"
                          }}
                          srcSet="
                            /_next/image?url=%2Ficons%2Fgift-open.svg&amp;w=32&amp;q=75 1x,
                            /_next/image?url=%2Ficons%2Fgift-open.svg&amp;w=48&amp;q=75 2x
                          "
                        /> */}
                            </div>
                          </div>
                          <span className="ml-[6px]">
                            Bigger loan amount unlocked! Click to select Offer
                          </span>
                          <div className="ml-6">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              className="transform rotate-90"
                            >
                              <g fill="none" fillRule="evenodd">
                                <g fill="#FFF">
                                  <g>
                                    <path
                                      d="M8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8 3.582-8 8-8zm-.554 4.397c-.293-.218-.71-.194-.976.073l-.073.084c-.218.293-.194.71.073.976L8.939 8l-2.47 2.47-.072.084c-.218.293-.194.71.073.976.293.293.767.293 1.06 0L11.06 8 7.53 4.47z"
                                      transform="translate(-7.000000, -7.000000) translate(7.000000, 7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                          </div>
                        </div>
                      </div>
                    </button>
                  </div>
                </div>
              </div>
              <div className="w-full" data-testid="offers-container-2">
                <div className="relative border border-gray-300 rounded-10">
                  <div className="hidden absolute -left-2 top-[10px] bottom-[10px] w-2 rounded-tl-full rounded-bl-full bg-secondary"></div>
                  <div className="hidden absolute -right-2 top-[10px] bottom-[10px] w-2 rounded-tr-full rounded-br-full bg-secondary"></div>
                  <div className="py-[18px]">
                    <div data-testid="container-pnbhfl-bank">
                      <div className="px-4 pb-5">
                        <div className="relative w-20 h-10">
                          <div
                            style={{
                              display: "block",
                              overflow: "hidden",
                              position: "absolute",
                              inset: "0px",
                              boxSizing: "border-box",
                              margin: "0px",
                            }}
                          >
                         <img
                        alt="description of image"
                        src={pnb}
                        decoding="async"
                        style={{
                          position: "absolute",
                          inset: "0px",
                          boxSizing:"border-box",
                          padding: "0px",
                          border: "none",
                          margin: "auto",
                          display: "block",
                          width: "0px",
                          height: "0px",
                          minWidth: "100%",
                          maxWidth: "100%",
                          minHeight: "100%",
                          maxHeight: "100%",
                          objectFit: "contain",
                          objectPosition:" left center"
                        }}
                        sizes="100vw"
                        srcSet={`${pnb} 640w, ${pnb} 750w,${pnb} 828w,${pnb} 1080w,${pnb} 1200w,${pnb} 1920w,${pnb} 2048w,${pnb} 3840w`}
                      /> 
                          </div>
                        </div>
                      </div>
                      <div className="grid grid-cols-4 px-4 pt-4 pb-3 border-t gap-x-1 gap-y-5 border-gray-light">
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                    
                      src="/icons/loan-amount.svg"
                      alt="Loan Amount"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Loan Amount
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="offers_pnbhfl-bank"
                          >
                            4, 00, 00, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            indian rupees
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/rate-of-interest.svg"
                      alt="Rate of Interest"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Rate of Interest
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            7.35%
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                      src="/icons/monthly-emi.svg"
                      alt="Monthly EMI"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Monthly EMI
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="emi_pnbhfl-bank"
                          >
                            2, 75, 589/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/processing-fees.svg"
                      alt="Processing Fee"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Processing Fee
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            10, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            + GST
                          </div>
                        </div>
                        <div className="absolute top-2 right-5">
                          <div className="flex items-center h-full mt-4 sm:mt-3">
                         
                            <button
                              // type="button"
                              type={"primary"}
                              onClick={handleSubmit}
                              className="w-auto h-9 app-btn app-btn-primary"
                              data-testid="grab-this-offer-pnbhfl-bank"
                              data-category="loan_offer"
                              data-action="Grab This Offer"
                              data-label="/cibil-score"
                            >
                              <svg width="17" height="15" viewBox="0 0 17 15">
                                <g fill="none" fillRule="evenodd">
                                  <g fill="#FFF" fillRule="nonzero">
                                    <path
                                      d="M14.53 14.398V22h-4.366c-1.02 0-1.846-.82-1.846-1.833v-5.77h6.211zm7.34 0v5.769c0 1.013-.825 1.833-1.846 1.833h-4.365v-7.602h6.212zM12.037 7.5c.668-.666 1.833-.667 2.5 0 .244.242.466.747.652 1.283.186-.536.409-1.04.652-1.283.668-.666 1.832-.666 2.5 0 .335.332.519.775.519 1.246s-.184.914-.518 1.247c-.071.07-.165.14-.276.208l3.943-.001c.647 0 1.18.339 1.18.75v1.5c0 .411-.533.75-1.18.75H8.18c-.647 0-1.18-.339-1.18-.75v-1.5c0-.411.533-.75 1.18-.75h4.132c-.11-.067-.205-.136-.276-.208-.335-.332-.518-.775-.518-1.246s.183-.914.518-1.246zm1.25.481c-.205 0-.397.079-.542.224-.146.145-.225.336-.225.54 0 .206.08.398.225.542.176.176.944.449 1.758.67-.223-.81-.497-1.576-.674-1.752-.144-.144-.337-.224-.542-.224zm3.804 0c-.205 0-.397.08-.542.224-.177.176-.45.941-.674 1.753.815-.222 1.582-.495 1.759-.67.145-.145.225-.338.225-.542 0-.205-.081-.396-.226-.54-.145-.145-.337-.225-.542-.225z"
                                      transform="translate(-7.000000, -7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </svg>
                              <span className="ml-2">Grab this Offer</span>
                            </button>
                           
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="relative px-4 lg:absolute bottom-4 lg:right-8 lg:-bottom-4 lg:px-0">
                    <button
                      type="button"
                      className="inline-flex items-center justify-end w-full mt-0 sm:mt-4 cursor-pointer"
                      data-testid="pnbhfl-bank"
                    >
                      <div className="relative inline-flex items-center h-10 px-3 font-medium tracking-normal text-white rounded-l-full rounded-r-full app-text-dm-xs bg-secondary">
                        <div className="inline-flex items-center">
                          <div>
                            <div
                              style={{
                                display: "inline-block",
                                maxWidth: "100%",
                                overflow: "hidden",
                                position: "relative",
                                boxSizing: "border-box",
                                margin: "0px",
                              }}
                            >
                              <div
                                style={{
                                  boxSizing: "border-box",
                                  display: "block",
                                  maxWidth: "100%",
                                }}
                              >
                                {/* <img
                             alt="description of image"
                            aria-hidden="true"
                            role="presentation"
                            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjIiIGhlaWdodD0iMjIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+"
                            style={{
                              maxWidth: "100%",
                              display: "block",
                              margin: "0px",
                              border: "none",
                              padding: "0px"
                            }}
                          /> */}
                              </div>
                              <noscript></noscript>
                              {/* <img
                          alt="description of image"
                          src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                          decoding="async"
                          className="flex-shrink-0"
                          style={{
                            position: "absolute",
                            inset: "0px",
                            boxSizing: "border-box",
                            padding: "0px",
                            border: "none",
                            margin: "auto",
                            display: "block",
                            width: "0px",
                            height: "0px",
                            minWidth: "100%",
                            maxWidth: "100%",
                            minHeight: "100%",
                            maxHeight: "100%"
                          
                          }}                        /> */}
                            </div>
                          </div>
                          <span className="ml-[6px]">
                            Bigger loan amount unlocked! Click to select Offer
                          </span>
                          <div className="ml-6">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              className="transform rotate-90"
                            >
                              <g fill="none" fillRule="evenodd">
                                <g fill="#FFF">
                                  <g>
                                    <path
                                      d="M8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8 3.582-8 8-8zm-.554 4.397c-.293-.218-.71-.194-.976.073l-.073.084c-.218.293-.194.71.073.976L8.939 8l-2.47 2.47-.072.084c-.218.293-.194.71.073.976.293.293.767.293 1.06 0L11.06 8 7.53 4.47z"
                                      transform="translate(-7.000000, -7.000000) translate(7.000000, 7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                          </div>
                        </div>
                      </div>
                    </button>
                  </div>
                </div>
              </div>
              <div className="w-full" data-testid="offers-container-3">
                <div className="relative border border-gray-300 rounded-10">
                  <div className="hidden absolute -left-2 top-[10px] bottom-[10px] w-2 rounded-tl-full rounded-bl-full bg-secondary"></div>
                  <div className="hidden absolute -right-2 top-[10px] bottom-[10px] w-2 rounded-tr-full rounded-br-full bg-secondary"></div>
                  <div className="py-[18px]">
                    <div data-testid="container-icici-bank">
                      <div className="px-4 pb-5">
                        <div className="relative w-20 h-10">
                          <div
                            style={{
                              display: "block",
                              overflow: "hidden",
                              position: "absolute",
                              inset: "0px",
                              boxSizing: "border-box",
                              margin: "0px",
                            }}
                          >
                            <noscript></noscript>
                           <img
                        alt="description of image"
                        src={icici}
                        decoding="async"
                        style={{
                          position: "absolute",
                          inset: "0px",
                          boxSizing: "border-box",
                          padding: "0px",
                          border: "none",
                          margin: "auto",
                          display: "block",
                          width: "0px",
                          height: "0px",
                          minWidth: "100%",
                          maxWidth: "100%",
                          minHeight: "100%",
                          maxHeight: "100%",
                          objectFit: "contain",
                          objectPosition: "left center"
                        }}
                      /> 
                          </div>
                        </div>
                      </div>
                      <div className="grid grid-cols-4 px-4 pt-4 pb-3 border-t gap-x-1 gap-y-5 border-gray-light">
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                  
                      src="/icons/loan-amount.svg"
                      alt="Loan Amount"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Loan Amount
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="offers_icici-bank"
                          >
                            4, 00, 00, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            indian rupees
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/rate-of-interest.svg"
                      alt="Rate of Interest"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Rate of Interest
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            6.7%
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                      src="/icons/monthly-emi.svg"
                      alt="Monthly EMI"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Monthly EMI
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="emi_icici-bank"
                          >
                            2, 58, 111/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/processing-fees.svg"
                      alt="Processing Fee"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Processing Fee
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            10, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            + GST
                          </div>
                        </div>
                        <div className="absolute top-2 right-5">
                          <div className="flex items-center h-full mt-4 sm:mt-3">
                           
             
                            <button
                              // type="button"
                              type={"primary"}
                              onClick={handleSubmit}
                              className="w-auto h-9 app-btn app-btn-primary"
                              data-testid="grab-this-offer-icici-bank"
                              data-category="loan_offer"
                              data-action="Grab This Offer"
                              data-label="/cibil-score"
                            >
                              <svg width="17" height="15" viewBox="0 0 17 15">
                                <g fill="none" fillRule="evenodd">
                                  <g fill="#FFF" fillRule="nonzero">
                                    <path
                                      d="M14.53 14.398V22h-4.366c-1.02 0-1.846-.82-1.846-1.833v-5.77h6.211zm7.34 0v5.769c0 1.013-.825 1.833-1.846 1.833h-4.365v-7.602h6.212zM12.037 7.5c.668-.666 1.833-.667 2.5 0 .244.242.466.747.652 1.283.186-.536.409-1.04.652-1.283.668-.666 1.832-.666 2.5 0 .335.332.519.775.519 1.246s-.184.914-.518 1.247c-.071.07-.165.14-.276.208l3.943-.001c.647 0 1.18.339 1.18.75v1.5c0 .411-.533.75-1.18.75H8.18c-.647 0-1.18-.339-1.18-.75v-1.5c0-.411.533-.75 1.18-.75h4.132c-.11-.067-.205-.136-.276-.208-.335-.332-.518-.775-.518-1.246s.183-.914.518-1.246zm1.25.481c-.205 0-.397.079-.542.224-.146.145-.225.336-.225.54 0 .206.08.398.225.542.176.176.944.449 1.758.67-.223-.81-.497-1.576-.674-1.752-.144-.144-.337-.224-.542-.224zm3.804 0c-.205 0-.397.08-.542.224-.177.176-.45.941-.674 1.753.815-.222 1.582-.495 1.759-.67.145-.145.225-.338.225-.542 0-.205-.081-.396-.226-.54-.145-.145-.337-.225-.542-.225z"
                                      transform="translate(-7.000000, -7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </svg>
                              <span className="ml-2">Grab this Offer</span>
                            </button>
                           
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="relative px-4 lg:absolute bottom-4 lg:right-8 lg:-bottom-4 lg:px-0">
                    <button
                      type="button"
                      className="inline-flex items-center justify-end w-full mt-0 sm:mt-4 cursor-pointer"
                      data-testid="icici-bank"
                    >
                      <div className="relative inline-flex items-center h-10 px-3 font-medium tracking-normal text-white rounded-l-full rounded-r-full app-text-dm-xs bg-secondary">
                        <div className="inline-flex items-center">
                          <div>
                            <div
                              style={{
                                display: " inline-block",
                                maxWidth: "100%",
                                overflow: "hidden",
                                position: "relative",
                                boxSizing: " border-box",
                                margin: "0px",
                              }}
                            >
                              <div
                                style={{
                                  boxSizing: "border-box",
                                  display: "block",
                                  maxWidth: "100%",
                                }}
                              >
                                {/* <img
                             alt="description of image"
                            aria-hidden="true"
                            role="presentation"
                            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjIiIGhlaWdodD0iMjIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+"
                            style={{
                              maxWidth: "100%",
                              display: "block",
                              margin: "0px",
                              border: "none",
                              padding: "0px",
                            }}
                          /> */}
                              </div>
                              <noscript></noscript>
                              {/* <img
                          alt="description of image"
                          src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                          decoding="async"
                          className="flex-shrink-0"
                          style={{
                            position: "absolute",
                            inset: "0px",
                            boxSizing: "border-box",
                            padding: "0px",
                            border: "none",
                            margin: "auto",
                            display: "block",
                            width: "0px",
                            height: "0px",
                            minWidth: "100%",
                            maxWidth: "100%",
                            minHeight: "100%",
                            maxHeight: "100%"
                          }}
                        /> */}
                            </div>
                          </div>
                          <span className="ml-[6px]">
                            Bigger loan amount unlocked! Click to select Offer
                          </span>
                          <div className="ml-6">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              className="transform rotate-90"
                            >
                              <g fill="none" fillRule="evenodd">
                                <g fill="#FFF">
                                  <g>
                                    <path
                                      d="M8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8 3.582-8 8-8zm-.554 4.397c-.293-.218-.71-.194-.976.073l-.073.084c-.218.293-.194.71.073.976L8.939 8l-2.47 2.47-.072.084c-.218.293-.194.71.073.976.293.293.767.293 1.06 0L11.06 8 7.53 4.47z"
                                      transform="translate(-7.000000, -7.000000) translate(7.000000, 7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                          </div>
                        </div>
                      </div>
                    </button>
                  </div>
                </div>
              </div>
              <div className="w-full" data-testid="offers-container-4">
                <div className="relative border border-gray-300 rounded-10">
                  <div className="hidden absolute -left-2 top-[10px] bottom-[10px] w-2 rounded-tl-full rounded-bl-full bg-secondary"></div>
                  <div className="hidden absolute -right-2 top-[10px] bottom-[10px] w-2 rounded-tr-full rounded-br-full bg-secondary"></div>
                  <div className="py-[18px]">
                    <div data-testid="container-axis-bank">
                      <div className="px-4 pb-5">
                        <div className="relative w-20 h-10">
                          <div
                            style={{
                              display: "block",
                              overflow: "hidden",
                              position: "absolute",
                              inset: "0px",
                              boxSizing: "border-box",
                              margin: "0px",
                            }}
                          >
                            <noscript></noscript>
                           <img
                         alt="description of image"
                        src={axis}
                        decoding="async"
                        style={{
                          position:"absolute",
                          inset: "0px",
                          boxSizing: "border-box",
                          padding: "0px",
                          border: "none",
                          margin: "auto",
                          display: "block",
                          width: "80px",
                          height: "40px",
                          minwidth: "100%",
                          maxWidth: "100%",
                          minHeight: "100%",
                          maxHeight: "100%",
                          objectFit: "contain",
                          objectPosition: "left center"
                        }}
                      /> 
                          </div>
                        </div>
                      </div>
                      <div className="grid grid-cols-4 px-4 pt-4 pb-3 border-t gap-x-1 gap-y-5 border-gray-light">
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                      src="/icons/loan-amount.svg"
                      alt="Loan Amount"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Loan Amount
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="offers_axis-bank"
                          >
                            4, 00, 00, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            indian rupees
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/rate-of-interest.svg"
                      alt="Rate of Interest"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Rate of Interest
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            6.9%
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                      src="/icons/monthly-emi.svg"
                      alt="Monthly EMI"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Monthly EMI
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="emi_axis-bank"
                          >
                            2, 63, 440/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/processing-fees.svg"
                      alt="Processing Fee"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Processing Fee
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            10, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            + GST
                          </div>
                        </div>
                        <div className="absolute top-2 right-5">
                          <div className="flex items-center h-full mt-4 sm:mt-3">
              
                            <button
                              // type="button"
                              type={"primary"}
                              onClick={handleSubmit}
                              className="w-auto h-9 app-btn app-btn-primary"
                              data-testid="grab-this-offer-axis-bank"
                              data-category="loan_offer"
                              data-action="Grab This Offer"
                              data-label="/cibil-score"
                            >
                              <svg width="17" height="15" viewBox="0 0 17 15">
                                <g fill="none" fillRule="evenodd">
                                  <g fill="#FFF" fillRule="nonzero">
                                    <path
                                      d="M14.53 14.398V22h-4.366c-1.02 0-1.846-.82-1.846-1.833v-5.77h6.211zm7.34 0v5.769c0 1.013-.825 1.833-1.846 1.833h-4.365v-7.602h6.212zM12.037 7.5c.668-.666 1.833-.667 2.5 0 .244.242.466.747.652 1.283.186-.536.409-1.04.652-1.283.668-.666 1.832-.666 2.5 0 .335.332.519.775.519 1.246s-.184.914-.518 1.247c-.071.07-.165.14-.276.208l3.943-.001c.647 0 1.18.339 1.18.75v1.5c0 .411-.533.75-1.18.75H8.18c-.647 0-1.18-.339-1.18-.75v-1.5c0-.411.533-.75 1.18-.75h4.132c-.11-.067-.205-.136-.276-.208-.335-.332-.518-.775-.518-1.246s.183-.914.518-1.246zm1.25.481c-.205 0-.397.079-.542.224-.146.145-.225.336-.225.54 0 .206.08.398.225.542.176.176.944.449 1.758.67-.223-.81-.497-1.576-.674-1.752-.144-.144-.337-.224-.542-.224zm3.804 0c-.205 0-.397.08-.542.224-.177.176-.45.941-.674 1.753.815-.222 1.582-.495 1.759-.67.145-.145.225-.338.225-.542 0-.205-.081-.396-.226-.54-.145-.145-.337-.225-.542-.225z"
                                      transform="translate(-7.000000, -7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </svg>
                              <span className="ml-2">Grab this Offer</span>
                            </button>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="relative px-4 lg:absolute bottom-4 lg:right-8 lg:-bottom-4 lg:px-0">
                    <button
                      type="button"
                      className="inline-flex items-center justify-end w-full mt-0 sm:mt-4 cursor-pointer"
                      data-testid="axis-bank"
                    >
                      <div className="relative inline-flex items-center h-10 px-3 font-medium tracking-normal text-white rounded-l-full rounded-r-full app-text-dm-xs bg-secondary">
                        <div className="inline-flex items-center">
                          <div>
                            <div
                              style={{
                                display: "inline-block",
                                maxWidth: "100%",
                                overflow: "hidden",
                                position: "relative",
                                boxSizing: "border-box",
                                margin: "0px",
                              }}
                            >
                              <div
                                style={{
                                  boxSizing: " border-box",
                                  display: "block",
                                  maxWidth: "100%",
                                }}
                              >
                                {/* <img
                              alt="description of image"
                            aria-hidden="true"
                            role="presentation"
                            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjIiIGhlaWdodD0iMjIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+"
                            style={{
                              maxWidth: "100%",
                              display: "block",
                              margin:"0px",
                              border: "none",
                              padding: "0px"
                            }}
                          /> */}
                              </div>
                              <noscript></noscript>
                              {/* <img
                          alt="description of image"
                          src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                          decoding="async"
                          className="flex-shrink-0"
                          style={{
                            position: "absolute",
                            inset: "0px",
                            boxSizing:" border-box",
                            padding: "0px",
                            border: "none",
                            margin: "auto",
                            display: "block",
                            width: "0px",
                            height: "0px",
                            minwidth: "100%",
                            maxWidth: "100%",
                            minHeight: "100%",
                            maxHeight: "100%"
                          }}
                        /> */}
                            </div>
                          </div>
                          <span className="ml-[6px]">
                            Bigger loan amount unlocked! Click to select Offer
                          </span>
                          <div className="ml-6">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              className="transform rotate-90"
                            >
                              <g fill="none" fillRule="evenodd">
                                <g fill="#FFF">
                                  <g>
                                    <path
                                      d="M8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8 3.582-8 8-8zm-.554 4.397c-.293-.218-.71-.194-.976.073l-.073.084c-.218.293-.194.71.073.976L8.939 8l-2.47 2.47-.072.084c-.218.293-.194.71.073.976.293.293.767.293 1.06 0L11.06 8 7.53 4.47z"
                                      transform="translate(-7.000000, -7.000000) translate(7.000000, 7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                          </div>
                        </div>
                      </div>
                    </button>
                  </div>
                </div>
              </div>
              <div className="w-full" data-testid="offers-container-5">
                <div className="relative border border-gray-300 rounded-10">
                  <div className="hidden absolute -left-2 top-[10px] bottom-[10px] w-2 rounded-tl-full rounded-bl-full bg-secondary"></div>
                  <div className="hidden absolute -right-2 top-[10px] bottom-[10px] w-2 rounded-tr-full rounded-br-full bg-secondary"></div>
                  <div className="py-[18px]">
                    <div data-testid="container-lnthfl">
                      <div className="px-4 pb-5">
                        <div className="relative w-20 h-10">
                          <div
                            style={{
                              display: "block",
                              overflow: "hidden",
                              position: "absolute",
                              inset: "0px",
                              boxSizing: "border-box",
                              margin: "0px",
                            }}
                          >
                            <noscript></noscript>
                            <img
                        alt="description of image"
                        src={lnt}
                        decoding="async"
                        style={{
                          position: "absolute",
                          inset: "0px",
                          boxSizing: "border-box",
                          padding: "0px",
                          border: "none",
                          margin: "auto",
                          display: "block",
                          width: "80px",
                          height: "40px",
                          minwidth: "100%",
                          maxWidth: "100%",
                          minHeight: "100%",
                          maxHeight: "100%",
                          objectFit: "contain",
                          objectPosition: "left center"
                        }}
                      /> 
                          </div>
                        </div>
                      </div>
                      <div className="grid grid-cols-4 px-4 pt-4 pb-3 border-t gap-x-1 gap-y-5 border-gray-light">
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                      src="/icons/loan-amount.svg"
                      alt="Loan Amount"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Loan Amount
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="offers_lnthfl"
                          >
                            4, 00, 00, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            indian rupees
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/rate-of-interest.svg"
                      alt="Rate of Interest"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Rate of Interest
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            6.75%
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                      src="/icons/monthly-emi.svg"
                      alt="Monthly EMI"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Monthly EMI
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="emi_lnthfl"
                          >
                            2, 59, 439/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/processing-fees.svg"
                      alt="Processing Fee"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Processing Fee
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            5, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            + GST
                          </div>
                        </div>
                        <div className="absolute top-2 right-5">
                          <div className="flex items-center h-full mt-4 sm:mt-3">
                         
                            <button
                              type={"primary"}
                              onClick={handleSubmit}
                              // type="button"
                              className="w-auto h-9 app-btn app-btn-primary"
                              data-testid="grab-this-offer-lnthfl"
                              data-category="loan_offer"
                              data-action="Grab This Offer"
                              data-label="/cibil-score"
                            >
                              <svg width="17" height="15" viewBox="0 0 17 15">
                                <g fill="none" fillRule="evenodd">
                                  <g fill="#FFF" fillRule="nonzero">
                                    <path
                                      d="M14.53 14.398V22h-4.366c-1.02 0-1.846-.82-1.846-1.833v-5.77h6.211zm7.34 0v5.769c0 1.013-.825 1.833-1.846 1.833h-4.365v-7.602h6.212zM12.037 7.5c.668-.666 1.833-.667 2.5 0 .244.242.466.747.652 1.283.186-.536.409-1.04.652-1.283.668-.666 1.832-.666 2.5 0 .335.332.519.775.519 1.246s-.184.914-.518 1.247c-.071.07-.165.14-.276.208l3.943-.001c.647 0 1.18.339 1.18.75v1.5c0 .411-.533.75-1.18.75H8.18c-.647 0-1.18-.339-1.18-.75v-1.5c0-.411.533-.75 1.18-.75h4.132c-.11-.067-.205-.136-.276-.208-.335-.332-.518-.775-.518-1.246s.183-.914.518-1.246zm1.25.481c-.205 0-.397.079-.542.224-.146.145-.225.336-.225.54 0 .206.08.398.225.542.176.176.944.449 1.758.67-.223-.81-.497-1.576-.674-1.752-.144-.144-.337-.224-.542-.224zm3.804 0c-.205 0-.397.08-.542.224-.177.176-.45.941-.674 1.753.815-.222 1.582-.495 1.759-.67.145-.145.225-.338.225-.542 0-.205-.081-.396-.226-.54-.145-.145-.337-.225-.542-.225z"
                                      transform="translate(-7.000000, -7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </svg>
                              <span className="ml-2">Grab this Offer</span>
                            </button>
                           
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="relative px-4 lg:absolute bottom-4 lg:right-8 lg:-bottom-4 lg:px-0">
                    <button
                      type="button"
                      className="inline-flex items-center justify-end w-full mt-0 sm:mt-4 cursor-pointer"
                      data-testid="lnthfl"
                    >
                      <div className="relative inline-flex items-center h-10 px-3 font-medium tracking-normal text-white rounded-l-full rounded-r-full app-text-dm-xs bg-secondary">
                        <div className="inline-flex items-center">
                          <div>
                            <div
                              style={{
                                display: "inline-block",
                                maxWidth: "100%;",
                                overflow: "hidden",
                                position: "relative",
                                boxSizing: "border-box",
                                margin: "0px",
                              }}
                            >
                              <div
                                style={{
                                  boxSizing: "border-box",
                                  display: "block",
                                  maxWidth: "100%",
                                }}
                              >
                                {/* <img
                            alt="description of image"
                            aria-hidden="true"
                            role="presentation"
                            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjIiIGhlaWdodD0iMjIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+"
                            style={{
                              maxWidth: "100%",
                              display: "block",
                              margin: "0px",
                              border: "none",
                              padding: "0px"
                            }}
                          /> */}
                              </div>
                              <noscript></noscript>
                              {/* <img
                          alt="description of image"
                          src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                          decoding="async"
                          className="flex-shrink-0"
                          style={{
                            position: "absolute",
                            inset: "0px",
                            boxSizing: "border-box",
                            padding: "0px",
                            border: "none",
                            margin: "auto",
                            display: "block",
                            width: "0px",
                            height: "0px",
                            minwidth: "100%",
                            maxWidth: "100%",
                            minHeight: "100%",
                            maxHeight: "100%"
                          }}
                        /> */}
                            </div>
                          </div>
                          <span className="ml-[6px]">
                            Bigger loan amount unlocked! Click to select Offer
                          </span>
                          <div className="ml-6">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              className="transform rotate-90"
                            >
                              <g fill="none" fillRule="evenodd">
                                <g fill="#FFF">
                                  <g>
                                    <path
                                      d="M8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8 3.582-8 8-8zm-.554 4.397c-.293-.218-.71-.194-.976.073l-.073.084c-.218.293-.194.71.073.976L8.939 8l-2.47 2.47-.072.084c-.218.293-.194.71.073.976.293.293.767.293 1.06 0L11.06 8 7.53 4.47z"
                                      transform="translate(-7.000000, -7.000000) translate(7.000000, 7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                          </div>
                        </div>
                      </div>
                    </button>
                  </div>
                </div>
              </div>
              <div className="w-full" data-testid="offers-container-6">
                <div className="relative border border-gray-300 rounded-10">
                  <div className="hidden absolute -left-2 top-[10px] bottom-[10px] w-2 rounded-tl-full rounded-bl-full bg-secondary"></div>
                  <div className="hidden absolute -right-2 top-[10px] bottom-[10px] w-2 rounded-tr-full rounded-br-full bg-secondary"></div>
                  <div className="py-[18px]">
                    <div data-testid="container-iifl">
                      <div className="px-4 pb-5">
                        <div className="relative w-20 h-10">
                          <div
                           
                            style={{
                              display: "block",
                              overflow: "hidden",
                              position: "absolute",
                              inset: "0px",
                              boxSizing: "border-box",
                              margin: "0px",
                            }}
                          ><img
                          alt="description of image"
                          src={iifl}  />
                            <noscript></noscript>
                             {/* <img
                        alt="description of image"
                        src={iifl}
                        decoding="async"
                        style="
                          position: absolute;
                          inset: 0px;
                          boxSizing: border-box;
                          padding: 0px;
                          border: none;
                          margin: auto;
                          display: block;
                          width: 0px;
                          height: 0px;
                          minwidth: 100%;
                          maxWidth: 100%;
                          minHeight: 100%;
                          maxHeight: 100%;
                          objectFit: contain;
                          objectPosition: left center;
                        "
                      />  */}
                          </div>
                        </div>
                      </div>
                      <div className="grid grid-cols-4 px-4 pt-4 pb-3 border-t gap-x-1 gap-y-5 border-gray-light">
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                      
                      src="/icons/loan-amount.svg"
                      alt="Loan Amount"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Loan Amount
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="offers_iifl"
                          >
                            4, 00, 00, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            indian rupees
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/rate-of-interest.svg"
                      alt="Rate of Interest"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Rate of Interest
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            8.25%
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 sm:col-span-1">
                          {/* <img
                      src="/icons/monthly-emi.svg"
                      alt="Monthly EMI"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Monthly EMI
                          </div>
                          <div
                            className="text-sm app-text-dm-large text-primary"
                            data-testid="emi_iifl"
                          >
                            3, 00, 507/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            onwards
                          </div>
                        </div>
                        <div className="col-span-2 lg:col-span-1">
                          {/* <img
                      src="/icons/processing-fees.svg"
                      alt="Processing Fee"
                      className="h-8 hidden"
                    /> */}
                          <div className="mt-2 uppercase app-text-input-secondary text-gray text-opacity-60">
                            Processing Fee
                          </div>
                          <div className="text-sm app-text-dm-large text-primary">
                            80, 000/-
                          </div>
                          <div className="uppercase app-text-dm-small text-gray text-opacity-60 text-xxs">
                            + GST
                          </div>
                        </div>
                        <div className="absolute top-2 right-5">
                          <div className="flex items-center h-full mt-4 sm:mt-3">
                         
                            <button
                              type={"primary"}
                              onClick={handleSubmit}
                              // type="button"
                              className="w-auto h-9 app-btn app-btn-primary"
                              data-testid="grab-this-offer-iifl"
                              data-category="loan_offer"
                              data-action="Grab This Offer"
                              data-label="/cibil-score"
                            >
                              <svg width="17" height="15" viewBox="0 0 17 15">
                                <g fill="none" fillRule="evenodd">
                                  <g fill="#FFF" fillRule="nonzero">
                                    <path
                                      d="M14.53 14.398V22h-4.366c-1.02 0-1.846-.82-1.846-1.833v-5.77h6.211zm7.34 0v5.769c0 1.013-.825 1.833-1.846 1.833h-4.365v-7.602h6.212zM12.037 7.5c.668-.666 1.833-.667 2.5 0 .244.242.466.747.652 1.283.186-.536.409-1.04.652-1.283.668-.666 1.832-.666 2.5 0 .335.332.519.775.519 1.246s-.184.914-.518 1.247c-.071.07-.165.14-.276.208l3.943-.001c.647 0 1.18.339 1.18.75v1.5c0 .411-.533.75-1.18.75H8.18c-.647 0-1.18-.339-1.18-.75v-1.5c0-.411.533-.75 1.18-.75h4.132c-.11-.067-.205-.136-.276-.208-.335-.332-.518-.775-.518-1.246s.183-.914.518-1.246zm1.25.481c-.205 0-.397.079-.542.224-.146.145-.225.336-.225.54 0 .206.08.398.225.542.176.176.944.449 1.758.67-.223-.81-.497-1.576-.674-1.752-.144-.144-.337-.224-.542-.224zm3.804 0c-.205 0-.397.08-.542.224-.177.176-.45.941-.674 1.753.815-.222 1.582-.495 1.759-.67.145-.145.225-.338.225-.542 0-.205-.081-.396-.226-.54-.145-.145-.337-.225-.542-.225z"
                                      transform="translate(-7.000000, -7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </svg>
                              <span className="ml-2">Grab this Offer</span>
                            </button>
                           
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="relative px-4 lg:absolute bottom-4 lg:right-8 lg:-bottom-4 lg:px-0">
                    <button
                      type="button"
                      className="inline-flex items-center justify-end w-full mt-0 sm:mt-4 cursor-pointer"
                      data-testid="iifl"
                    >
                      <div className="relative inline-flex items-center h-10 px-3 font-medium tracking-normal text-white rounded-l-full rounded-r-full app-text-dm-xs bg-secondary">
                        <div className="inline-flex items-center">
                          <div>
                            <div
                              style={{
                                display: "inline-block",
                                maxWidth: "100%",
                                overflow: "hidden",
                                position: "relative",
                                boxSizing: "border-box",
                                margin: "0px",
                              }}
                            >
                              <div
                                style={{
                                  boxSizing: " border-box",
                                  display: "block",
                                  maxWidth: "100%",
                                }}
                              >
                                {/* <img
                              alt="description of image"
                            aria-hidden="true"
                            role="presentation"
                            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjIiIGhlaWdodD0iMjIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+"
                            style={{
                              maxWidth: "100%",
                              display: "block",
                              margin: "0px",
                              border: "none",
                              padding: "0px"
                            }}
                          /> */}
                              </div>
                              <noscript></noscript>
                              {/* <img
                          alt="description of image"
                          src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                          decoding="async"
                          className="flex-shrink-0"
                          style={{
                            position: "absolute",
                            inset: "0px",
                            boxSizing: "border-box",
                            padding: "0px",
                            border: "none",
                            margin: "auto",
                            display: "block",
                            width: "0px",
                            height: "0px",
                            minwidth: "100%",
                            maxWidth: "100%",
                            minHeight: "100%",
                            maxHeight: "100%"

                          }}
                        /> */}
                            </div>
                          </div>
                          <span className="ml-[6px]">
                            Bigger loan amount unlocked! Click to select Offer
                          </span>
                          <div className="ml-6">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              className="transform rotate-90"
                            >
                              <g fill="none" fillRule="evenodd">
                                <g fill="#FFF">
                                  <g>
                                    <path
                                      d="M8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8 3.582-8 8-8zm-.554 4.397c-.293-.218-.71-.194-.976.073l-.073.084c-.218.293-.194.71.073.976L8.939 8l-2.47 2.47-.072.084c-.218.293-.194.71.073.976.293.293.767.293 1.06 0L11.06 8 7.53 4.47z"
                                      transform="translate(-7.000000, -7.000000) translate(7.000000, 7.000000)"
                                    ></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                          </div>
                        </div>
                      </div>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="pb-5 text-sm flex items-start sm:pt-3">
            <span className="mr-1">*</span>
            <span>
              Rate of Interest and Processing Fee are subject to sole discretion
              of banks
            </span>
          </div>
        </div>
      </div>


          <div
                    className={
                      "form__item button__items d-flex justify-content-between"
                    }
                  >
                    <Button
                      type={"default"}
                      onClick={prev}
                      className="w-auto space-x-2 app-btn app-btn-primary"
                      data-testid="submit"
                      data-category="unlock_credit_score"
                      data-action="Unlock credit score"
                      data-label="/personal-detail"
                    >
                      <span>Back</span>
                    </Button>
                    <Button
                      type={"primary"}
                      onClick={handleSubmit}
                      className="w-auto space-x-2 app-btn app-btn-primary"
                      data-testid="submit"
                      data-category="unlock_credit_score"
                      data-action="Unlock credit score"
                      data-label="/personal-detail"
                    >
                      Next
                    </Button>
                  </div>
    </div>
      );
    }}
  </Formik>
);
};
export default Exploreloan;
