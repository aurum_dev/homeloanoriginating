import React from "react";
import "../../../assets/css/Footer/Footer.css";
function Footer() {
  return (
    <div className="" style={{display:"show"}}>

<link
      rel="preload"
      as="style"
      href="https://fonts.googleapis.com/css?family=Rubik:400,500%7CPoppins:600,400,500,700&amp;display=swap&amp;ver=1629726924"
    />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Rubik:400,500%7CPoppins:600,400,500,700&amp;display=swap&amp;ver=1629726924"
      media="print"
      onLoad="this.media='all'"
    />

    <noscript>
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Rubik:400,500%7CPoppins:600,400,500,700&display=swap&ver=1629726924"
      />
    </noscript>
<footer className="main-footer">
    
      {/* <div className="widgets-section top-part">
        <div className="auto-container">
          <div className="row clearfix" style={{paddingleft: "20px"}}>
            <div className="top-div col-xl-5 col-lg-12 col-md-12 col-sm-12">
              <div className="inner">
             

                <p className="desk-view">
                  Stay up-to-date on the latest industry news, digital
                  <br />products and Innovation impacting insurance.
                </p>

                <p className="mobile-view">
                  Stay up-to-date on the latest industry news, digital products
                  and Innovation impacting insurance.
                </p>
              </div>
            </div>

            <div className="top-div col-xl-7 col-lg-12 col-md-12 col-sm-12" style={{paddingleft: "0px"}}>
              <div className="inner">
                <div className="newsletter-form">
                  <form method="post" action="">
                    <div className="form-group clearfix row">
                      <input
                        type="email"
                        className="col-md-7"
                        name="email"
                        value=""
                        placeholder="Enter your email address"
                        required=""
                      />
                      <button type="submit" className="col-md-4">
                        Subscribe <i className="fas fa-long-arrow-alt-right"></i>
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> */}
      <div className="widgets-section mid-part">
        <div className="auto-container">
          <div className="row clearfix">
            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <div className="inner">
        
                <h3 className="wow">
                  {/* <span>The Future of PropTech </span> With Artificial
                  Intelligence */}
                </h3>
              </div>
            </div>

            <div className="column col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <div className="footer-widget links-widget">
                <div className="widget-content">
                  <div className="row clearfix">
                    <div className="col-lg-12 col-md-12 col-sm-12">
                      <div className="row clearfix">
                        <div className="column col-lg-3 col-md-3 col-sm-12">
                          <div className="widget-title">
                            <h4>Partners Solutions</h4>
                          </div>
                          <ul className="links">
                            <li><a href="https://www.aurumproptech.com/" target="_blank">- Aurum Premium Listing</a></li>
                            <li><a href="https://www.aurumventures.in/" target="_blank">- Aurum Ventures</a></li>
                            <li><a href="https://aurumproptech.in/" target="_blank">- Aurum PropTech</a></li>
                            <li><a href="https://auruminfinity.com/" target="_blank">- Aurum Infinity</a></li>
                            <li>
                              <a
                                target="_blank"
                                href="https://www.aurumproptech.com/CREX/"
                                >- Aurum CREX</a
                              >
                            </li>
                          </ul>
                        </div>

                        <div className="column col-lg-3 col-md-3 col-sm-12">
                          <div className="widget-title">
                            <h4>Our Solutions</h4>
                          </div>
                          <ul className="links">
                            <li>
                              <a target="_blank" href="https://www.sell.do/"
                                >- Sell.do
                              </a>
                            </li>
                            <li>
                              <a target="_blank" href="https://kylas.io/"
                                >- Kylas
                              </a>
                            </li>

                            <li>
                              <a
                                target="_blank"
                                href="https://thehousemonk.com/"
                                >- TheHouseMonk</a
                              >
                            </li>
                            <li>
                              <a
                                target="_blank"
                                href="http://www.integrowamc.com/"
                                >- Integrow AMC</a
                              >
                            </li>
                            <li>
                              <a target="_blank" href="https://grexter.in/"
                                >- Grexter</a
                              >
                            </li>
                            <li>
                              <a target="_blank" href="https://beyondwalls.com/"
                                >- Beyondwalls</a
                              >
                            </li>
                          </ul>
                        </div>

                        <div className="column col-lg-3 col-md-3 col-sm-12">
                          <div className="widget-title">
                            <h4>Latest Launch</h4>
                          </div>
                          <ul className="links">
                            <li><a href="https://dev.aurumkuberx.com/" target="_blank">- Aurum Q5</a></li>
                          </ul>
                        </div>

                        <div className="column col-lg-3 col-md-3 col-sm-12">
                          <div className="widget-title">
                            <h4>Contact Us</h4>
                          </div>
                          <ul className="links" style={{paddingleft: "0px"}}>
                            <li
                              style={{
                                fontsize: "18px",
                                opacity: "0.8",
                                fontfamily: "Rubik"
                              }}
                            >
                              Aurum Building Q1
                            </li>
                            <li
                              style={{
                                fontsize:"16px",
                                opacity: "0.5",
                                fontfamily: "Rubik"}}
                              
                            >
                              Thane - Belapur Rd, Ghansoli, Navi Mumbai,
                              Maharashtra 400710
                            </li>
                          </ul>
                          <div className="widget-title">
                            <h4>Follow Us</h4>
                          </div>
                          <ul className="social-links">
                            <li>
                              <a
                                href="https://www.facebook.com/AurumPropTech"
                                target="_blank"
                                ><span
                                  className="fab fa-facebook-square fa-lg"
                                ></span
                              ></a>
                            </li>
                            <li>
                              <a
                                href="https://www.instagram.com/aurumproptech/"
                                target="_blank"
                                ><span className="fab fa-instagram fa-lg"></span
                              ></a>
                            </li>
                            <li>
                              <a
                                href="https://www.linkedin.com/uas/login?session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2F71413509%2Fadmin"
                                target="_blank"
                                ><span className="fab fa-linkedin fa-lg"></span
                              ></a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

 
      <div className="footer-bottom">
        <div className="auto-container">
          <div className="inner">
            <div className="copyright">
              © 2022 Aurum Software &amp; Solutions Private Limited |
              <a href="#">Cookie Policy</a> |
              <a href="privacy-policy.html " target="_blank">Privacy Policy</a> |
              <a href="Terms-conditions.html" target="_blank">Terms &amp; Conditions</a> 
            </div>
          </div>
        </div>
      </div>
    </footer>
    </div>
  );
}
export default Footer;
