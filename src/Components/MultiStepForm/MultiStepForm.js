import React, { useState } from "react";
import { Steps } from "antd";
import { Provider } from "./MultiStepFormContext";
// import Details from "./Details";
// import Address from "./Address";
 import Review from "./Review";
 

import Loanrequirements from "../MultiStepForm/LoanRequire/Requirement";
import Personaldetails from "../MultiStepForm/Personaldetails/Personaldetails";
import Exploreloan from  "../MultiStepForm/ExploreLoanoffer/Exploreloan"
import Documentverify from "../MultiStepForm/Documentverify/Documentverify";




const { Step } = Steps;

const requirementInitialState = {
  
  slider:"",
  slidersubreuire:"",
  emptype:"",
  workexp:"",
  anuincome: "",
  monthincome: "",
  // anuincome: "",
  emi: "",

};

const personaldetailsInitialState = {
 name: "",
 gender: "",
 date: "",
 marriedstatus: "",
 email: "",
 pannum:"",
 mobilenum: "",
 address:"",
 pincode:"",
 state:"",
 city:"",
 propname:"",
 projname:"",
 nameofbuild:"",
 locality:"",

};

const exploreloanInitialState = {
  explorename: "",
 
  // city: "",
  // radio:"",
  // color:"",
  // slider:""
};

 const documentverifyInitialState = {
 
  // portal:"",
  file:"",
  pancard:"",
  tancard:"",
  form:"",
  itr:"",
  subpan:"",
  relation:"",
  salaryslip:"",
 };
 

const renderStep = (step) => {
  switch (step) {
    case 0:
      return <Loanrequirements />;
    case 1:
      return <Personaldetails />;
    case 2:
      return <Exploreloan />;
    case 3:
      return <Documentverify />;
      case 4:
        return <Review />;
    default:
      return null;
  }
};

const MultiStepForm = () => {
  const [requirement, setRequirement] = useState(requirementInitialState);
  const [personaldetails, setPersonaldetails] = useState(personaldetailsInitialState);
  const [ exploreloan, setExploreloan] = useState(exploreloanInitialState);
  const [documentverify, setDocumentverify] = useState(documentverifyInitialState);
  const [currentStep, setCurrentStep] = useState(0);

  const next = () => {
    if (currentStep === 4) {
      setCurrentStep(0);
      setRequirement(requirementInitialState);
      setPersonaldetails(personaldetailsInitialState);
      setExploreloan(exploreloanInitialState);
      setDocumentverify(documentverifyInitialState);
      return;
    }
    setCurrentStep(currentStep + 1);
  };

  const submitForm = () => {

  }
  const prev = () => setCurrentStep(currentStep - 1);
  return (
    <Provider value={{requirement, setRequirement, next, prev, personaldetails, setPersonaldetails,exploreloan,setExploreloan,documentverify,  setDocumentverify }}>
      <Steps current={currentStep}  style={{paddingLeft: "30px",
    paddingRight: "30px",paddingTop:"30px"}}>
        <Step title={"Basic Details"} />
        <Step title={" Personal Information"} />
        <Step title={" Loan Offers"} />
        <Step title={" Document Verification"} />
        <Step title={"Review and Save"} />
      </Steps> 
      <main>{renderStep(currentStep)}</main>
    </Provider>
  );
  
};
export default MultiStepForm;
