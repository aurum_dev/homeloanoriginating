import { Button, Col, Row } from "antd";
import React, { useContext, useEffect } from "react";
import MultiStepFormContext from "./MultiStepFormContext";
import JSONPretty from "react-json-pretty";
import axios from "axios"

// const fs = require("fs");

const Review = () => {
  const {
    requirement,
    personaldetails,
    exploreloan,
    documentverify,
    next,
    prev,
  } = useContext(MultiStepFormContext);

  // const data = {
  //   requirement: JSON.stringify(requirement),
  //   personaldetails: JSON.stringify(personaldetails),
  //   exploreloan: JSON.stringify(exploreloan),
  //   documentverify: JSON.stringify(documentverify),
  // };

  const data = {
    requirement: requirement,
    personaldetails: personaldetails,
    exploreloan: exploreloan,
    documentverify: documentverify,
  };

  var raw = JSON.stringify(data);
  var requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
     body: raw,
    redirect: "follow",
  };

  useEffect(() => {
  fetch("/home-loan/api/homeloan", requestOptions)
    .then((response) => response.text())

    .then(result => alert(JSON.parse(result).msg))

    .catch((error) => console.log("error", error));
  })
  //   fs.writeFile('./Data.json', JSON.stringify(data), (err) => {
  //     if (err) console.log('Error writing file:', err);
  // })

  return (
    <div>
      <meta
        http-equiv="content-type"
        content="application/json; charset=UTF-8"
      ></meta>
      <div className={"details__wrapper"}>
        <JSONPretty id="json-pretty" data={data}></JSONPretty>

        {/* <JSONPretty id="json-pretty" data={personaldetails}></JSONPretty>
    <JSONPretty id="json-pretty" data={exploreloan}></JSONPretty>
    <JSONPretty id="json-pretty" data={documentverify}></JSONPretty> */}
        {/* <div>

   
        {data.requirement}
      
      </div>
      <div>

        {data.personaldetails}
      </div>
      <div>

        {data.exploreloan}
      </div>
      <div>
   
        {data.documentverify}
      </div> */}
        {/* <div
        className={"form__item button__items d-flex justify-content-between"}
      >
        <Button type={"default"} onClick={prev}>
          Back
        </Button>
        <Button type={"primary"} onClick={next}>
          Confirm
        </Button>
      </div> */}
        <div
          className={"form__item button__items d-flex justify-content-between"}
          style={{ marginTop: "15px" }}
        >
          <Button
            type={"primary"}
            onClick={prev}
            style={{
              backgrounColor: "#fff",
              color: "white",
              width: "175px",
              height: "52px",
              fontSize: "1.25rem",
              borderradius: "0.3rem",
            }}
            className="btn btn-primary btn-lg active"
          >
            <span style={{ marginLeft: "28px" }}>Previous</span>
          </Button>
          <Button
            type={"primary"}
            onClick={next}
            style={{
              backgrounColor: "#fff",
              color: "white",
              width: "175px",
              height: "52px",
              fontSize: "1.25rem",
              borderradius: "0.3rem",
            }}
            className="btn btn-primary btn-lg active"
          >
            Confirm
          </Button>
        </div>
      </div>
    </div>
  );
};
export default Review;
