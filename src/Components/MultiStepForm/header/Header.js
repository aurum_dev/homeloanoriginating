import React,{useState,useEffect} from "react";
import successicon1 from "../../../assets/images/successicon1.png";
// import "../../../assets/css/header/header.css";

function Header() {
    

   
  return (
    <div >
    <div className="sticky-wrapper" style={{ height: "101px" }}></div>
    <header
        className="header-section sticky is-sticky"
        data-toggle="sticky-onscroll"
    >
       
        <div className="header-navigation">
            <div className="container">
                <div className="row align-items-center">
                    <nav className="navbar navbar-light light-blue lighten-4">
                        {/* <a className="navbar-brand" href="#">
          Navbar
        </a> */}
                        <div className="logo">
                            <a href="#">   <img
                                src={successicon1}
                                alt=""

                            /></a>
                        </div>

                        <button  
                            className="navbar-toggler toggler-example"
                            type="button"
                            data-toggle="collapse"
                            data-target="#navbarSupportedContent1"
                            aria-controls="navbarSupportedContent1"
                            aria-expanded='false'
                            aria-label="Toggle navigation"
                        >
                            <span className="dark-blue-text">
                                <i className="fas fa-bars fa-1x"></i>
                            </span>
                        </button>

                        <div
                            className= "collapse navbar-collapse"
                            id="navbarSupportedContent1"
                        >
                            <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                    <a className="nav-link" href="/#getstarted">
                                    Get Started
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/#contact">
                                    Contact Us
                                    </a>
                                </li>
                                <li className="nav-item active">
                                    <a className="nav-link" href="/#offering" style={{color: "rgba(0,0,0,.5)"}} >
                                    Our Offerings<span className="sr-only">(current)</span>
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/#howworks">
                                    How it works
                                    </a>
                                </li>
                                {/* <li className="nav-item">
                                    <a className="nav-link" href="#">
                                    Get Started
                                    </a>
                                </li> */}
                                {/* <li className="nav-item">
                                    <a className="nav-link" href="/#contact">
                                    Contact Us
                                    </a>
                                </li> */}
                            </ul>
                        </div>

                        <div className="col-xl-10 mobile-menu">
                <div className="nav-menu">
                    <nav style={{display: "block"}}>
                        <ul className="mobile-slider">
                        <li className="mobile-slider-menu"><a href="/#getstarted">Get Started</a></li>
                        <li className="mobile-slider-menu"><a href="/#contact">Contact Us
                            </a>
                            
                            </li>
                            <li className="mobile-slider-menu"><a href="/#offering" >Our Offerings</a>
                             
                            </li>
                            <li className="mobile-slider-menu"><a href="/#howworks">How it works</a></li>
                            {/* <li className="mobile-slider-menu"><a href="#">Get Started</a>
                             
                            </li> */}
                            {/* <li className="mobile-slider-menu"><a href="/#contact">Contact Us
                            </a>
                            
                            </li> */}
                           

                    
                             {/* <li className="nav-item" style={{width: "78px",
height: "38px",border:"1px solid #7FCD91",marginLeft:"20px"}}>
<a className="nav-link login" href="#" style={{padding:"3px 0px",
marginRight:"21px"}}>Login</a>
</li>	 */}
                        </ul>
                    </nav>
                </div>
            </div> 
                    </nav>
                    {/* <div className="col-xl-2 col-lg-6 col-md-6 col-6">
                <div className="logo">
                    <a href="index.html">   <img
src={aurumlogo}
alt=""

/></a>
                </div>
            </div>*/}
          
                </div>
            </div>
        </div>
    </header>
</div>
  );
}
export default Header;
